@extends('layouts.app_login')

@section('content')
    <div class="card">
        <div class="card-body">

            <h3 class="text-center mt-0 m-b-15">
                <a href="#" class="logo logo-admin"><img src="/assets/images/logo_ctlao.png" height="60" alt="logo">&nbsp;&nbsp;Đăng nhập</a>
            </h3>

            <div class="p-3">
                <form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }} row">
                        <div class="col-12">
                            <input class="form-control" type="text" required="" placeholder="Username" name="username"
                                   value="{{ old('username') }}">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                        <div class="col-12">
                            <input class="form-control" type="password" placeholder="Password" required name="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group text-center row m-t-20">
                        <div class="col-12">
                            <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Đăng nhập
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
