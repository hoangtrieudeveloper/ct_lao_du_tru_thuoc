@extends('layouts.app')
@section('title')
    Thông tin hành chính các cơ sở y tế (Mẫu 04)
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">

            <div class="card m-b-30 m-t-20 min-h-80">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Thông tin hành chính các cơ sở y tế (Mẫu 04)</h4>
                    <div class="pull-right" style="margin-top: -40px">
                        <button class="btn btn-success btn-sm pull-right" style="margin-left: 10px" data-toggle="modal"
                                data-target="#modal-view-mau">Tạo mẫu Công văn
                        </button>
                        <a class="btn btn-sm btn-primary pull-right"
                           href="{{route('prov-info-site-export')}}">Xuất Excel</a>
                    </div>
                    <table class="table table-striped datatable">
                        <thead>
                        <tr>
                            <th rowspan="2">TT</th>
                            <th rowspan="2">Tên Cơ sở y tế</th>
                            <th rowspan="2">Mã KBCB CSYT</th>
                            <th rowspan="2">Địa chỉ chi tiết của cơ sở y tế</th>
                            <th colspan="3">Thông tin liên hệ</th>
                        </tr>
                        <tr>
                            <th>Cán bộ đầu mối</th>
                            <th>Điện thoại</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $idx => $row)
                            <tr>
                                <td class="text-center">{{$idx + 1}}</td>
                                <td>{{$row->name}}</td>
                                <td class="text-center">{{$row->ma_cskcb}}</td>
                                <td>{{$row->address}}</td>
                                <td>{{$row->can_bo_dau_moi}}</td>
                                <td>{{$row->dienthoai}}</td>
                                <td>{{$row->email}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('prov.mau_04.modal_cv')

@endsection