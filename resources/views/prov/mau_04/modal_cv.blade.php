<div class="modal" id="modal-view-mau">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button class="btn btn-sm btn-primary" onclick="downCV()">Download PDF</button>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="body-view-mau">
                <style>
                    .t-right {
                        text-align: right !important;
                    }

                    .table-mau {
                        font-size: 12px !important;
                        margin-top: 20px;
                        width: 100%;
                        border-collapse: collapse;
                    }

                    .table-mau, .table-mau td, .table-mau th {
                        border: 1px solid #ccc;
                    }

                    .mau-container {
                        font-family: " Times New Roman", Times, serif;
                        padding: 20px;
                    }

                    .table-mau td, .table-mau th {
                        padding: 3px 5px;
                    }

                    .page-top {
                        height: 20px;
                    }

                </style>
                <div class="mau-container">
                    <div class="page-top"></div>
                    <div>
                        <div style="float: left; font-weight: bold">
                            <div>Sở Y tế/ Đơn vị đầu mối chống Lao {{$tinh->name}}</div>
                        </div>

                        <div style="text-align: center; font-weight: bold; font-size: 14px; clear: both; margin-top: 10px">
                            THÔNG TIN HÀNH CHÍNH CÁC CƠ SỞ Y TẾ
                        </div>

                    </div>
                    <table class="table-mau">
                        {!! $tHead !!}
                        <tbody>
                        @foreach($data as $idx => $row)
                            @if($idx > 0 && ($idx % 16 == 0) && $idx < count($data) - 1)
                        </tbody>
                    </table>
                    <div class="page-top break-table"></div>
                    <table class="table-mau">
                        {!! $tHead !!}
                        <tbody>
                        @endif
                        <tr>
                            <td class="text-center">{{$idx + 1}}</td>
                            <td>{{$row->name}}</td>
                            <td class="text-center">{{$row->ma_cskcb}}</td>
                            <td>{{$row->address}}</td>
                            <td>{{$row->can_bo_dau_moi}}</td>
                            <td>{{$row->dienthoai}}</td>
                            <td>{{$row->email}}</td>
                        </tr>
                        @endforeach
                        </tbody>

                    </table>
                    <div style="padding-bottom: 170px; margin-top: 20px">
                        <div style="float: left; width: 50%">
                            <div style="text-align: center; font-weight: bold">Người lập biểu</div>
                            <div style="text-align: center; font-style: italic">(Ký và ghi rõ họ tên)</div>
                        </div>
                        <div style="float: left; width: 50%">
                            <div style="text-align: center; font-style: italic">Ngày {{date('d')}}
                                tháng {{date('m')}} năm {{date('Y')}}</div>
                            <div style="text-align: center;font-weight: bold">Thủ trưởng đơn vị</div>
                            <div style="text-align: center; font-style: italic">(Ký, đóng dấu và ghi rõ họ tên)</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.4/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('js/FileSaver.js')}}"></script>
<script src="/html2pdf/html2pdf.bundle.min.js"></script>
<script>
    function downCV() {
        const element = document.getElementById("body-view-mau");
        var opt = {
            filename: 'CongVanBaoCao_Mau04.pdf',
            jsPDF: {unit: 'in', format: 'letter', orientation: 'portrait'},
            html2canvas: {
                dpi: 128,
                scale: 4,
                letterRendering: true,
                useCORS: true
            },
            pagebreak: {before: '.break-table'}
        };
        html2pdf()
            .set(opt)
            .from(element)
            .save();
    }
</script>