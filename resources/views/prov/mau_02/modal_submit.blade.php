<div class="modal" id="modal-plan-submit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Gửi kế hoạch lên trung ương</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'prov-submit-plan', 'files' => 'true')) !!}
                <div>Tỉnh/TP: <strong>{{$tinh->name}}</strong></div>
                <div>Số kế hoạch: <strong>TTP{{$soCV}}</strong></div>
                <div class="text-danger">Chú ý(*): Kiểm tra so khớp số kế hoạch trên với File Báo cáo nhu cầu kế hoạch
                    thuốc
                </div>
                <div class="form-group">
                    <label>Báo cáo tổng hợp kế hoạch dự trù theo thuốc (Mẫu 02)</label>
                    <input type="file" name="mau_02" required class="dropify" data-height="100"
                           accept=".pdf,.png,.jpg,.jpeg"/>
                </div>
                <div class="form-group">
                    <label>Báo cáo tổng hợp kế hoạch dự trù theo cơ sở y tế (Mẫu 03)</label>
                    <input type="file" name="mau_03" required class="dropify" data-height="100"
                           accept=".pdf,.png,.jpg,.jpeg"/>
                </div>
                <div class="form-group">
                    <label>Thông tin hành chính các cơ sở y tế (Mẫu 04)</label>
                    <input type="file" name="mau_04" required class="dropify" data-height="100"
                           accept=".pdf,.png,.jpg,.jpeg"/>
                </div>
                <div class="form-group">
                    <label>Biên bản thẩm định của sở y tế</label>
                    <input type="file" name="bb_syt" required class="dropify" data-height="100"
                           accept=".pdf,.png,.jpg,.jpeg"/>
                </div>
                @if($hasGiaiTrinh)
                    <div class="form-group col-md-12">
                        <label class="text-warning">Công văn giải trình trong trường hợp cần thiết giải trình về việc dự
                            trù thuốc tăng
                            giảm quá 30% so với lượng thuốc đã sử dụng giai đoạn 2019-2020 (Bản scan công văn đã
                            được ký, đóng dấu)</label>
                        <input type="file" name="file_cv_giai_trinh"
                               class="dropify" data-height="70" accept=".pdf,.png,.jpg,.jpeg"/>
                    </div>
                @endif
                <input type="submit" class="btn btn-success btn-sm pull-right" value="Gửi kế hoạch">
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
// Basic
        $('.dropify').dropify();

// Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

// Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function (event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function (event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function (event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function (e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>