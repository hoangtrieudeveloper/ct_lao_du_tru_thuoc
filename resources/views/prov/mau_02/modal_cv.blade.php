<div class="modal" id="modal-view-mau">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button class="btn btn-sm btn-primary" onclick="downCV()">Download PDF</button>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="body-view-mau">
                <style>
                    .t-right {
                        text-align: right !important;
                    }

                    .table-mau {
                        font-size: 12px !important;
                        margin-top: 20px;
                        width: 100%;
                        border-collapse: collapse;
                    }

                    .table-mau, .table-mau td, .table-mau th {
                        border: 1px solid #ccc;
                    }

                    .mau-container {
                        font-family: " Times New Roman", Times, serif;
                    }

                    .table-mau td, .table-mau th {
                        padding: 3px 5px;
                    }
                </style>
                <div class="mau-container">
                    <div>
                        <div style="float: left;  font-weight: bold">

                            <div>{{session('uinfo')->fullname ? session('uinfo')->fullname : ''}}</div>
                        </div>
                        <div style="float: right; width: 65%">
                            <div style="text-align: right">TTP{{$soCV}}</div>
                        </div>
                        <div style="text-align: center; font-weight: bold; font-size: 14px; clear: both; margin-top: 10px">
                            BÁO CÁO TỔNG HỢP NHU CẦU SỬ DỤNG THUỐC CHỐNG LAO HÀNG 1 THUỘC DANH MỤC ĐẤU THẦU TẬP TRUNG
                            QUỐC GIA NĂM 2022 -2023
                        </div>
                        {{--                        <div style="text-align: center; font-style: italic">(Ban hành kèm theo Công văn số--}}
                        {{--                            …....../BVPTƯ-CTCLQG ngày …... tháng ….. năm 2021 của Bệnh viện Phổi Trung ương)--}}
                        {{--                        </div>--}}
                    </div>
                    <table class="table-mau">
                        <thead>
                        <tr>
                            <th rowspan="3">STT</th>

                            <th rowspan="3">Mã thuốc</th>
                            <th rowspan="3">Tên hoạt chất</th>
                            <th rowspan="3">Nồng độ, hàm lượng</th>
                            <th rowspan="3">Dạng bào chế</th>
                            <th rowspan="3">Đường dùng</th>
                            <th rowspan="3">Nhóm thuốc</th>
                            <th rowspan="3">Đơn vị tính</th>
                            <th rowspan="3" style="max-width: 100px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo
                                thống kê của
                                CTCLQG
                            </th>
                            <th colspan="{{($plan->year_to + 1 - $plan->year_from) * 4}}">Số lượng dự trù (24 tháng)
                            </th>
                            <th rowspan="3">Tổng số lượng</th>
                        </tr>
                        <tr>
                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                <th colspan="4">{{$year}}</th>
                            @endfor

                        </tr>
                        <tr>
                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                <th>Quý I</th>
                                <th>Quý II</th>
                                <th>Quý III</th>
                                <th>Quý IV</th>
                            @endfor

                        </tr>
                        </thead>
                        <tbody>
                        <?php $stt = 0;?>
                        @foreach($data as $row)
                            <?php $stt++;?>
                            <tr class="{{$stt > 0 && $stt % 9 == 0?'break-table':''}}">
                                <td style="text-align: center">{{$stt}}</td>
                                <td>{{$row['ma_thuoc']}}</td>
                                <td>{{$row['ten_hoat_chat']}}</td>
                                <td>{{$row['ham_luong']}}</td>
                                <td>{{$row['dang_bao_che']}}</td>
                                <td>{{$row['duong_dung']}}</td>
                                <td>{{$row['nhom_thuoc']}}</td>
                                <td>{{$row['dvt']}}</td>
                                <td style="text-align: right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row))}}</td>
                                @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                    @for($q = 1; $q <= 4; $q ++)
                                        <td style="text-align: right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row))}}</td>
                                    @endfor
                                @endfor
                                <td style="text-align: right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row))}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="19"><strong>Tổng {{count($data)}} khoản</strong></td>
                        </tr>
                        </tbody>
                    </table>
                    <div style="padding-bottom: 170px; margin-top: 20px">
                        <div style="float: left; width: 50%">
                            <div style="text-align: center; font-weight: bold">Người lập biểu</div>
                            <div style="text-align: center; font-style: italic">(Ký và ghi rõ họ tên)</div>
                        </div>
                        <div style="float: left; width: 50%">
                            <div style="text-align: center; font-style: italic">Ngày {{date('d')}}
                                tháng {{date('m')}} năm {{date('Y')}}</div>
                            <div style="text-align: center;font-weight: bold">Thủ trưởng đơn vị</div>
                            <div style="text-align: center; font-style: italic">(Ký, đóng dấu và ghi rõ họ tên)</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.4/jszip.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('js/FileSaver.js')}}"></script>
<script src="/html2pdf/html2pdf.bundle.min.js"></script>
<script>
    function downCV() {
        const element = document.getElementById("body-view-mau");
        var opt = {
            filename: 'CongVanBaoCao_Mau02.pdf',
            jsPDF: {unit: 'in', format: 'letter', orientation: 'landscape'},
            html2canvas: {
                dpi: 128,
                scale: 4,
                letterRendering: true,
                useCORS: true
            },
            pagebreak: {before: '.break-table'}
        };
        html2pdf()
            .set(opt)
            .from(element)
            .save();
    }
</script>