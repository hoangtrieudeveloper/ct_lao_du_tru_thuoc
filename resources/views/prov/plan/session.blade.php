@extends('layouts.app')
@section('title')
    Danh sách kế hoạch đã gửi lên Tỉnh/TP
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="card m-b-30 m-t-20 min-h-80">
                <div class="card-body">
                    <div class="pull-right">
                        <button class="btn btn-primary btn-sm pull-right" style="margin-left: 10px" data-toggle="modal"
                                data-target="#modal-ds-cs">Danh sách cơ sở chưa gửi kế hoạch
                        </button>
                    </div>
                    <h4 class="mt-0 header-title">Danh sách kế hoạch đã gửi lên Tỉnh/TP</h4>
                    <div class="text-primary" style="font-size: 14px; font-style: italic">Đã có
                        <strong>{{count($csGuiKH)}}</strong> trên tổng số
                        <strong>{{count($lstCoSo)}}</strong> cơ sở trong tỉnh đã gửi kế hoạch dự trù (Không tính các cơ
                        sở đã bị từ chối
                        kế hoạch)
                    </div>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Ma CSKCB</th>
                            <th>Ten CSKCB</th>
                            <th>Số kế hoạch</th>
                            <th>Thời gian gửi</th>
                            <th>File đính kèm</th>
                            <th>Ghi chú</th>
                            <th>Trạng thái</th>
                            <th>Xử lý</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($session as $stt => $row)
                            <tr>
                                <td class="text-center">{{$stt + 1}}</td>
                                <td class="text-center">{{$row->ma_cskcb}}</td>
                                <td>{{$row->ten_cskcb}}</td>
                                <td>{{$row->cong_van}}</td>
                                <td class="text-right">{{date('H:i:s d/m/Y', strtotime($row->created_at))}}</td>
                                <td>
                                    <ul>
                                        @foreach(json_decode($row->file_attach, true) as $key => $file)
                                            @if(array_key_exists('file', $file))
                                                @if($key == 'file_bao_gia')
                                                    <li>File Báo giá
                                                        <ul>
                                                            @foreach($file['file'] as $fid=>$f)
                                                                <li>
                                                                    <a href="javascript:viewFile('{{route('file-session-cv', ['id' => $row->id, 'key' => $key, 'fid'=>$fid])}}')">{{$f}}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                @else
                                                    <li>
                                                        <a href="javascript:viewFile('{{route('file-session-cv', ['id' => $row->id, 'key' => $key])}}')">{{$file['title']}}</a>
                                                    </li>
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </td>
                                <td class="text-center"><a href="javascript:showGhiChu('{{$row->ghi_chu}}')"><i
                                                class="fa fa-eye"></i></a></td>
                                <td class="text-center {{Config::get('const.session_status_color')[$row->trang_thai]}}">
                                    {{Config::get('const.session_status_label')[$row->trang_thai]}}
                                </td>
                                <td class="text-center"><a href="javascript:showRow('{{$row->id}}')"><i
                                                class="fa fa-list"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.components.modal_preview')
    <div class="modal" id="modal-session-note">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Ghi chú của Tỉnh/TP</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="body-session-note">

                </div>
            </div>
        </div>
    </div>
    @include('prov.plan.modal_confirm')
    <div class="modal" id="modal-ds-cs">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Danh sách cơ sở chưa gửi kế hoạch</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="body-session-note">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Mã CSKCB</th>
                            <th>Tên CSKCB</th>
                            <th>Email</th>
                            <th>Điện thoại</th>
                            <th>Cán bộ đầu mối</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lstCoSo as $row)
                            @if(!in_array($row->ma_cskcb, $csGuiKH))
                                <tr>
                                <td>{{$row->ma_cskcb}}</td>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->dienthoai}}</td>
                                    <td>{{$row->can_bo_dau_moi}}</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showGhiChu(ghichu) {
            $("#body-session-note").html(ghichu)
            $("#modal-session-note").modal("show");
        }

    </script>

@endsection