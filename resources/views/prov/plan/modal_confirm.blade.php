<div class="modal" id="modal-session-row">
    <div class="modal-dialog modal-xxl">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Kế hoạch</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="body-session-row">

            </div>
        </div>
    </div>
</div>
<script>
    function showRow(id) {
        $.ajax({
            url: '{{route('prov-plan-session-row')}}?id=' + id,
            type: 'GET',
            async: true,
            success: function (result) {
                $("#body-session-row").html(result);

                $("#modal-session-row").modal('show');
            },
            error: function () {
                modalMessage('danger', 'Xảy ra lỗi!');
                $(".loader").hide();
            },
        });
    }

    function confirmSession(confirm) {
        var mess = confirm == 1 ? "Bạn muốn xác nhận kế hoạch này?" : "Bạn muốn từ chối kế hoạch này?";
        if (window.confirm(mess)) {
            $.ajax({
                url: '{{route('prov-plan-session-confirm')}}',
                data: {
                    _token: '{{csrf_token()}}',
                    id: $("#session_id_xu_ly").val(),
                    confirm,
                    ghi_chu: $("#input-ghi-chu").val()
                },
                async: true,
                success: function (result) {
                    window.location.reload();
                },
                error: function () {
                    modalMessage('danger', 'Xảy ra lỗi!');
                    $(".loader").hide();
                },
            });
        }
    }
</script>