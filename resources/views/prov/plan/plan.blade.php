@extends('layouts.app')
@section('title')
    Tổng hợp kế hoạch dự trù theo Cơ sở y tế (Mẫu 03)
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="card m-t-20">
                <form class="card-body">
                    <div class="pull-right">
                        <input type="submit" style="margin-left: 20px" class="btn btn-sm btn-success pull-right"
                               value="Tìm kiếm">
                        <a class="btn btn-sm btn-primary" href="{{route(\Request::route()->getName())}}">Xóa tìm
                            kiếm</a>
                    </div>
                    <h4 class="mt-0 header-title">Tìm kiếm</h4>
                    <div class="form-group from-inline form-filter">
                        <label>Cơ sở: </label>
                        <select name="ma_cskcb" class="select-filter-data" data-table="category_site"
                                data-key="ma_cskcb"
                                data-label="ma_cskcb,name" data-search="ma_cskcb,name"
                                data-tinh="{{$tinh->ma_tinh}}">
                            @if(array_key_exists('ma_cskcb', $filter))
                                <option value="{{$site->ma_cskcb}}">{{$site->ma_cskcb}}-{{$site->name}}</option>
                            @endif
                        </select>

                        <label>Thuốc: </label>
                        <select name="ma_thuoc" class="select-filter-data" data-table="category_medicine"
                                data-key="code"
                                data-label="code,name" data-search="code,name">
                            @if(array_key_exists('ma_thuoc', $filter))
                                <option value="{{$thuoc->code}}">{{$thuoc->code}}-{{$thuoc->name}}</option>
                            @endif
                        </select>
                    </div>
                </form>
            </div>
            <div class="card m-b-30 m-t-20 min-h-80">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Tổng hợp kế hoạch dự trù theo Cơ sở y tế (Mẫu 03)</h4>
                    <div class="pull-right" style="margin-top: -40px">
{{--                        <a class="btn btn-success btn-sm pull-right" style="margin-left: 10px" data-toggle="modal"--}}
{{--                           data-target="#modal-plan-submit">Gửi kế hoạch</a>--}}
                        <button class="btn btn-success btn-sm pull-right" style="margin-left: 10px" data-toggle="modal"
                                data-target="#modal-view-mau">Tạo mẫu Công văn
                        </button>
                        <a class="btn btn-sm btn-primary pull-right" href="{{route('prov-plan-export', $filter)}}">Xuất Excel</a>
                    </div>
                    <table class="table table-bordered table-striped" id="table-plan">
                        <thead>
                        <tr>


                            <th rowspan="3">Mã thuốc</th>
                            <th rowspan="3">Tên hoạt chất</th>
                            <th rowspan="3">Nồng độ, hàm lượng</th>
                            <th rowspan="3">Dạng bào chế</th>
                            <th rowspan="3">Đường dùng</th>
                            <th rowspan="3">Nhóm thuốc</th>
                            <th rowspan="3">Đơn vị tính</th>
                            <th rowspan="3" style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo
                                thống kê của
                                CTCLQG
                            </th>
                            <th colspan="{{($plan->year_to + 1 - $plan->year_from) * 4}}">Số lượng dự trù (24 tháng)
                            </th>
                            <th rowspan="3">Tổng số lượng</th>
                            <th rowspan="3">Cơ sở y tế</th>
                        </tr>
                        <tr>
                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                <th colspan="4">{{$year}}</th>
                            @endfor

                        </tr>
                        <tr>
                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                <th>Quý I</th>
                                <th>Quý II</th>
                                <th>Quý III</th>
                                <th>Quý IV</th>
                            @endfor

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="20"><strong>Tổng {{count($data)}} khoản</strong></td>
                        </tr>
                        @foreach($data as $row)
                            <tr>

                                <td>{{$row['ma_thuoc']}}</td>
                                <td>{{$row['ten_hoat_chat']}}</td>
                                <td>{{$row['ham_luong']}}</td>
                                <td>{{$row['dang_bao_che']}}</td>
                                <td>{{$row['duong_dung']}}</td>
                                <td>{{$row['nhom_thuoc']}}</td>
                                <td>{{$row['dvt']}}</td>
                                <td class="text-right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row))}}</td>
                                @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                    @for($q = 1; $q <= 4; $q ++)
                                        <td class="text-right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row))}}</td>
                                    @endfor
                                @endfor
                                <td class="text-right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row))}}</td>
                                <td>{{$row['ten_cskcb']}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if(count($data) > 0)
        @include('prov.plan.modal_cv')
        @include('prov.mau_02.modal_submit')
    @endif

@endsection