@extends('layouts.app')
@section('title')
    Danh sách kế hoạch đã gửi lên TW
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="card m-b-30 m-t-20 min-h-80">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Danh sách kế hoạch đã gửi TW</h4>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Số kế hoạch</th>
                            <th>Thời gian gửi</th>
                            <th>File đính kèm</th>
                            <th>Ghi chú</th>
                            <th>Trạng thái</th>
                            <th>Chi tiết</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($session as $stt => $row)
                            <tr>
                                <td class="text-center">{{$stt + 1}}</td>
                                <td>{{$row->cong_van}}</td>
                                <td class="text-right">{{date('H:i:s d/m/Y', strtotime($row->created_at))}}</td>
                                <td>
                                    <ul>
                                        @foreach(json_decode($row->file_attach, true) as $key => $file)
                                            @if(array_key_exists('file', $file))
                                                <li>
                                                    <a href="javascript:viewFile('{{route('file-session-cv', ['id' => $row->id, 'key' => $key])}}')">{{$file['title']}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </td>
                                <td class="text-center"><a href="javascript:showGhiChu('{{$row->ghi_chu}}')"><i
                                                class="fa fa-eye"></i></a></td>
                                <td class="text-center {{Config::get('const.session_status_color')[$row->trang_thai]}}">
                                    {{Config::get('const.session_status_label')[$row->trang_thai]}}
                                </td>
                                <td class="text-center"><a href="javascript:showRow('{{$row->id}}')"><i
                                                class="fa fa-list"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @include('layouts.components.modal_preview')
        <div class="modal" id="modal-session-note">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Ghi chú của Tỉnh/TP</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="body-session-note">

                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="modal-session-row">
            <div class="modal-dialog modal-xxl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Kế hoạch</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="body-session-row">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function showGhiChu(ghichu) {
            $("#body-session-note").html(ghichu)
            $("#modal-session-note").modal("show");
        }

        function showRow(id) {
            $.ajax({
                url: '{{route('prov-session-sent-row')}}?id=' + id,
                type: 'GET',
                async: true,
                success: function (result) {
                    $("#body-session-row").html(result);
                    $("#modal-session-row").modal('show');
                },
                error: function () {
                    modalMessage('danger', 'Xảy ra lỗi!');
                    $(".loader").hide();
                },
            });
        }
    </script>

@endsection