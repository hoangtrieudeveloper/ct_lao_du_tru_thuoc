@extends('layouts.app')
@section('title')
    Thông tin chi tiết danh mục thuốc
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Thông tin chi tiết danh mục thuốc</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body row view-info">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Mã thuốc : </span><label for="control-label">{{$data->code}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Tên hoạt chất : </span><label for="control-label">{{$data->name}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Dạng tế bào : </span><label for="control-label">{{$data->cell_type}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Nồng độ/Hàm lượng : </span><label for="control-label">{{$data->concentration}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Đường dùng : </span><label for="control-label">{{$data->type}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Nhóm TCKT : </span><label for="control-label">{{$data->group_tckt}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Đơn vị tính : </span><label for="control-label">{{$data->unit}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>CTCL khuyến cáo dự trù : </span><label for="control-label">{{$data->ctcl_planning}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Ngày tạo  : </span><label for="control-label">{{date('Y/m/d',strtotime($data->created_at))}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Ngày cập nhật  : </span><label for="control-label">{{date('Y/m/d',strtotime($data->updated_at))}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class=" button-items">
                                        <a href="{{route("update-categorymedicine")}}?id={{$data->id}}"
                                           class="btn btn-info waves-effect waves-light pull-left">Cập nhật</a>
                                        <a href="{{route('categorymedicine-index')}}"
                                           class="btn btn-secondary waves-effect pull-left text-white">Quay lại</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<style>
    .view-info .form-group span {
        font-size: 13px;
    }
    .view-info .form-group label{
        color: black;
    }
</style>
