@extends('layouts.app')
@section('title')
    Cập nhật danh mục thuốc
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        @if (Session::has('message'))
                            <div class="alert alert-info"
                                 style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                        @endif
                    </div>
                    <div class="page-title-box">
                        <h4 class="page-title">Cập nhật thông tin danh mục thuốc : {{$category_obj->code}}</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body handleData">
                            {!! Form::open(array('route' => 'update-categorymedicine','method' => 'POST')) !!}
                            <input type="hidden" name="id" value="{{$category_obj->id}}">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-2 col-form-label">Mã thuốc</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$category_obj->code}}" name="code" placeholder="Mã thuốc" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Tên hoạt chất</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="search" value="{{$category_obj->name}}" name="name" placeholder="Tên hoạt chất" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Dạng tế bào</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$category_obj->cell_type}}" name="cell_type" placeholder="Dạng tế bào">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Nồng độ/Hàm lượng</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$category_obj->concentration}}" name="concentration" placeholder="Nồng độ/Hàm lượng">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Đường dùng</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$category_obj->type}}" name="type" placeholder="Đường dùng">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Nhóm TCKT</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$category_obj->group_tckt}}" name="group_tckt" placeholder="Nhóm TCKT">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Đơn vị tính</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$category_obj->unit}}" name="unit" placeholder="Đơn vị tính">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">CTCL khuyến cáo dự trù</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$category_obj->ctcl_planning}}" name="ctcl_planning" placeholder="CTCL khuyến cáo dự trù">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10 button-items">
                                    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light pull-right" value="0">Cập nhật</button>
                                    <a href="{{route('categorymedicine-index')}}" class="btn btn-secondary waves-effect pull-right text-white">Quay lại</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
@endsection
<style>
    .handleData .form-group label {
        font-size: 14px;
        font-weight: 500;
        color: black;
    }
</style>
