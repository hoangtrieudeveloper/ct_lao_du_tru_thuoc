@extends('layouts.app')
@section('title')
    Quản lý danh mục thuốc
@endsection
@section('content')
    {!! Form::open(array('route' => 'delete-categorymedicine', 'class' => 'form', 'id' => 'frmrowremove')) !!}
    <input type="hidden" name="row_code" id="row_code">
    {!! Form::close() !!}
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        @if (Session::has('message'))
                            <div class="alert alert-info"
                                 style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                        @endif
                    </div>
                    <div class="page-title-box">
                        <h4 class="page-title mb-2 pull-left">Quản lý danh mục thuốc</h4>
                        <div class="page-title-right">
                            <a href="{{route('create-categorymedicine')}}" class="btn btn-info pull-right">Thêm mới
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30 mt-3">
                        <div class="card-body">
                            <table id="datatable" class="table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th>Mã thuốc</th>
                                    <th>Tên hoạt chất</th>
                                    <th>Dạng tế bào</th>
                                    <th>Nồng độ/Hàm lượng</th>
                                    <th>Đường dùng</th>
                                    <th>Nhóm TCKT</th>
                                    <th>Đơn vị tính</th>
                                    <th>CTCL khuyến cáo dự trù</th>
                                    <th class="text-center">Ngày tạo</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $k => $v)
                                    <tr>
                                        <td class="text-center">{{$k+1}}</td>
                                        <td>{{$v['code']}}</td>
                                        <td>{{$v['name']}}</td>
                                        <td>{{$v['cell_type']}}</td>
                                        <td>{{$v['concentration']}}</td>
                                        <td>{{$v['type']}}</td>
                                        <td>{{$v['group_tckt']}}</td>
                                        <td>{{$v['unit']}}</td>
                                        <td>{{$v['ctcl_planning']}}</td>
                                        <td class="text-center">{{date('Y/m/d',strtotime($v['created_at']))}}</td>
                                        <td class="block-update_user text-center">
                                            <a class="btn btn-info waves-effect waves-light btn-sm mb-2"
                                               href="{{route('view-info-category-medicine')}}?id={{$v->id}}"><i class="mdi mdi-eye"></i>
                                            </a>
                                            <a class="btn btn-success waves-effect waves-light btn-sm mb-2"
                                                    href="{{route('update-categorymedicine')}}?id={{$v->id}}"><i
                                                        class="fa fa-edit admin-action"></i></a>
                                            <button type="button" class="btn btn-danger waves-effect waves-light btn-sm"
                                                    onclick="removeRow({{$v['id']}})"><i
                                                        class="fa fa-trash-o admin-action"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection
@section('script')
    <script language="javascript">
        function removeRow(row_code) {
            if (confirm("Bạn có chắc chắn muốn xóa?")) {
                document.getElementById("row_code").value = row_code;
                frmrowremove.submit();
            }
        }
    </script>
@endsection
