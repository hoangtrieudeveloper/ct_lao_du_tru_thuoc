@extends('layouts.app')
@section('title')
    Danh sách kế hoạch đã gửi lên TW
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="card m-b-30 m-t-20 min-h-80">
                <div class="card-body">
                    <div class="pull-right">
                        <button class="btn btn-primary btn-sm pull-right" style="margin-left: 10px" data-toggle="modal"
                                data-target="#modal-ds-dv">Danh sách đơn vị chưa gửi kế hoạch
                        </button>
                    </div>
                    <h4 class="mt-0 header-title">Danh sách kế hoạch đã gửi TW</h4>
                    <div class="text-primary" style="font-size: 14px; font-style: italic">Đã có
                        <strong>{{count($maTinhSent)}}</strong> trên tổng số
                        <strong>{{count($userTinh)}}</strong> đơn vị cấp Tỉnh/TP đã gửi kế hoạch dự trù lên TW (Không
                        tính các đơn vị đã bị từ chối
                        kế hoạch)
                    </div>
                    <table class="table table-bordered table-striped datatable">
                        <thead>
                        <tr>
                            <th>Mã Tỉnh<br><input></th>
                            <th>Số kế hoạch<br><input></th>
                            <th>Thời gian gửi</th>
                            <th>File đính kèm</th>
                            <th>Ghi chú</th>
                            <th>Người xác nhận</th>
                            <th>Thời gian xác nhận</th>
                            <th>Trạng thái<br><input></th>
                            <th>Chi tiết</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($session as $stt => $row)
                            <tr>
                                <td class="text-center">{{array_key_exists($row->ma_tinh, $dictTinh)?$dictTinh[$row->ma_tinh]:''}}</td>
                                <td>{{$row->cong_van}}</td>
                                <td class="text-right">{{date('H:i:s d/m/Y', strtotime($row->created_at))}}</td>
                                <td>
                                    <ul>
                                        @foreach(json_decode($row->file_attach, true) as $key => $file)
                                            @if(array_key_exists('file', $file))
                                                <li>
                                                    <a href="javascript:viewFile('{{route('file-session-cv', ['id' => $row->id, 'key' => $key])}}')">{{$file['title']}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </td>
                                <td class="text-center"><a href="javascript:showGhiChu('{{$row->ghi_chu}}')"><i
                                                class="fa fa-eye"></i></a></td>
                                <td class="text-center">
                                    {{array_key_exists($row->user_xacnhan_kehoach, $dictUser)?$dictUser[$row->user_xacnhan_kehoach]:''}}
                                </td>
                                <td class="text-right">{{$row->updated_at_xacnhan_kehoach ? date('H:i:s d/m/Y', strtotime($row->updated_at_xacnhan_kehoach)) : ''}}</td>
                                <td class="text-center {{Config::get('const.session_status_color')[$row->trang_thai]}}">
                                    {{Config::get('const.session_status_label')[$row->trang_thai]}}
                                </td>
                                <td class="text-center"><a href="javascript:showRow('{{$row->id}}')"><i
                                                class="fa fa-list"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @include('layouts.components.modal_preview')
        <div class="modal" id="modal-session-note">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Ghi chú của Tỉnh/TP</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="body-session-note">

                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="modal-session-row">
            <div class="modal-dialog modal-xxl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Kế hoạch</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" id="body-session-row">

                        {!! Form::open(array('route' => 'center-xuly-kh', 'class' => 'form', 'id' => 'formXuLyKH')) !!}
                        <input type="hidden" id="input-ses-id" name="id">
                        <input type="hidden" id="input-ses-trang-thai" name="trang_thai">
                        <textarea class="form-control" placeholder="Ghi chú cho đơn vị cấp Tỉnh/TP"
                                  name="note"></textarea>
                        {!! Form::close() !!}
                        <div class="pull-right" style="margin: 10px">
                            <a class="btn btn-sm btn-success" href="javascript:confirmPlan(1)">Xác nhận</a>
                            <a class="btn btn-sm btn-danger" href="javascript:confirmPlan(0)">Từ chối</a>
                        </div>
                        <ul class="nav nav-pills" style="margin: 10px">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" aria-current="page" href="#mau02">Mẫu
                                    02</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#mau03">Mẫu 03</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#mau04">Mẫu 04</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="mau02" role="tabpanel" aria-labelledby="home-tab">...</div>
                            <div class="tab-pane" id="mau03" role="tabpanel" aria-labelledby="profile-tab">...</div>
                            <div class="tab-pane" id="mau04" role="tabpanel" aria-labelledby="messages-tab">...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modal-ds-dv">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Danh sách đơn vị chưa gửi kế hoạch</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="body-session-note">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Mã tỉnh/Mã đơn vị</th>
                            <th>Tên Tỉnh/TP/Đơn vị</th>
                            <th>Email</th>
                            <th>Điện thoại</th>
                            <th>Cán bộ đầu mối</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($userTinh as $row)
                            @if(!in_array($row->ma_tinh, $maTinhSent))
                                <tr>
                                    <td>{{$row->ma_tinh}}</td>
                                    <td>{{array_key_exists($row->ma_tinh, $dictTinh)?$dictTinh[$row->ma_tinh]:''}}</td>
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->dienthoai}}</td>
                                    <td>{{$row->can_bo_dau_moi}}</td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        function showGhiChu(ghichu) {
            $("#body-session-note").html(ghichu)
            $("#modal-session-note").modal("show");
        }

        function showRow(id) {
            $("#loading").show();
            $.ajax({
                url: '{{route('center-prov-session-tw-row')}}?id=' + id,
                type: 'GET',
                async: true,
                success: function (result) {
                    // $("#body-session-row").html(result);
                    $("#mau03").html(result.m03);
                    $("#mau02").html(result.m02);
                    $("#mau04").html(result.m04);
                    $("#input-ses-id").val(id);
                    $("#modal-session-row").modal('show');
                    $("#loading").hide();
                },
                error: function () {
                    modalMessage('danger', 'Xảy ra lỗi!');
                    $("#loading").hide();
                },
            });
        }


        function confirmPlan(xn) {
            var mess = xn == 1 ? 'Chắc chắn muốn xác nhận kế hoạch này?' : 'Chắc chắn muốn từ chối kế hoạch này';
            if (confirm(mess)) {
                $("#input-ses-trang-thai").val(xn);
                $("#formXuLyKH").submit();
            }
        }


    </script>

@endsection