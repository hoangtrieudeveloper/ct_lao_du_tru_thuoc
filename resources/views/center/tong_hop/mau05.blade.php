@extends('layouts.app')
@section('title')
    Tổng hợp kế hoạch mẫu 05
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="card m-b-30 m-t-20 min-h-80">
                <div class="card-body">
                    <div class="pull-right">
                        <a class="btn btn-success btn-sm" href="{{route('center-mau-05-export')}}">Xuất Excel</a>
                    </div>
                    <h4 class="mt-0 header-title">Tổng hợp kế hoạch đã xác nhận theo thuốc (Mẫu 05)</h4>
                    <table class="table-bordered table table-striped">
                        <thead>
                        <tr>
                            <th rowspan="3">Mã thuốc</th>
                            <th rowspan="3">Tên hoạt chất</th>
                            <th rowspan="3">Nồng độ, hàm lượng</th>
                            <th rowspan="3">Dạng bào chế</th>
                            <th rowspan="3">Đường dùng</th>
                            <th rowspan="3">Nhóm thuốc</th>
                            <th rowspan="3">Đơn vị tính</th>
                            <th rowspan="3" style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo
                                thống kê của
                                CTCLQG
                            </th>
                            <th colspan="{{($plan->year_to + 1 - $plan->year_from) * 4}}">Số lượng dự trù (24 tháng)
                            </th>
                            <th rowspan="3">Tổng số lượng</th>
                        </tr>
                        <tr>
                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                <th colspan="4">{{$year}}</th>
                            @endfor

                        </tr>
                        <tr>
                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                <th>Quý I</th>
                                <th>Quý II</th>
                                <th>Quý III</th>
                                <th>Quý IV</th>
                            @endfor

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="20"><strong>Tổng {{count($data)}} khoản</strong></td>
                        </tr>
                        @foreach($data as $row)
                            <tr>
                                <td>{{$row['ma_thuoc']}}</td>
                                <td>{{$row['ten_hoat_chat']}}</td>
                                <td>{{$row['ham_luong']}}</td>
                                <td>{{$row['dang_bao_che']}}</td>
                                <td>{{$row['duong_dung']}}</td>
                                <td>{{$row['nhom_thuoc']}}</td>
                                <td>{{$row['dvt']}}</td>
                                <td class="text-right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row))}}</td>
                                @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                    @for($q = 1; $q <= 4; $q ++)
                                        <td class="text-right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row))}}</td>
                                    @endfor
                                @endfor
                                <td class="text-right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row))}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection