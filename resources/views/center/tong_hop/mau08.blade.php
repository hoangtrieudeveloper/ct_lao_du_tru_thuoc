@extends('layouts.app')
@section('title')
    Thông tin địa bàn hành chính cơ sở y tế
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="card m-t-20">
                <form class="card-body">
                    <div class="pull-right">
                        <input type="submit" style="margin-left: 20px" class="btn btn-sm btn-success pull-right"
                               value="Tìm kiếm">
                        <a class="btn btn-sm btn-primary" href="{{route(\Request::route()->getName())}}">Xóa tìm
                            kiếm</a>
                    </div>
                    <h4 class="mt-0 header-title">Tìm kiếm</h4>
                    <div class="form-group from-inline form-filter">
                        <label>Tỉnh: </label>
                        <select name="ma_tinh" class="select-filter-data" data-table="provinces"
                                data-key="ma_tinh"
                                data-label="ma_tinh,name" data-search="ma_tinh,name">
                            @if(array_key_exists('ma_tinh', $filter))
                                <option value="{{$tinh->ma_tinh}}">{{$tinh->ma_tinh}}-{{$tinh->name}}</option>
                            @endif
                        </select>

                        <label>Cơ sở y tế: </label>
                        <select name="ma_cskcb" class="select-filter-data" data-table="category_site"
                                data-key="ma_cskcb"
                                data-label="ma_cskcb,name" data-search="ma_cskcb,name"
                                data-tinh="{{array_key_exists('ma_tinh', $filter)?$filter['ma_tinh']:''}}">
                            @if(array_key_exists('ma_cskcb', $filter))
                                <option value="{{$site->ma_cskcb}}">{{$site->ma_cskcb}}-{{$site->name}}</option>
                            @endif
                        </select>
                    </div>
                </form>
            </div>
            <div class="card m-b-30 m-t-20 min-h-80">
                <div class="card-body">
                    <div class="pull-right">
                        <a class="btn btn-success btn-sm" href="{{route('center-mau-08-export', $filter)}}">Xuất Excel</a>
                    </div>
                    <h4 class="mt-0 header-title">Thông tin địa bàn hành chính cơ sở y tế</h4>
                    <div class="text-primary">Tổng số {{count($data)}} cơ sở y tế</div>
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th rowspan="2">TT</th>
                            <th rowspan="2">Tỉnh/TP</th>
                            <th rowspan="2">Tên Cơ sở y tế</th>
                            <th rowspan="2">Mã KBCB CSYT</th>
                            <th rowspan="2">Địa chỉ chi tiết của cơ sở y tế</th>
                            <th colspan="3">Thông tin liên hệ</th>
                        </tr>
                        <tr>
                            <th>Cán bộ đầu mối</th>
                            <th>Điện thoại</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $idx => $row)
                            <tr>
                                <td class="text-center">{{$idx + 1}}</td>
                                <td>{{array_key_exists($row->ma_tinh, $dictTinh)?$dictTinh[$row->ma_tinh]:''}}</td>
                                <td>{{$row->name}}</td>
                                <td class="text-center">{{$row->ma_cskcb}}</td>
                                <td>{{$row->address}}</td>
                                <td>{{$row->can_bo_dau_moi}}</td>
                                <td>{{$row->dienthoai}}</td>
                                <td>{{$row->email}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection