<div class="modal" id="modal-session-row">
    <div class="modal-dialog modal-xxl">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="nav nav-tabs">
                    <li style="height: 35px"><a data-toggle="pill" class="active" href="#home" id="firstActive"><h4>Kế hoạch</h4></a></li>
                    <li style="margin-top: 16px;margin-left: 15px;height: 35px"><a data-toggle="pill" href="#menu1" id="lastActive">(Thông tin CSKCB)</a></li>
                </ul>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="body-session-row">

            </div>
        </div>
    </div>
</div>
<script>
    function showRow(id) {
        $.ajax({
            url: '{{route('center-row-session-csyt')}}?id=' + id,
            type: 'GET',
            async: true,
            success: function (result) {
                $('.nav-tabs #firstActive').addClass("active");
                $('.nav-tabs #lastActive').removeClass("active");
                $("#body-session-row").html(result);

                $("#modal-session-row").modal('show');
            },
            error: function () {
                modalMessage('danger', 'Xảy ra lỗi!');
                $(".loader").hide();
            },
        });
    }
</script>
<style>
    .nav-tabs>li>a.active, .nav-tabs>li>a.active:focus, .nav-tabs>li>a.active:hover {
         color: #555;
         cursor: default;
         background-color: #fff;
     }
    .nav-tabs>li>a {
        margin-right: 2px;
    }
    .nav-tabs {
        border: none;
    }
</style>