<div class="modal" role="dialog" id="modal-alert">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div id="modal-alert-mess">Modal body text goes here.</div>
            </div>
            <div class="modal-footer">
                <button id="modal-alert-btn-cancel" type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">
                    OK
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    function modalAlert(mess, opt = {}) {
        var {onCancel, btnCancel} = opt;
        $("#modal-alert-mess").html(mess);

        if (btnCancel) $("#modal-confirm-btn-cancel").html(btnCancel)
        if (onCancel) {
            $("#modal-alert-btn-cancel").click(function () {
                onCancel();
            })
        }

        $("#modal-alert").modal('show');
    }
</script>
<style>
    #modal-alert-mess {
        font-size: 14px;
    }
    #modal-alert .modal-dialog  .modal-content{
        background-color: #eee !important;
    }
</style>