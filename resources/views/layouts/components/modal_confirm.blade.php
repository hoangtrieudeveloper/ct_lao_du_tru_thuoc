<div class="modal" role="dialog" id="modal-confirm">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div id="modal-confirm-mess">Modal body text goes here.</div>
            </div>
            <div class="modal-footer">
                <button id="modal-confirm-btn-cancel" type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Quay lại</button>
                <button id="modal-confirm-btn-confirm" type="button" class="btn btn-primary btn-sm">Xác nhận</button>
            </div>
        </div>
    </div>
</div>
<script>
    function modalConfirm(opt) {
        var {mess, onSubmit, btnConfirm, btnCancel} = opt;
        $("#modal-confirm-mess").html(mess);
        $("#modal-confirm-btn-confirm").click(function () {
            onSubmit();
            $("#modal-confirm").modal('hide');
        });
        if(btnConfirm) $("#modal-confirm-btn-confirm").html(btnConfirm)
        if(btnCancel) $("#modal-confirm-btn-cancel").html(btnCancel)

        $("#modal-confirm").modal('show');
    }
</script>
<style>
    #modal-confirm-mess{
        font-size: 14px;
    }
</style>