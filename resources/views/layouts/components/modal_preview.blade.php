<div class="modal" id="modal-preview-file">
    <div class="modal-dialog modal-xxl">
        <div class="modal-content">
            <div class="modal-header">
                <a class="btn btn-primary btn-sm" id="link-down-file">Tải file về</a>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <iframe id="iframe-view-file" width="100%" style="height: 80vh"></iframe>
            </div>
        </div>
    </div>
</div>
<script>
    function viewFile(url) {
        $("#iframe-view-file").attr("src", url);
        $("#link-down-file").attr('href', url + "&download=true");
        $("#modal-preview-file").modal("show");
    }
</script>