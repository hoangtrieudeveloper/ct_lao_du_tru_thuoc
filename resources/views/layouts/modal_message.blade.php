<div id="modal_message" style="z-index: 9999 !important;" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center" style="margin: auto">

                <h4 class="modal-title-message text-center"></h4>
            </div>
        </div>
    </div>
</div>
<script>
    function modalMessage(type, message) {
        console.log("Message-" + type + ":" + message);
        $('.modal-title-message').html(message);
        $('.modal-title-message').addClass("text-" + type);
        $("#modal_message").modal("show");
        setTimeout(function () {
            $("#modal_message").modal("hide");
        }, 2000);
    }
    @if (Session::has('message'))
    modalMessage("{{Session::get('status')}}", "{{Session::get('message')}}");
    @endif
</script>
