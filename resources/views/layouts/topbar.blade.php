<header id="topnav">
    <div class="topbar-main">
        <div>

            <!-- Logo container-->
            <div class="logo">
                <!-- Image Logo -->
                <a href="/" class="logo-normal">
                    <img src="{{asset('/assets/images/logo_ctlao.png')}}" alt="" style="width: 45px;height: 45px"
                         class="logo-large"> <span class="text-dark" style="font-size: 14px">Bệnh viện Phổi trung ương - Chương trình chống lao QG</span>
                </a>
            </div>
            <!-- End Logo container-->
            <div class="menu-extras topbar-custom">
                <ul class="list-inline float-right mb-0">
                    <!-- User-->
                    <li class="list-inline-item dropdown notification-list">
                        <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown"
                           href="#" role="button"
                           title="{{session('uinfo')->fullname}}"
                           aria-haspopup="false" aria-expanded="false">
                            <span style="color: #000000;font-size: 14px;margin-right: 10px;font-weight: 700;letter-spacing: .03em;text-transform: uppercase;">
                                {{session('uinfo') ? (strlen(session('uinfo')->fullname) >= 40 ? substr(session('uinfo')->fullname, 0, 40) . "..." : session('uinfo')->fullname) : ''}}</span>
                            <img src="{!! session('uinfo')->avatar ? session('uinfo')->avatar : asset('/assets/images/users/user.png')   !!}"
                                 alt="user" class="rounded-circle">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown "
                             style="width: 250px !important;">
                            <!-- item-->
                            @if(Session::get('uinfo')->group_scope == 3)
                                <a class="dropdown-item" href="{{route('update-view-info')}}"><i
                                            class="mdi mdi-account-circle m-r-5 text-muted"></i>Cập nhật thông tin cơ sở</a>
                            @endif
                            @if(Session::get('uinfo')->group_scope == 2)
                                <a class="dropdown-item" href="{{route('update-view-info-tinh')}}"><i
                                            class="mdi mdi-account-circle m-r-5 text-muted"></i>Cập nhật thông tin</a>
                            @endif
                            <a class="dropdown-item" href="{{route('change-password')}}"><i
                                        class="mdi mdi-account-circle m-r-5 text-muted"></i>Đổi
                                mật khẩu</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/security/logout"><i
                                        class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                        </div>
                    </li>
                    <li class="menu-item list-inline-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle nav-link">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>

                </ul>
            </div>
            <!-- end menu-extras -->

            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <!-- MENU Start -->
    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    @if(Session::get('uinfo')->group_scope == 1)
                        @if(\App\Scopes::checkAccess('user-index'))
                            <li class="has-submenu">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account"></i>
                                    <span> Thành viên</span></a>
                                <ul class="submenu">
                                    <li><a href="{{route("user-index")}}">Quản lý thành viên</a></li>
                                    @if(\App\Scopes::checkAccess('group-user-index'))
                                        <li><a href="{{route("group-user-index")}}">Quản lý Nhóm</a></li>
                                    @endif
                                    @if(\App\Scopes::checkAccess('User-dept-index'))
                                        <li><a href="{{route("User-dept-index")}}">Quản phòng ban</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        <li class="has-submenu">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-code-not-equal"></i>
                                <span> Danh mục</span></a>
                            <ul class="submenu">
                                @if(\App\Scopes::checkAccess('categorysite-index'))
                                    <li><a href="{{route("categorysite-index")}}">Quản lý site</a></li>
                                @endif
                                @if(\App\Scopes::checkAccess('categorymedicine-index'))
                                    <li><a href="{{route("categorymedicine-index")}}">Quản thuốc</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif
                        @if(Session::get('uinfo')->group_scope == 4)
                        <li class="has-submenu">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-code-not-equal"></i>
                                <span> Theo dõi kế hoạch</span></a>
                            <ul class="submenu">
                                <li><a href="{{route("center-prov-session-tw")}}">Danh sách kế hoạch Tỉnh/TP gửi lên TW</a></li>
                                <li><a href="{{route("center-csyt-session-tw")}}">Danh sách kế hoạch CSYT gửi lên Tỉnh/TP</a></li>
                            </ul>
                        </li>
                            <li class="has-submenu">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-code-not-equal"></i>
                                    <span> Tổng hợp kế hoạch đã xác nhận</span></a>
                                <ul class="submenu">
                                    <li><a href="{{route("center-mau-05")}}">(Mẫu 05) Tổng hợp kế hoạch đã xác nhận theo thuốc</a></li>
                                    <li><a href="{{route("center-mau-06")}}">(Mẫu 06) Tổng hợp kế hoạch đã xác nhận theo Tỉnh/TP</a></li>
                                    <li><a href="{{route("center-mau-07")}}">(Mẫu 07) Tổng hợp kế hoạch đã xác nhận theo Cơ sở y tế</a></li>
                                    <li><a href="{{route("center-mau-08")}}">(Mẫu 08) Thông tin hành chính các Cơ sở y tế</a></li>
                                </ul>
                            </li>
                        @endif
                    @if(Session::get('uinfo')->group_scope == 2)
                        <li class="has-submenu">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-code-not-equal"></i>
                                <span> Tỉnh/Thành phố</span></a>
                            <ul class="submenu">
                                <li><a href="{{route("prov-csyt-session")}}">Danh sách kế hoạch CSYT gửi lên</a></li>
                                <li><a href="{{route("prov-plan-by-medicine")}}">Tổng hợp kế hoạch dự trù theo thuốc
                                        (Mẫu 02)</a></li>
                                <li><a href="{{route("prov-plan")}}">Tổng hợp kế hoạch dự trù theo Cơ sở y tế (Mẫu
                                        03)</a></li>
                                <li><a href="{{route("prov-info-site")}}">Thông tin hành chính Cơ sở y tế (Mẫu
                                        04)</a></li>
                                <li><a href="{{route("prov-session-sent")}}">Danh sách kế hoạch đã gửi TW</a></li>
                            </ul>
                        </li>
                    @endif
                    @if(Session::get('uinfo')->group_scope == 3)
                        <li class="has-submenu">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-code-not-equal"></i>
                                <span> Cơ sở y tế</span></a>
                            <ul class="submenu">
                                <li><a href="{{route("csyt-plan-index")}}">Kế hoạch dự trù</a></li>
                                <li><a href="{{route("csyt-plan-session")}}">Danh sách kế hoạch đã gửi</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>
<div style="clear: both"></div>
