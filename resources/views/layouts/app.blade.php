<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title')</title>
    <meta content="Admin" name="description"/>
    <meta content="CTLao" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="shortcut icon" href="{!! asset('/assets/images/logo_ctlao.png') !!}">
    <!-- DataTables -->
    <link href="/hozizontal/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/hozizontal/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/hozizontal/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- End Responsive datatable examples -->

    <!-- jQuery  -->
    <script src="/hozizontal/assets/js/jquery.min.js"></script>
    <script src="/hozizontal/assets/js/popper.min.js"></script>
    <script src="/hozizontal/assets/js/bootstrap.min.js"></script>
    <script src="/hozizontal/assets/js/modernizr.min.js"></script>
    <script src="/hozizontal/assets/js/waves.js"></script>
    <script src="/hozizontal/assets/js/jquery.slimscroll.js"></script>
    <script src="/hozizontal/assets/js/jquery.nicescroll.js"></script>
    <script src="/hozizontal/assets/js/jquery.scrollTo.min.js"></script>

    <script src="/hozizontal/assets/plugins/skycons/skycons.min.js"></script>
    <script src="/hozizontal/assets/plugins/raphael/raphael-min.js"></script>
    <script src="/hozizontal/assets/plugins/morris/morris.min.js"></script>

    <script src="/hozizontal/assets/pages/dashborad.js"></script>
    <script src="/hozizontal/assets/js/select2.js"></script>
    <!-- Required datatable js -->
    <script src="/hozizontal/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/hozizontal/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="/hozizontal/assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="/hozizontal/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Datatable init js -->
    <script src="/hozizontal/assets/pages/datatables.init.js"></script>
    <script src="/hozizontal/assets/js/custom.js"></script>
    <!-- App js -->
    <script src="/hozizontal/assets/js/app.js"></script>
    <script src="/js/main.js"></script>

    <!-- Dropzone js -->
    <script src="{!! asset('/assets/plugins/dropzone/dist/dropzone.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/dropify/js/dropify.js') !!}"></script>
    <link href="{!! asset('/assets/plugins/dropzone/dist/dropzone.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('/assets/plugins/dropify/css/dropify.min.css') !!}" rel="stylesheet">

    <!-- Sweet-Alert  -->
    <script src="{!! asset('/assets/plugins/sweet-alert2/sweetalert2.min.js') !!}"></script>

    <link href="/hozizontal/assets/plugins/morris/morris.css" rel="stylesheet">
    <link href="/hozizontal/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/hozizontal/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="/hozizontal/assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="/hozizontal/assets/css/select2.css" rel="stylesheet" type="text/css">

    <link href="/css/custom.css" rel="stylesheet" type="text/css">

    <!-- Required datatable js -->
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="/assets/plugins/datatables/jszip.min.js"></script>
    <script src="/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="/assets/plugins/datatables/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="/assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

    <!-- Datatable init js -->
    <script src="/assets/pages/datatables.init.js"></script>
    <!-- DataTables -->
    <link href="/assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="/assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner"></div>
    </div>
</div>
<!-- Navigation Bar-->
@include("layouts/topbar")
<!-- End Navigation Bar-->
<!-- wrapper -->
@yield('content')
<div id="loading">
    <p id="loading-image"><div class="loader"></div></p>
</div>
<!-- end wrapper -->

<!-- Footer -->
{{--<footer class="footer">--}}
{{--    <div class="container-fluid">--}}
{{--        <div class="row">--}}
{{--            <div class="col-12">--}}
{{--                © 2018 Annex by Mannatthemes.--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</footer>--}}
<!-- End Footer -->
<div id="modal_message" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-modal="true"
     role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <h5 class="modal-title-message text-center text-dark text-center modal-title p-3 fw-bold"
                id="myModalLabel"></h5>
        </div>
    </div>
</div>
@include("layouts/modal_message")
@include("layouts.components.modal_confirm")
@include("layouts.components.modal_alert")

@yield('script')
<script>
    //show hiden loader
    //$('#loading').hide(); $('#loading').show();
    $(window).on('load', function () {
       setTimeout(function (){
           $('#loading').hide();
       },300)
    })

    $('.btn-animation').on('click', function (br) {
        //adding animation
        $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + $(this).data("animation") + '  animated');
    });
    /* BEGIN SVG WEATHER ICON */
    if (typeof Skycons !== 'undefined') {
        var icons = new Skycons(
            {"color": "#fff"},
            {"resizeClear": true}
            ),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);
        icons.play();
    }
    ;

    // scroll

    $(document).ready(function () {

        $("#boxscroll").niceScroll({cursorborder: "", cursorcolor: "#cecece", boxzoom: true});
        $("#boxscroll2").niceScroll({cursorborder: "", cursorcolor: "#cecece", boxzoom: true});

    });
</script>


</body>
</html>
