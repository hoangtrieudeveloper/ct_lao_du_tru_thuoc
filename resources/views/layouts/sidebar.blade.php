<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="/" class="logo"><img
                        src="{{asset('/assets/images/logo_ctlao.png')}}"
                        alt="user" class="rounded-circle"
                        style="width: 45px;height: 45px"> CT Lao
            </a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">
            <ul>
                @if(Session::get('uinfo')->group_scope == 1)
                    @if(\App\Scopes::checkAccess('user-index'))
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account"></i>
                                <span> Thành viên</span> <span class="float-right"><i
                                            class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="{{route("user-index")}}">Quản lý thành viên</a></li>
                                @if(\App\Scopes::checkAccess('group-user-index'))
                                    <li><a href="{{route("group-user-index")}}">Quản lý Nhóm</a></li>
                                @endif
                                @if(\App\Scopes::checkAccess('User-dept-index'))
                                    <li><a href="{{route("User-dept-index")}}">Quản phòng ban</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-code-not-equal"></i>
                            <span> Danh mục</span> <span class="float-right"><i
                                        class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            @if(\App\Scopes::checkAccess('categorysite-index'))
                                <li><a href="{{route("categorysite-index")}}">Quản lý site</a></li>
                            @endif
                            @if(\App\Scopes::checkAccess('categorymedicine-index'))
                                <li><a href="{{route("categorymedicine-index")}}">Quản thuốc</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
                    @if(Session::get('uinfo')->group_scope == 2)
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-code-not-equal"></i>
                                <span> Tỉnh/Thành phố</span> <span class="float-right"><i
                                            class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="list-unstyled">
                                <li><a href="{{route("prov-csyt-session")}}">Danh sách kế hoạch CSYT gửi lên</a></li>
                                <li><a href="{{route("prov-plan")}}">Tổng hợp kế hoạch đã xác nhận</a></li>
                                <li><a href="{{route("prov-session-sent")}}">Danh sách kế hoạch đã gửi TW</a></li>
                            </ul>
                        </li>
                    @endif
                @if(Session::get('uinfo')->group_scope == 3)
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-code-not-equal"></i>
                            <span> Cơ sở y tế</span> <span class="float-right"><i
                                        class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{route("csyt-plan-index")}}">Kế hoạch dự trù</a></li>
                            <li><a href="{{route("csyt-plan-session")}}">Danh sách kế hoạch đã gửi</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
