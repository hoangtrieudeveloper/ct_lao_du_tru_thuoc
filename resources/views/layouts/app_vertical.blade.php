<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title')</title>
    <meta content="Dashboard" name="description"/>
    <meta content="Mannatthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- jQuery  -->
    <script src="{!! asset('/assets/js/jquery.min.js') !!}"></script>
    <script src="{!! asset('/assets/js/popper.min.js') !!}"></script>
    <script src="{!! asset('/assets/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('/assets/js/modernizr.min.js') !!}"></script>
    <script src="{!! asset('/assets/js/detect.js') !!}"></script>
    <script src="{!! asset('/assets/js/fastclick.js') !!}"></script>
    <script src="{!! asset('/assets/js/jquery.slimscroll.js') !!}"></script>
    <script src="{!! asset('/assets/js/jquery.blockUI.js') !!}"></script>
    <script src="{!! asset('/assets/js/waves.js') !!}"></script>
    <script src="{!! asset('/assets/js/jquery.nicescroll.js') !!}"></script>
    <script src="{!! asset('/assets/js/jquery.scrollTo.min.js') !!}"></script>

    <script src="{!! asset('/assets/plugins/skycons/skycons.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/raphael/raphael-min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/morris/morris.min.js') !!}"></script>

    <script src="{!! asset('/assets/pages/dashborad.js') !!}"></script>
    <script src="{!! asset('/assets/js/select2.js') !!}"></script>
    <!-- Required datatable js -->
    <script src="{!! asset('/assets/plugins/datatables/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/dataTables.bootstrap4.min.js') !!}"></script>
    <!-- Buttons examples -->
    <script src="{!! asset('/assets/plugins/datatables/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/buttons.bootstrap4.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/jszip.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/pdfmake.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/vfs_fonts.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/buttons.html5.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/buttons.print.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/buttons.colVis.min.js') !!}"></script>
    <!-- Responsive examples -->
    <script src="{!! asset('/assets/plugins/datatables/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/datatables/responsive.bootstrap4.min.js') !!}"></script>

    <!-- Datatable init js -->
    <script src="{!! asset('/assets/pages/datatables.init.js') !!}"></script>
    <!-- App js -->
    <script src="{!! asset('/assets/js/custom.js') !!}"></script>
    <link href="{!! asset('/css/custom.css') !!}" rel="stylesheet"
          type="text/css"/>

    <link href="{!! asset('/assets/plugins/datatables/dataTables.bootstrap4.min.css') !!}" rel="stylesheet"
          type="text/css"/>
    <link href="{!! asset('/assets/plugins/datatables/buttons.bootstrap4.min.css') !!}" rel="stylesheet"
          type="text/css"/>
    <link href="{!! asset('/assets/plugins/datatables/responsive.bootstrap4.min.css') !!}" rel="stylesheet"
          type="text/css"/>
    <link rel="shortcut icon" href="{!! asset('/assets/images/logo_ctlao.png') !!}">
    <link href="{!! asset('/assets/plugins/morris/morris.css') !!}" rel="stylesheet">
    <link href="{!! asset('/assets/plugins/animate/animate.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('/assets/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('/assets/css/icons.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('/assets/css/style.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('/assets/css/select2.css') !!}" rel="stylesheet" type="text/css">

    <!-- Dropzone js -->
    <script src="{!! asset('/assets/plugins/dropzone/dist/dropzone.js') !!}"></script>
    <script src="{!! asset('/assets/plugins/dropify/js/dropify.min.js') !!}"></script>
    <link href="{!! asset('/assets/plugins/dropzone/dist/dropzone.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('/assets/plugins/dropify/css/dropify.min.css') !!}" rel="stylesheet">

    <!-- Sweet-Alert  -->
    <script src="{!! asset('/assets/plugins/sweet-alert2/sweetalert2.min.js') !!}"></script>

    <!-- Edittable -->
    <script src="assets/plugins/tiny-editable/mindmup-editabletable.js"></script>
    <script src="assets/plugins/tiny-editable/numeric-input-example.js"></script>
    <script src="assets/plugins/tabledit/jquery.tabledit.js"></script>

</head>


<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner"></div>
    </div>
</div>

<!-- Begin page -->
<div id="wrapper">

    <!-- ========== Left Sidebar Start ========== -->
@include("layouts/sidebar")
@include("layouts/modal_message")
<!-- Left Sidebar End -->

    <!-- Start right Content here -->

    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <!-- Top Bar Start -->
        @include("layouts/topbar")
        <!-- Top Bar End -->

            @yield('content')

        </div> <!-- content -->

        {{--        <footer class="footer">--}}
        {{--            © 2021.--}}
        {{--        </footer>--}}

    </div>
    <!-- End Right content here -->

</div>
<!-- END wrapper -->

<div id="modal_message" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-modal="true"
     role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <h5 class="modal-title-message text-center text-dark text-center modal-title p-3 fw-bold"
                id="myModalLabel"></h5>
        </div>
    </div>
</div>
<script src="{!! asset('/assets/js/app.js') !!}"></script>
@yield('script')
<script>
    $('.btn-animation').on('click', function (br) {
        //adding animation
        $('.modal .modal-dialog').attr('class', 'modal-dialog  ' + $(this).data("animation") + '  animated');
    });
    /* BEGIN SVG WEATHER ICON */
    if (typeof Skycons !== 'undefined') {
        var icons = new Skycons(
            {"color": "#fff"},
            {"resizeClear": true}
            ),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);
        icons.play();
    }
    ;
    // scroll
    $(document).ready(function () {
        $("#boxscroll").niceScroll({cursorborder: "", cursorcolor: "#cecece", boxzoom: true});
        $("#boxscroll2").niceScroll({cursorborder: "", cursorcolor: "#cecece", boxzoom: true});
    });

</script>

</body>
</html>
