@extends('layouts.app')
@section('title')
    Thông tin chi tiết danh mục Site
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Thông tin chi tiết danh mục Site</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body row view-info">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Mã Tỉnh/TP : </span> <label
                                                for="control-label">{{array_key_exists($data->ma_tinh, $dictProvince)?$dictProvince[$data->ma_tinh]['ma_tinh']:''}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Mã CSKCB : </span> <label
                                                for="control-label">{{$data->ma_cskcb}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Tỉnh/TP : </span><label
                                                for="control-label">{{array_key_exists($data->ma_tinh, $dictProvince)?$dictProvince[$data->ma_tinh]['name']:''}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Quận/Huyện : </span><label
                                                for="control-label">{{array_key_exists($data->district_id, $dictDistrict)?$dictDistrict[$data->district_id]:''}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Tên : </span><label for="control-label">{{$data->name}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Địa chỉ : </span><label for="control-label">{{$data->address}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Ngày tạo  : </span><label for="control-label">{{date('Y/m/d',strtotime($data->created_at))}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <span>Ngày cập nhật  : </span><label for="control-label">{{date('Y/m/d',strtotime($data->updated_at))}}</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class=" button-items">
                                        <a href="{{route("update-categorysite")}}?id={{$data->id}}"
                                           class="btn btn-info waves-effect waves-light pull-left">Cập nhật</a>
                                        <a href="/categorysite"
                                           class="btn btn-secondary waves-effect pull-left text-white">Quay lại</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<style>
    .view-info .form-group span {
        font-size: 13px;
    }
    .view-info .form-group label{
        color: black;
    }
</style>
