@extends('layouts.app')
@section('title')
    Quản lý danh mục site
@endsection
@section('content')
    {!! Form::open(array('route' => 'delete-categorysite', 'class' => 'form', 'id' => 'frmrowremove')) !!}
    <input type="hidden" name="row_code" id="row_code">
    {!! Form::close() !!}
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        @if (Session::has('message'))
                            <div class="alert alert-info"
                                 style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                        @endif
                    </div>
                    <div class="page-title-box">
                        <h4 class="page-title mb-2 pull-left">Quản lý danh mục site</h4>
                        <div class="page-title-right">
                            <a href="/categorysite/addcategorysite" class="btn btn-info pull-right">Thêm mới
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30 mt-3">
                        <div class="card-body">
                            <table class="table table-striped table-bordered datatable" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th>Tỉnh/TP<br><input></th>
                                    <th>Quận/Huyện</th>
                                    <th>Mã Tỉnh/TP<br><input></th>
                                    <th>Mã CSKCB<br><input></th>
                                    <th>Tên<br><input></th>
                                    <th>Địa chỉ</th>
                                    <th class="text-center">Ngày tạo</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $k => $v)
                                    <tr>
                                        <td class="text-center">{{$k+1}}</td>
                                        <td>{{array_key_exists($v->ma_tinh, $dictProvince)?$dictProvince[$v->ma_tinh]['name']:''}}</td>
                                        <td>{{array_key_exists($v->district_id, $dictDistrict)?$dictDistrict[$v->district_id]:''}}</td>
                                        <td>{{array_key_exists($v->ma_tinh, $dictProvince)?$dictProvince[$v->ma_tinh]['ma_tinh']:''}}</td>
                                        <td>{{$v['ma_cskcb']}}</td>
                                        <td>{{$v['name']}}</td>
                                        <td>{{$v['address']}}</td>
                                        <td class="text-center">{{date('Y/m/d',strtotime($v['created_at']))}}</td>
                                        <td class="block-update_user text-center">
                                            <a class="btn btn-info waves-effect waves-light btn-sm mb-2"
                                               href="{{route('view-info-category-site')}}?id={{$v->id}}"><i class="mdi mdi-eye"></i>
                                            </a>
                                            <a class="btn btn-success waves-effect waves-light btn-sm mb-2"
                                                    href="{{route('update-categorysite')}}?id={{$v->id}}"><i
                                                        class="fa fa-edit admin-action"></i></a>
                                            <button type="button" class="btn btn-danger waves-effect waves-light btn-sm"
                                                    onclick="removeRow({{$v['id']}})"><i
                                                        class="fa fa-trash-o admin-action"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection
@section('script')
    <script language="javascript">
        function removeRow(row_code) {
            if (confirm("Bạn có chắc chắn muốn xóa?")) {
                document.getElementById("row_code").value = row_code;
                frmrowremove.submit();
            }
        }
    </script>
@endsection
