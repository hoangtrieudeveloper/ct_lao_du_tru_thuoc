@extends('layouts.app')
@section('title')
    Thêm mới danh mục site
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        @if (Session::has('message'))
                            <div class="alert alert-info"
                                 style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                        @endif
                    </div>
                    <div class="page-title-box">
                        <h4 class="page-title">Thêm mới danh mục site</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body handleData">
                            {!! Form::open(array('route' => 'create-categorysite','method' => 'POST')) !!}
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-2 col-form-label">Mã</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="" name="ma_cskcb" placeholder="Mã" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Tên</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="search" value="" name="name" placeholder="Tên" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Địa chỉ</label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="" name="address" placeholder="Địa chỉ" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tỉnh/TP</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" id="tinhtp" name="ma_tinh" required>
                                        <option value="">Chọn Tỉnh/TP</option>
                                        @foreach($list_province as $v)
                                            <option value="{{$v->ma_tinh}}">{{$v->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
{{--                            <div class="form-group row">--}}
{{--                                <label class="col-sm-2 col-form-label">Tỉnh/TP</label>--}}
{{--                                <div class="col-sm-10">--}}
{{--                                    <select class="custom-select" id="tinhtp" onchange="checkQuanHuyen()" name="province_id" required>--}}
{{--                                        <option value="">Chọn Tỉnh/TP</option>--}}
{{--                                        @foreach($list_province as $v)--}}
{{--                                            <option value="{{$v->id}}">{{$v->name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group row">--}}
{{--                                <label class="col-sm-2 col-form-label">Quận/Huyện</label>--}}
{{--                                <div class="col-sm-10">--}}
{{--                                    <select class="custom-select district" name="district_id">--}}
{{--                                        <option value="">Chọn Quận/Huyện</option>--}}
{{--                                        @foreach($listDictrict as $v)--}}
{{--                                            <option value="{{$v->id}}" style="display: none" class="quanhuyen_{{$v->province_id}}">{{$v->name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10 button-items">
                                    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light pull-right" value="0">Thêm mới</button>
                                    <a href="/categorysite" class="btn btn-secondary waves-effect pull-right text-white">Quay lại</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
@endsection
<style>
    .handleData .form-group label {
        font-size: 14px;
        font-weight: 500;
        color: black;
    }
</style>

