@extends('layouts.app')
@section('title')
    Import dữ liệu kế hoạch dự trù
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="card m-b-30 m-t-20">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Upload dữ liệu dự trù(Excel)</h4>
                    {!! Form::open(array('route' => 'csyt-plan-import', 'files' => 'true')) !!}
                    <div class="pull-right" style="margin-top: -40px">
                        <button class="btn btn-success btn-sm pull-right" style="margin-left: 10px">Tải file lên
                        </button>
                        <a class="btn btn-primary btn-sm pull-right" href="/bieumau/Biểu mẫu dự trù CSYT_20210530.xlsx">Tải
                            file
                            mẫu</a>
                    </div>
                    <input type="file" name="file" id="input-file-now-custom-2" class="dropify" data-height="100"
                           required accept=".xls,.xlsx"/>
                    {!! Form::close() !!}
                </div>
            </div>
            @if($method == 'POST')
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(array('route' => 'csyt-submit-import', 'id'  => 'form-submit-import')) !!}
                        <h4 class="mt-0 header-title">Kết quả import</h4>
                        <div class="text-danger"
                             style="font-style: italic; font-size: 14px; margin-bottom: 10px; padding-right: 150px"
                             id="note-import-error"></div>
                        <div class="pull-right" style="margin-top: -40px">
                            <a class="btn btn-sm btn-success" href="javascript:submitImport()">Xác nhận đẩy dữ liệu</a>
                        </div>
                        <div class="table-rep-plugin">
                            <div class="table-responsive b-0" data-pattern="priority-columns">
                                @include('csyt.plan.import_table')
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script>
        function countError() {
            var numberError = $(".dot-error").length;
            console.log({numberError});
            if (numberError > 0) {
                $("#note-import-error").html('Hiện đang có dự trù của <strong>' + numberError + '</strong>' +
                    ' thuốc chưa hợp lệ. Những thuốc này khi <strong>Xác nhận đẩy dữ liệu</strong> ' +
                    'sẽ <strong>không được ghi nhận</strong> trong kế hoạch dự trù của CSYT. Vui lòng kiểm tra kĩ trước khi đẩy dữ liệu.');
            } else {
                $("#note-import-error").html('');
            }
        }

        function submitImport() {
            var numberError = $(".dot-error").length;
            var mess = "Bạn đã kiểm tra kĩ và chắc chắn muốn xác nhận dữ liệu này?";
            if (numberError > 0) {
                mess = "Hiện đang có " + numberError + " dòng dữ liệu chưa chính xác. Các dòng này sẽ bị xóa khi các bạn đồng ý đẩy dữ liệu. Bạn chắc chắn muốn xóa và đẩy dữ liệu hay xem lại?";
            }
            modalConfirm({
                mess,
                btnConfirm: "Xóa và đẩy dữ liệu",
                btnCancel: "Xem lại",
                onSubmit: function () {
                    $("#form-submit-import").submit();
                }
            })
            // if (confirm(mess)) {
            //     $("#form-submit-import").submit();
            // }
        }

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip()
            $('.dropify').dropify();
            countError();

// Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

// Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function (event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function (event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function (event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function (e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
@endsection
