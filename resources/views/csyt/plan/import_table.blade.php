<table class="table table-bordered">
    <thead>
    <tr>
        <th rowspan="3"></th>
        <th rowspan="3">STT</th>
        <th rowspan="3" style="min-width: 105px">Mã thuốc</th>
        <th rowspan="3" style="min-width: 200px">Tên hoạt chất</th>
        <th rowspan="3" style="min-width: 105px">Nồng độ, hàm lượng</th>
        <th rowspan="3">Dạng bào chế</th>
        <th rowspan="3">Đường dùng</th>
        <th rowspan="3" style="min-width: 70px">Nhóm thuốc</th>
        <th rowspan="3">Đơn vị tính</th>
        <th rowspan="3" style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến
            31/12/2020 theo thống kê của
            CTCLQG
        </th>
        <th colspan="8">Số lượng dự trù (24 tháng)</th>
        <th rowspan="3">Tổng số lượng</th>
        <th rowspan="3" style="width: 60px"></th>
    </tr>
    <tr>
        <th colspan="4">2022</th>
        <th colspan="4">2023</th>
    </tr>
    <tr>
        <th>Quý I</th>
        <th>Quý II</th>
        <th>Quý III</th>
        <th>Quý IV</th>
        <th>Quý I</th>
        <th>Quý II</th>
        <th>Quý III</th>
        <th>Quý IV</th>
    </tr>
    </thead>
    <tbody>
    @foreach($dataExcel as $idx => $row)
        <tr id="row-{{$idx}}">
            {!! \App\Plan::rowInport($row, $idx) !!}
        </tr>
    @endforeach
    </tbody>
</table>
<script>
    function removeRow(id) {
        if(confirm("Bạn chắc chắn muốn xóa dòng này?")){
            $(`tr#row-${id}`).remove();
        }
    }

    function enableEdit(id) {
        $(`tr#row-${id} > td > input`).prop('readonly', false);
        $(`tr#row-${id} > td > a.table-action.text-primary`).attr('href', `javascript:submitRow('${id}')`);
        $(`tr#row-${id} > td > a.table-action.text-primary`).html('<i class="fa fa-check"></i>');
    }

    function submitRow(id) {
        var data = {
            '_token': '{{csrf_token()}}',
            idx: id,
            'stt': $(`tr#row-${id} > td.td-stt`).html(),
            'ma_thuoc': $(`tr#row-${id} > td > input[name="ma_thuoc[]"]`).val(),
            'ten_hoat_chat': $(`tr#row-${id} > td > input[name="ten_hoat_chat[]"]`).val(),
            'ham_luong': $(`tr#row-${id} > td > input[name="ham_luong[]"]`).val(),
            'dang_bao_che': $(`tr#row-${id} > td > input[name="dang_bao_che[]"]`).val(),
            'duong_dung': $(`tr#row-${id} > td > input[name="duong_dung[]"]`).val(),
            'nhom_thuoc': $(`tr#row-${id} > td > input[name="nhom_thuoc[]"]`).val(),
            'dvt': $(`tr#row-${id} > td > input[name="dvt[]"]`).val(),
            'sudung': $(`tr#row-${id} > td > input[name="sudung[]"]`).val(),
            '20221': $(`tr#row-${id} > td > input[name="20221[]"]`).val(),
            '20222': $(`tr#row-${id} > td > input[name="20222[]"]`).val(),
            '20223': $(`tr#row-${id} > td > input[name="20223[]"]`).val(),
            '20224': $(`tr#row-${id} > td > input[name="20224[]"]`).val(),
            '20231': $(`tr#row-${id} > td > input[name="20231[]"]`).val(),
            '20232': $(`tr#row-${id} > td > input[name="20232[]"]`).val(),
            '20233': $(`tr#row-${id} > td > input[name="20233[]"]`).val(),
            '20234': $(`tr#row-${id} > td > input[name="20234[]"]`).val(),
            'tongso': $(`tr#row-${id} > td > input[name="tongso[]"]`).val(),
        }
        console.log(data);
        $.ajax({
            url: '{{route('csyt-check-row')}}',
            type: 'POST',
            data: data,
            async: true,
            success: function (result) {
                $(`tr#row-${id}`).html(result.view);
                $('[data-toggle="tooltip"]').tooltip();
                countError();
            },
            error: function () {
                modalMessage('danger', 'Xảy ra lỗi!');
            },
        });
    }
</script>