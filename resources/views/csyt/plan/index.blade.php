@extends('layouts.app')
@section('title')
    Dữ liệu kế hoạch dự trù 2022 - 2023
@endsection
@section('content')
    {!! Form::open(array('route' => 'delete-plan', 'class' => 'form', 'id' => 'frmrowremove')) !!}
    <input type="hidden" name="ma_thuoc_delete" id="ma_thuoc_delete">
    <input type="hidden" name="ma_cskcb_delete" id="ma_cskcb_delete">
    {!! Form::close() !!}
    <div class="wrapper">
        <div class="container-fluid">
            <div class="card m-b-30 m-t-20 min-h-80">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Dữ liệu kế hoạch dự trù 2022 - 2023</h4>
                    @if($hasGiaiTrinh)
                        <div style="font-style: italic" class="text-warning m-b-10"><i class="fa fa-circle"></i> Kế
                            hoạch vượt quá <strong>130%</strong> hoặc thấp hơn <strong>70%</strong> so với <strong>số
                                lượng
                                sử dụng từ 01/01/2019 đến 31/12/2020 theo thống kê của CTCLQG</strong><br>
                            (Cần đính kèm công văn giải trình khi gửi kế hoạch lên Tỉnh/TP)
                        </div>
                    @endif

                    <div class="pull-right" style="margin-top: -40px">
                        <a class="btn btn-success btn-sm pull-right" style="margin-left: 10px" data-toggle="modal"
                           data-target="#modal-plan-submit">Gửi kế hoạch</a>
                        <button class="btn btn-success btn-sm pull-right" style="margin-left: 10px" data-toggle="modal"
                                data-target="#modal-view-mau">Tạo mẫu Công văn
                        </button>
                        <a class="btn btn-primary btn-sm pull-right" href="{{route('csyt-plan-import')}}">Tải file
                            lên</a>
                        <a class="btn btn-success btn-sm pull-right mr-2" data-toggle="modal"
                           data-target="#myModal_create">Tạo mới</a>
                    </div>
                    <table class="table table-bordered table-striped" id="table-plan">
                        <thead>
                        <tr>
                            <th rowspan="3">Mã thuốc</th>
                            <th rowspan="3">Tên hoạt chất</th>
                            <th rowspan="3">Nồng độ, hàm lượng</th>
                            <th rowspan="3">Dạng bào chế</th>
                            <th rowspan="3">Đường dùng</th>
                            <th rowspan="3">Nhóm thuốc</th>
                            <th rowspan="3">Đơn vị tính</th>
                            <th rowspan="3" style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo
                                thống kê của
                                CTCLQG
                            </th>
                            <th colspan="{{($plan->year_to + 1 - $plan->year_from) * 4}}">Số lượng dự trù (24 tháng)
                            </th>
                            <th rowspan="3">Tổng số lượng</th>
                            <th rowspan="3" style="width: 80px">Hành động</th>
                        </tr>
                        <tr>
                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                <th colspan="4">{{$year}}</th>
                            @endfor

                        </tr>
                        <tr>
                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                <th>Quý I</th>
                                <th>Quý II</th>
                                <th>Quý III</th>
                                <th>Quý IV</th>
                            @endfor

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $row)
                            <tr class="{{$row['giaitrinh']?'text-warning':''}}">
                                <td>{{$row['ma_thuoc']}}</td>
                                <td>{{$row['ten_hoat_chat']}}</td>
                                <td>{{$row['ham_luong']}}</td>
                                <td>{{$row['dang_bao_che']}}</td>
                                <td>{{$row['duong_dung']}}</td>
                                <td>{{$row['nhom_thuoc']}}</td>
                                <td>{{$row['dvt']}}</td>
                                <td class="text-right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row))}}</td>
                                @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                                    @for($q = 1; $q <= 4; $q ++)
                                        <td class="text-right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row))}}</td>
                                    @endfor
                                @endfor
                                <td class="text-right">
                                    <strong>{{\App\Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row))}}</strong>
                                </td>
                                <td class="block-update_user text-center">
                                    <a type="button"
                                       class="text-primary table-action"
                                       href="javascript:view_data_plan('{{$row['ma_cskcb']}}','{{($row['ma_thuoc'])}}')"><i
                                                class="fa fa-edit admin-action"></i></a>
                                    <a type="button" class="text-danger table-action"
                                       href="javascript:removeRow('{{$row['ma_cskcb']}}','{{($row['ma_thuoc'])}}')"><i
                                                class="fa fa-trash-o admin-action"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="18"><strong>Tổng: {{count($data)}} khoản</strong></td>
                            {{--                            <td style="text-align: right">{{\App\Utils::formatNumber(\App\Utils::keyFromArr('sudung', $tong))}}</td>--}}
                            {{--                            @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)--}}
                            {{--                                @for($q = 1; $q <= 4; $q ++)--}}
                            {{--                                    <td style="text-align: right">--}}
                            {{--                                        <strong>{{\App\Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $tong))}}</strong>--}}
                            {{--                                    </td>--}}
                            {{--                                @endfor--}}
                            {{--                            @endfor--}}
                            {{--                            <td style="text-align: right">--}}
                            {{--                                <strong>{{\App\Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $tong))}}</strong>--}}
                            {{--                            </td>--}}
                            {{--                            <td></td>--}}
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include("csyt.plan.modal_cv")
    @include("csyt.plan.modal_submit")
    <div class="modal fade" id="myModal_update" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 800px;">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title title_update text-white"></h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    {!! Form::open(array('route' => 'update-plan','method' => 'POST')) !!}
                    <input type="hidden" id="ma_thuoc" name="ma_thuoc">
                    <input type="hidden" id="ma_cskcb" name="ma_cskcb">
                    <div class="col-md-12 row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Tên hoạt chất : </label>
                                <span id="tenhoatchat"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Nồng độ,hàm lượng : </label>
                                <span id="nongdo_hamluong"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Dạng bào chế : </label>
                                <span id="dangbaoche"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Đường dùng : </label>
                                <span id="duongdung"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Nhóm thuốc : </label>
                                <span id="nhomthuoc"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Đơn vị tính : </label>
                                <span id="donvitinh"></span>
                            </div>
                        </div>
                    </div>
                    <div id="block-input">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" onclick="$(this).prop('hidden', true);">Cập nhật</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal_create" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 800px;">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title text-white">Thêm mới dữ liệu kế hoạch dự trù</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body row">
                    {!! Form::open(array('route' => 'create-plan','method' => 'POST')) !!}
                    <input type="hidden" id="ma_thuoc_create" name="ma_thuoc">
                    <div class="col-md-12 row">
                        <div class="col-md-12">
                            <div class="form-group select2-full-w">
                                <label class="col-form-label">Danh mục thuốc</label>
                                <select class="form-control" onchange="selectInfoMedicine()" id="dmthuoc"
                                        name="ma_thuoc" required>
                                    <option></option>
                                    @foreach($listMedicine as $v)
                                        <option value="{{$v->code}}">{{$v->code}} - {{$v->name}} - {{$v->group_tckt}} -
                                            Nồng độ hàm lượng: {{$v->concentration}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Tên hoạt chất : </label>
                                <span id="tenhoatchat_create"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Nồng độ,hàm lượng : </label>
                                <span id="nongdo_hamluong_create"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Dạng bào chế : </label>
                                <span id="dangbaoche_create"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Đường dùng : </label>
                                <span id="duongdung_create"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Nhóm thuốc : </label>
                                <span id="nhomthuoc_create"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="mb-1 text-black">Đơn vị tính : </label>
                                <span id="donvitinh_create"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group"><label class="mb-1">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo
                                thống kê của
                                CTCLQG</label>
                            <input type="number" class="form-control" placeholder="Số lượng sử dụng"
                                   id="soluong_sudung_create" name="sudung">
                        </div>
                    </div>
                    <div class="col-md-12 row">
                        @for($year = $plan->year_from; $year <= $plan->year_to; $year ++)
                            <div class="col-md-12 row">
                                <div class="col-md-12">
                                    <div class="form-group"><label class="mb-1">Năm :</label> <span
                                                id="nam">{{$year}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"><label class="mb-1">Quý I</label>
                                    <input type="number" class="form-control" name="{{$year}}1" value=""
                                           placeholder="Quý I"
                                           id='createquy{{$year}}1'>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"><label class="mb-1">Quý II</label>
                                    <input type="number" class="form-control" name="{{$year}}2" value=""
                                           placeholder="Quý II" id='createquy{{$year}}2'>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"><label class="mb-1">Quý III</label>
                                    <input type="number" class="form-control" name="{{$year}}3" value=""
                                           placeholder="Quý III" id='createquy{{$year}}3'>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"><label class="mb-1">Quý IV</label>
                                    <input type="number" class="form-control" name="{{$year}}4" value=""
                                           placeholder="Quý IV" id='createquy{{$year}}4'>
                                </div>
                            </div>
                        @endfor

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary" onclick="if($('#dmthuoc').val() !== '') $(this).prop('hidden', true);">Thêm mới</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')

    <script language="javascript">
        function removeRow(ma_cskcb, ma_thuoc) {
            if (confirm("Bạn có chắc chắn muốn xóa?")) {
                document.getElementById("ma_thuoc_delete").value = ma_thuoc;
                document.getElementById("ma_cskcb_delete").value = ma_cskcb;
                frmrowremove.submit();
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            // $('#table-plan').DataTable();
            $("#dmthuoc").select2();
        });

        function selectInfoMedicine() {
            var code = $('#dmthuoc').val();
            $.ajax({
                type: "GET",
                url: "{{route('view-info-dm-thuoc')}}",
                async: true,
                data: {code: code},
                success: function (result) {
                    var obj = jQuery.parseJSON(JSON.stringify(result));
                    $('#tenhoatchat_create').html(obj.name);
                    $('#nongdo_hamluong_create').html(obj.concentration);
                    $('#dangbaoche_create').html(obj.cell_type);
                    $('#duongdung_create').html(obj.type);
                    $('#nhomthuoc_create').html(obj.group_tckt);
                    $('#donvitinh_create').html(obj.unit);
                    $('#ma_thuoc_create').val(obj.code);
                }
            });
        }

                {{--function createPlan() {--}}
                {{--    if ($('#dmthuoc').val() === '') {--}}
                {{--        return alert('Vui lòng chọn danh mục thuốc!')--}}
                {{--    }--}}
                {{--    var year = '';--}}
                {{--    var obj = {};--}}
                {{--    "@for($year = $plan->year_from; $year <= $plan->year_to; $year ++)"--}}
                {{--    " @for($q = 1; $q <= 4; $q ++)"--}}
                {{--    obj[year + '' + ({{$year}} + '' + {{$q}})] = $('#createquy' + {{$year}} + '' + ({{$q}})).val();--}}
                {{--    "@endfor"--}}
                {{--    "@endfor"--}}
                {{--    obj['_token'] = "{{ csrf_token() }}";--}}
                {{--    obj['ma_thuoc'] = $('#ma_thuoc_create').val();--}}
                {{--    obj['sudung'] = $('#soluong_sudung_create').val();--}}
                {{--    $.ajax({--}}
                {{--        type: "POST",--}}
                {{--        url: "{{route('create-plan')}}",--}}
                {{--        async: true,--}}
                {{--        data: obj,--}}
                {{--        success: function (result) {--}}
                {{--            var c = jQuery.parseJSON(JSON.stringify(result));--}}
                {{--            if (c.status == 1) {--}}
                {{--                $("#myModal_create").modal("hide");--}}
                {{--                $('.modal-title-message').html(c.message);--}}
                {{--                $('.modal-title-message').addClass(c.success);--}}
                {{--                $("#modal_message").modal("show");--}}
                {{--                setTimeout(function () {--}}
                {{--                    $("#modal_message").modal("hide");--}}
                {{--                    window.location.replace("{{route("csyt-plan-index")}}");--}}
                {{--                }, 1000);--}}
                {{--            } else {--}}
                {{--                // $("#myModal_update").modal("hide");--}}
                {{--                $('.modal-title-message').html(c.message);--}}
                {{--                $('.modal-title-message').addClass(c.success);--}}
                {{--                $("#modal_message").modal("show");--}}
                {{--                setTimeout(function () {--}}
                {{--                    $("#modal_message").modal("hide");--}}
                {{--                }, 1000);--}}
                {{--            }--}}
                {{--        }--}}
                {{--    });--}}
                {{--}--}}


        function view_data_plan(ma_cskcb, ma_thuoc) {
            $.ajax({
                type: "GET",
                url: "{{route('view-info-csyt')}}",
                async: true,
                data: {ma_cskcb: ma_cskcb, ma_thuoc: ma_thuoc},
                success: function (result) {
                    $('#block-input').html('');
                    var array = jQuery.parseJSON(JSON.stringify(result));
                    var plan = array[1];
                    var data = array[0];
                    var planYear = array[2];
                    var QuySudung = array[3];
                    $('#tenhoatchat').html(data.ten_hoat_chat);
                    $('#nongdo_hamluong').html(data.ham_luong);
                    $('#dangbaoche').html(data.dang_bao_che);
                    $('#duongdung').html(data.duong_dung);
                    $('#nhomthuoc').html(data.nhom_thuoc);
                    $('#donvitinh').html(data.dvt);
                    $('#ma_thuoc').val(ma_thuoc);
                    $('#ma_cskcb').val(ma_cskcb);
                    var html = "                            <div class='col-md-12'>" +
                        "                                <div class='form-group'>" +
                        "                                    <label class='mb-1'>Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo thống kê của CTCLQG</label>" +
                        "                                    <input type='text' class='form-control' name='sudung' value='" + QuySudung.so_luong + "' placeholder='Số lượng sử dụng'" +
                        "                                           id='soluong_sudung'>" +
                        "                                </div>" +
                        "                            </div>" +
                        "                            </div>";
                    var year = '';
                    html += '<div class="col-md-12 row">';
                    for (year = planYear.year_from; year <= planYear.year_to; year++) {
                        html += "<div class='col-md-12 row'>" +
                            "                            <div class='col-md-12'>" +
                            "                                <div class='form-group'>" +
                            "                                    <label class='mb-1'>Năm :</label>" +
                            "                                    <span id='nam'>" + year + "</span>" +
                            "                                </div>" +
                            "                            </div>" +
                            "                            </div>";
                        plan[year].forEach((k, i) => {
                            html += "                        <div class='col-md-3'>" +
                                "                                <div class='form-group'>" +
                                "                                    <label class='mb-1'>" + 'Quý' + (i + 1) + "</label>" +
                                "                                    <input type='text' class='form-control' name='" + year + (i + 1) + "' value='" + k.so_luong + "' placeholder='Quý " + (i + 1) + "' id='quy" + year + (i + 1) + "'>" +
                                "                                </div>" +
                                "                            </div>";
                        });
                    }

                    html += '</div>';
                    $('#block-input').append(html);
                    $('.modal-footer').find('.btn-ud').show();
                    $('.title_update').html("Cập nhật kế hoạch dự trù - mã thuốc: " + data.ma_thuoc);
                    $("#myModal_update").modal("show");
                }
            });
        }

        {{--/*function updatePlan() {--}}
        {{--    var year = '';--}}
        {{--    var obj = {};--}}
        {{--    for (year = listData[2].year_from; year <= listData[2].year_to; year++) {--}}
        {{--        listData[1][year].forEach((k, i) => {--}}
        {{--            obj[year + '' + (i + 1)] = $('#quy' + year + (i + 1)).val();--}}
        {{--        });--}}
        {{--    }--}}
        {{--    obj['_token'] = "{{ csrf_token() }}";--}}
        {{--    obj['ma_thuoc'] = $('#ma_thuoc').val();--}}
        {{--    obj['ma_cskcb'] = $('#ma_cskcb').val();--}}
        {{--    obj['sudung'] = $('#soluong_sudung').val();--}}
        {{--    obj['listData'] = listData;--}}
        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: "{{route('update-plan')}}",--}}
        {{--        async: true,--}}
        {{--        data: obj,--}}
        {{--        success: function (result) {--}}
        {{--            var c = jQuery.parseJSON(JSON.stringify(result));--}}
        {{--            if (c.status == 1) {--}}
        {{--                $("#myModal_update").modal("hide");--}}
        {{--                $('.modal-title-message').html(c.message);--}}
        {{--                $('.modal-title-message').addClass(c.success);--}}
        {{--                $("#modal_message").modal("show");--}}
        {{--                setTimeout(function () {--}}
        {{--                    $("#modal_message").modal("hide");--}}
        {{--                    window.location.replace("{{route("csyt-plan-index")}}");--}}
        {{--                }, 1000);--}}
        {{--            } else {--}}
        {{--                // $("#myModal_update").modal("hide");--}}
        {{--                $('.modal-title-message').html(c.message);--}}
        {{--                $('.modal-title-message').addClass(c.success);--}}
        {{--                $("#modal_message").modal("show");--}}
        {{--                setTimeout(function () {--}}
        {{--                    $("#modal_message").modal("hide");--}}
        {{--                }, 1000);--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--}*/--}}

    </script>
@endsection
