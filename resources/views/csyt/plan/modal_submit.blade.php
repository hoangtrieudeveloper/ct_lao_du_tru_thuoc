<div class="modal" id="modal-plan-submit">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Gửi kế hoạch lên Tỉnh/TP</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'csyt-plan-submit-plan', 'files' => 'true')) !!}
                <input type="submit" class="btn btn-success btn-sm pull-right" value="Gửi kế hoạch">
                <div>Tên cơ sở y tế: <strong>{{$cskcb->name}}</strong></div>
                <div>Mã cơ sở y tế: <strong>{{$cskcb->ma_cskcb}}</strong></div>
                <div>Tỉnh/TP: <strong>{{$tinh->name}}</strong></div>
                <div>Số kế hoạch: <strong>CSYT{{$soCV}}</strong></div>
                <div class="text-danger">Chú ý(*): Kiểm tra so khớp số kế hoạch trên với File Báo cáo nhu cầu kế hoạch
                    thuốc
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="form-group col-md-12">
                        <label>Báo cáo nhu cầu kế hoạch thuốc <span class="text-danger">(*)</span> (Bản scan kế hoạch
                            thuốc đã được ký, đóng dấu)</label>
                        <input type="file" name="file_bckh" id="input-file-now-custom-2" required class="dropify"
                               data-height="70" accept=".pdf,.png,.jpg,.jpeg"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Biên bản họp hội đồng thuốc và điều trị <span class="text-danger">(*)</span> (Bản scan
                            biên bản đã được ký, đóng dấu)</label>
                        <input type="file" name="file_bb_hop" id="input-file-now-custom-2" required class="dropify"
                               data-height="70" accept=".pdf,.png,.jpg,.jpeg"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Công văn cam kết sử dụng ít nhất 80% nhu cầu dự trù <span
                                    class="text-danger">(*)</span> (Bản scan công văn đã được ký, đóng dấu)</label>
                        <input type="file" name="file_cv_cam_ket" id="input-file-now-custom-2" required
                               class="dropify" data-height="70" accept=".pdf,.png,.jpg,.jpeg"/>
                    </div>
                    @if($hasGiaiTrinh)
                        <div class="form-group col-md-12">
                            <label class="text-warning">Công văn giải trình trong trường hợp cần thiết giải trình về việc dự trù thuốc tăng
                                giảm quá 30% so với lượng thuốc đã sử dụng giai đoạn 2019-2020 (Bản scan công văn đã
                                được ký, đóng dấu)</label>
                            <input type="file" name="file_cv_giai_trinh" id="input-file-now-custom-2"
                                   class="dropify" data-height="70" accept=".pdf,.png,.jpg,.jpeg"/>
                        </div>
                    @endif
                    @if($hasThuocKhac)
                        <div class="form-group col-md-12">
                            <label>3 báo giá của nhà cung ứng nếu có dự trù thuốc thuộc nhóm TCKT khác với các thuốc
                                được quy định tại phụ lục 1, công văn số 1106/BVPTƯ ngày 14/5/2021</label>
                        </div>
                        <div class="col-md-4"><input type="file" name="file_bao_gia1" id="input-file-now-custom-2"
                                                     class="dropify" data-height="70"/></div>
                        <div class="col-md-4"><input type="file" name="file_bao_gia2" id="input-file-now-custom-2"
                                                     class="dropify" data-height="70"/></div>
                        <div class="col-md-4"><input type="file" name="file_bao_gia3" id="input-file-now-custom-2"
                                                     class="dropify" data-height="70"/></div>
                    @endif

                </div>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
    function clickSendPlan() {
        @if(is_null($existPlan))
        $("#modal-plan-submit").modal('show');
        @else
        modalMessage('primary', "Kế hoạch số {{$existPlan->cong_van}} đã được gửi đi và xác nhận, không thể gửi thêm kế hoạch!");
        @endif
    }

    $(document).ready(function () {
// Basic
        $('.dropify').dropify();

// Translated
        $('.dropify-fr').dropify();

// Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function (event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function (event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function (event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function (e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>