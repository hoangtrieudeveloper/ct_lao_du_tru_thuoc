@extends('layouts.layout')

@section('content')
    {!! Form::close() !!}
    <div class="right_col" role="main">
        <div class="row">
            <div class="container">
                <h1 class="text-center">Cập nhật thông tin</h1>
                <div class="text-center">
                    @if (Session::has('message'))
                        <div class="alert alert-info"
                             style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                    @endif
                </div>
                <hr>
                <div class="row">
                    <!-- left column -->
                {{--<div class="col-md-3">--}}
                {{--<div class="text-center">--}}
                {{--<img src="//placehold.it/100" class="avatar img-circle" alt="avatar">--}}
                {{--<h6>Upload a different photo...</h6>--}}

                {{--<input type="file" class="form-control">--}}
                {{--</div>--}}
                {{--</div>--}}

                <!-- edit form column -->
                    <div class="col-md-12 personal-info">
                        {!! Form::open(array('route' => 'update-profile','class'=> 'form-horizontal')) !!}
                        <input type="hidden" name="id" value="{{$user['id']}}">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Tên:</label>
                            <div class="col-lg-8">
                                <input class="form-control" type="text" name="fullname" value="{{$user['fullname']}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Tên đăng nhập:</label>
                            <div class="col-lg-8">
                                <input class="form-control" type="text" name="username" value="{{$user['username']}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Email:</label>
                            <div class="col-lg-8">
                                <input class="form-control" type="text" name="email" value="{{$user['email']}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Giới tính</label>
                            <div class="check_status">
                                <input type='radio' name='gender'
                                       value='1' {!! $user['gender'] == 1 ? 'checked' : '' !!}> Nam
                                <input type='radio' name='gender'
                                       value='2' {!! $user['gender'] == 2 ? 'checked' : '' !!}> Nữ
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Trạng thái</label>
                            <div class="check_status">
                                <input type='radio' name='status'
                                       value='1' {!! $user['status'] == 1 ? 'checked' : '' !!}> Hoạt động
                                <input type='radio' name='status'
                                       value='2' {!! $user['status'] == 2 ? 'checked' : '' !!}> Đóng
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <input type="submit" class="btn btn-primary" value="Lưu">
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>
@endsection
