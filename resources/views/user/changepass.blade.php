@extends('layouts.app')

@section('content')
    {!! Form::close() !!}
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        @if (Session::has('message'))
                            <div class="alert alert-info"
                                 style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                        @endif
                    </div>
                    <div class="page-title-box">
                        <h4 class="page-title mb-2 pull-left">Đổi mật khẩu</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30 mt-3">
                        {!! Form::open(array('route' => 'change-password','class'=> 'form-horizontal')) !!}
                        <div class="card-body handleData">
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Mật khẩu cũ:</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="password" name="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Mật khẩu mới:</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="password" name="password_new" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Nhập lại mật khẩu:</label>
                                <div class="col-lg-4">
                                    <input class="form-control" type="password" name="re_password_new" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <input type="submit" class="btn btn-primary" name="submit" value="Lưu">
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<style>
    .handleData .form-group label {
        font-size: 14px;
        font-weight: 500;
        color: black;
    }
</style>
