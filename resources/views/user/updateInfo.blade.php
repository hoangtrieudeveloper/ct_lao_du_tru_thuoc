@extends('layouts.app')
@section('title')
    Cập nhật thông tin
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
{{--                    <div class="text-center">--}}
{{--                        @if (Session::has('message'))--}}
{{--                            <div class="alert alert-info"--}}
{{--                                 style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>--}}
{{--                        @endif--}}
{{--                    </div>--}}
                    <div class="page-title-box">
                        <h4 class="page-title">Cập nhật thông tin</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body handleData">
                            <div class="text-danger" style=" font-size: 14px; font-style: italic">Lưu ý(*): Đề nghị các đơn vị cung cấp tên và đơn vị đầy đủ, chính xác, đúng như tên trong Giấy phép hoạt động và hợp đồng KBCB đã ký với CQBH để phục vụ việc ký hợp đồng, bảo lãnh và thực hiện hợp đồng</div>
                            {!! Form::open(array('route' => 'update-view-info','method' => 'POST')) !!}
                            <input type="hidden" name="id" value="{{$object['id']}}">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-sm-2 col-form-label">Mã</label>
                                <div class="col-sm-10">
                                    <input class="form-control" disabled type="text" value="{{$object['ma_cskcb']}}" name="ma_cskcb" placeholder="Mã" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-search-input" class="col-sm-2 col-form-label">Tên <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="search" value="{{$object['name']}}" name="name" placeholder="Tên" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Địa chỉ <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$object['address']}}" name="address" placeholder="Địa chỉ" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Cán bộ đầu mối <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$object['can_bo_dau_moi']}}" name="can_bo_dau_moi" placeholder="Cán bộ đầu mối" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Số điện thoại <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$object['dienthoai']}}" name="dienthoai" placeholder="Số điện thoại" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-sm-2 col-form-label">Email <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" value="{{$object['email']}}" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tỉnh/TP <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <select class="custom-select" id="tinhtp" onchange="checkQuanHuyen(1)" name="ma_tinh" required>
                                        <option value="">Chọn Tỉnh/TP</option>
                                        @foreach($list_province as $v)
                                            <option value="{{$v->id}}" {{$object['ma_tinh'] === $v->ma_tinh ? 'selected' : ''}}>{{$v->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Quận/Huyện</label>
                                <div class="col-sm-10">
                                    <select class="custom-select district" name="district_id" id="district_id">
                                        <option value="" data-tinh="0">Chọn Quận/Huyện</option>
                                        @foreach($listDictrict as $v)
                                            <option value="{{$v->id}}" data-tinh="{{$v->province_id}}"  style="display: none" class="quanhuyen_{{$v->province_id}}">{{$v->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10 button-items">
                                    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light pull-right" value="0">Cập nhật</button>
                                    <a href="/" class="btn btn-secondary waves-effect pull-right text-white">Quay lại</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>

</script>
@endsection
<style>
    .handleData .form-group label {
        font-size: 14px;
        font-weight: 500;
        color: black;
    }
</style>

