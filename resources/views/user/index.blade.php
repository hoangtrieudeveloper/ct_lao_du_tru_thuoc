@extends('layouts.app')
@section('title')
    Quản lý nhân viên
@endsection
@section('content')
    {!! Form::open(array('route' => 'delete-user', 'class' => 'form', 'id' => 'frmrowremove')) !!}
    <input type="hidden" name="row_code" id="row_code">
    {!! Form::close() !!}
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        @if (Session::has('message'))
                            <div class="alert alert-info"
                                 style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                        @endif
                    </div>
                    <div class="page-title-box">
                        <h4 class="page-title mb-2 pull-left">Quản lý nhân viên</h4>
                        @if(Session::get('uinfo')->group_scope == 1)
                            <div class="page-title-right">
                                <button type="button" class="btn btn-info pull-right btn-animation"
                                        data-animation="zoomIn"
                                        data-toggle="modal" data-target="#myModal">Thêm mới
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30 mt-3">
                        <div class="card-body">
                            <table class="table table-bordered table-striped datatable" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th>Tỉnh/TP<br><input></th>
{{--                                    <th>Quận huyện<br><input></th>--}}
                                    <th>Tên<br><input></th>
                                    <th>Tên đăng nhập<br><input></th>
                                    <th>Email<br><input></th>
                                    <th>Nhóm</th>
                                    <th>Phòng ban</th>
                                    <th class="text-center">Giới tính</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Ngày tạo</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $k => $v)
                                    <tr>
                                        <td class="text-center">{{$k+1}}</td>
                                        <td>{{array_key_exists($v->ma_tinh, $dictTinh)?$dictTinh[$v->ma_tinh]:''}}</td>
{{--                                        <td>{{array_key_exists($v->district_id, $dictHuyen)?$dictHuyen[$v->district_id]:''}}</td>--}}
                                        <td>{{$v['fullname']}}</td>
                                        <td>{{$v['username']}}</td>
                                        <td>{{$v['email']}}</td>
                                        <td>{{array_key_exists($v->group_scope, $dictGroup)?$dictGroup[$v->group_scope]:''}}</td>
                                        <td>{{array_key_exists($v->dept_id, $dictDEPT)?$dictDEPT[$v->dept_id]:''}}</td>
                                        <td class="text-center">{!! $v['gender'] == 1 ? 'Nam' : 'Nữ' !!}</td>
                                        <td class="text-center">
                                            <span class="btn btn-{!! $v['status'] == 1 ? 'success' : 'danger' !!} label color-white">{!! $v['status'] == 1 ? 'Hoạt động' : 'Không hoạt động' !!}</span>
                                        </td>
                                        <td class="text-center">{{date('d/m/Y',strtotime($v['created_at']))}}</td>
                                        <td class="block-update_user text-center">
                                            <button type="button" class="btn btn-info waves-effect waves-light btn-sm"
                                                    onclick="view_detail({{$v['id']}})"><i class="mdi mdi-eye"></i>
                                            </button>
                                            <button type="button"
                                                    class="btn btn-success waves-effect waves-light btn-sm"
                                                    onclick="view_data({{$v['id']}})"><i
                                                        class="fa fa-edit admin-action"></i></button>
                                            <button type="button" class="btn btn-danger waves-effect waves-light btn-sm"
                                                    onclick="removeRow({{$v['id']}})"><i
                                                        class="fa fa-trash-o admin-action"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div><!-- container -->


    </div> <!-- Page content Wrapper -->
    <div class="modal fade" id="myModal_update" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 800px;">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title text-white"></h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(array('route' => 'update-user','method' => 'POST')) !!}
                <div class="modal-body row">
                    <input type="hidden" id="id_user" name="id">
                    <input type="hidden" id="check_username" name="check_username">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="mb-1">Tên</label>
                            <input type="text" class="form-control" name="fullname" placeholder="họ" id="fullname"
                                   required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="mb-1">Tên đăng nhập</label>
                            <input type="text" class="form-control" name="username" placeholder="Tên đăng nhập"
                                   id="username" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="mb-1">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Email" id="email">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mt-2">
                            <label class="mb-1">Mật khẩu mới</label>
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu mới"
                                   id="password_new">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="mb-1">Phòng ban</label>
                            <select class="form-control select2_single" style="width: 100%" name="dept_id"
                                    id="dept_ud">
                                @foreach($user_dept as $k => $v)
                                    <option value="{{$v->id}}">{{$v->dept_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="mb-1">Nhóm</label>
                            <select class="form-control select2_single_group" style="width: 100%" name="group_scope"
                                    id="group_ud" onchange="checkGroupUd('',1)">
                                @foreach($user_group as $k => $v)
                                    <option value="{{$v->id}}">{{$v->group_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <div class="form-group">
                            <label class="mb-1">Tỉnh/TP</label>
                            <select class="custom-select" style="width: 100%" id="tinhtp_user_update"
                                    onchange="checkCskcb_update(1)"
                                    name="ma_tinh" required>
                                <option value="">Chọn Tỉnh/TP</option>
                                @foreach($list_province as $v)
                                    <option value="{{$v->id}}">{{$v->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 mt-2" id="ma_cskcb_ud">
                        <div class="form-group">
                            <label class="mb-1">Cơ sở KCB</label>
                            <select class="custom-select cskcb_update" id="checkCSKCBGroup_ud" name="ma_cskcb"
                                    style="width: 100%">
                                <option value="">Chọn cơ sở KCB</option>
                                @foreach($listCskcb as $v)
                                    <option value="{{$v->ma_cskcb}}" style="display: none"
                                            class="update_cskcb_{{$v->province_id}}">{{$v->ma_cskcb}}
                                        - {{$v->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 row mt-2">
                        <div class="col-md-6 mt-2">
                            <div class="form-group">
                                <label class="mb-1">Giới tính</label>
                                <div class="check_gender">
                                    <input type='radio' name='gender' value="1" id="gender1"> Nam&nbsp;&nbsp;
                                    <input type='radio' name='gender' value="2" id="gender2"> Nữ
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <div class="form-group">
                                <label class="mb-1">Trạng thái</label>
                                <div class="check_status">
                                    <input type='radio' name='status' value='1' id="status1"> Hoạt động&nbsp;&nbsp;
                                    <input type='radio' name='status' value='2' id="status2"> Đóng
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 800px;">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="text-white">Thêm mới thành viên</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(array('route' => 'create-user','method' => 'POST')) !!}
                <div class="modal-body row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="mb-1">Tên</label>
                            <input type="text" class="form-control" name="fullname" placeholder="Tên" id="ad_fullname"
                                   required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="mb-1">Tên đăng nhập</label>
                            <input type="text" class="form-control" name="username" placeholder="Tên đăng nhập"
                                   id="ad_username" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mt-2">
                            <label class="mb-1">Mật khẩu</label>
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu"
                                   id="ad_password" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mt-2">
                            <label class="mb-1">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Email" id="ad_email"
                                   required>
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <div class="form-group">
                            <label class="mb-1">Phòng ban</label>
                            <select class="form-control select2_single" style="width: 100%" name="dept" id="dept">
                                @foreach($user_dept as $k => $v)
                                    <option value="{{$v->id}}">{{$v->dept_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <div class="form-group">
                            <label class="mb-1">Nhóm</label>
                            <select class="form-control select2_single_group" style="width: 100%" name="group"
                                    id="group" onchange="checkGroup()">
                                @foreach($user_group as $k => $v)
                                    <option value="{{$v->id}}">{{$v->group_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 mt-2">
                        <div class="form-group">
                            <label class="mb-1">Tỉnh/TP</label>
                            <select class="custom-select" style="width: 100%" id="tinhtp_user" onchange="checkCskcb(1)"
                                    name="ma_tinh" required>
                                <option value="">Chọn Tỉnh/TP</option>
                                @foreach($list_province as $v)
                                    <option value="{{$v->id}}">{{$v->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 mt-2" id="ma_cskcb">
                        <div class="form-group">
                            <label class="mb-1">Cơ sở KCB</label>
                            <select class="custom-select cskcb" name="ma_cskcb" style="width: 100%"
                                    id="checkCSKCBGroup">
                                <option value="">Chọn cơ sở KCB</option>
                                @foreach($listCskcb as $v)
                                    <option value="{{$v->ma_cskcb}}" style="display: none"
                                            class="cskcb_{{$v->province_id}}">{{$v->ma_cskcb}} - {{$v->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 row mt-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="mb-1">Giới tính</label>
                                <div class="check_gender mt-2">
                                    <input type='radio' name='gender' value='1' checked>&nbsp;Nam&nbsp;&nbsp;
                                    <input type='radio' name='gender' value='2'>&nbsp;Nữ
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mt-2">
                            <div class="form-group">
                                <label class="mb-1">Trạng thái</label>
                                <div class="check_status mt-2">
                                    <input type='radio' name='status' value='1' checked> Hoạt động&nbsp;&nbsp;
                                    <input type='radio' name='status' value='2'> Đóng
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script language="javascript">
        function removeRow(row_code) {
            if (confirm("Bạn có chắc chắn muốn xóa?")) {
                document.getElementById("row_code").value = row_code;
                frmrowremove.submit();
            }
        }
    </script>
    <script>
        $(".select2_single").select2({
            tags: true
        });

        $(".select2_single_group").select2({
//            tags: true
        });
        $(".select2-multiple").select2({
            // allowClear: true
        });

        {{--function updateUser() {--}}
        {{--    var id = $('#id_user').val();--}}
        {{--    var fullname = $('#fullname').val();--}}
        {{--    var username = $('#username').val();--}}
        {{--    var email = $('#email').val();--}}
        {{--    var dept = $('#dept_ud').val();--}}
        {{--    var group = $('#group_ud').val();--}}
        {{--    var password_new = $('#password_new').val();--}}
        {{--    var gender = $('.check_gender').find('input:radio:checked').val();--}}
        {{--    var status = $('.check_status').find('input:radio:checked').val();--}}
        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: "{{route('update-user')}}",--}}
        {{--        async: true,--}}
        {{--        data: {--}}
        {{--            _token: "{{ csrf_token() }}",--}}
        {{--            id: id,--}}
        {{--            fullname: fullname,--}}
        {{--            username: username,--}}
        {{--            password: password_new,--}}
        {{--            dept_id: dept,--}}
        {{--            email: email,--}}
        {{--            group_scope: group,--}}
        {{--            gender: gender,--}}
        {{--            status: status,--}}
        {{--        },--}}
        {{--        success: function (result) {--}}
        {{--            var c = jQuery.parseJSON(JSON.stringify(result));--}}
        {{--            if (c.status == 1) {--}}
        {{--                $("#myModal_update").modal("hide");--}}
        {{--                $('.modal-title-message').html(c.message);--}}
        {{--                $('.modal-title-message').addClass(c.success);--}}
        {{--                $("#modal_message").modal("show");--}}
        {{--                setTimeout(function () {--}}
        {{--                    $("#modal_message").modal("hide");--}}
        {{--                    window.location.replace("{{route("user-index")}}");--}}
        {{--                }, 1000);--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}

        {{--function addUser() {--}}
        {{--    var fullname = $('#ad_fullname').val();--}}
        {{--    var password = $('#ad_password').val();--}}
        {{--    var username = $('#ad_username').val();--}}
        {{--    var group = $('#group').val();--}}
        {{--    var dept = $('#dept').val();--}}
        {{--    var email = $('#ad_email').val();--}}
        {{--    var gender = $('.check_gender').find('input:radio:checked').val();--}}
        {{--    var status = $('.check_status').find('input:radio:checked').val();--}}
        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: "{{route('create-user')}}",--}}
        {{--        async: true,--}}
        {{--        data: {--}}
        {{--            _token: "{{ csrf_token() }}",--}}
        {{--            fullname: fullname,--}}
        {{--            password: password,--}}
        {{--            username: username,--}}
        {{--            dept: dept,--}}
        {{--            group: group,--}}
        {{--            email: email,--}}
        {{--            gender: gender,--}}
        {{--            status: status,--}}
        {{--        },--}}
        {{--        success: function (result) {--}}
        {{--            var c = jQuery.parseJSON(JSON.stringify(result));--}}
        {{--            if (c.status == 1) {--}}
        {{--                $('#myModal').modal().hide();--}}
        {{--                $('.modal-title-message').html(c.message);--}}
        {{--                $('.modal-title-message').addClass(c.success);--}}
        {{--                $("#modal_message").modal("show");--}}
        {{--                setTimeout(function () {--}}
        {{--                    $("#modal_message").modal("hide");--}}
        {{--                    window.location.replace("{{route("user-index")}}");--}}
        {{--                }, 1000);--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}

        function view_detail($id) {
            $(".loader").show();
            var check = 1;
            $.ajax({
                type: "GET",
                url: "{{route('view-info')}}",
                async: true,
                data: {id: $id},
                success: function (result) {
                    resetData();
                    var c = jQuery.parseJSON(JSON.stringify(result));
                    $('#fullname').val(result['fullname']);
                    $('#fullname').prop('disabled', true);
                    $('#username').val(result['username']);
                    $('#password_new').prop('disabled', true);
                    $('#username').prop('disabled', true);
                    $('#email').val(result['email']);
                    $('#email').prop('disabled', true);
                    var group_scope = result['group_scope'];
                    $('#group_ud option').each(function () {
                        if ($(this).val() == group_scope) {
                            $(this).prop("selected", true);
                            $(this).val(group_scope).trigger('change');
                        }
                    });
                    $('#group_ud').prop('disabled', true);
                    var dept_id = result['dept_id'];
                    $('#dept_ud option').each(function () {
                        if ($(this).val() == dept_id) {
                            $(this).prop("selected", true);
                            $(this).val(dept_id).trigger('change');
                        }
                    });
                    $('#dept_ud').prop('disabled', true);
                    var province_id = result['ma_tinh'];
                    $('#tinhtp_user_update option').each(function () {
                        if ($(this).val() == province_id) {
                            $(this).prop("selected", true);
                            $(this).val(province_id).trigger('change');
                        }
                    });
                    $('#tinhtp_user_update').prop('disabled', true);
                    var ma_cskcb = result['ma_cskcb'];
                    $('.cskcb_update option').each(function () {
                        if ($(this).val() == ma_cskcb) {
                            $(this).prop("selected", true);
                            $(this).val(ma_cskcb).trigger('change');
                        }
                    });
                    $('.cskcb_update').prop('disabled', true);
                    if (result['gender'] == 1) {
                        $('#gender1').attr('checked', true);
                    } else $('#gender2').attr('checked', true);
                    if (result['status'] == 1) {
                        $('#status1').attr('checked', true);
                    } else if (result['status'] == 2) {
                        $('#status2').attr('checked', true);
                    }
                    $("#myModal_update").find("input[type=radio]").attr('disabled', true);
                    $('#id_user').val(result['id']);
                    $('#check_username').val(result['username']);
                    $('.modal-footer').find('.btn-ud').hide();
                    $(".loader").hide();
                    $('.modal-title').html("Thông tin người dùng" + result['username']);
                    $("#myModal_update").modal("show");
                }
            });
        }

        function view_data($id) {
            $(".loader").show();
            $.ajax({
                type: "GET",
                url: "{{route('view-info')}}",
                async: true,
                data: {id: $id},
                success: function (result) {
                    resetData();
                    var c = jQuery.parseJSON(JSON.stringify(result));
                    $('#fullname').val(result['fullname']);
                    $('#fullname').prop('disabled', false);
                    $('#username').val(result['username']);
                    $('#username').prop('disabled', false);
                    $('#password_new').prop('disabled', false);
                    $('#email').val(result['email']);
                    $('#email').prop('disabled', false);
                    var group_scope = result['group_scope'];
                    $('#group_ud option').each(function () {
                        if ($(this).val() == group_scope) {
                            $(this).prop("selected", true);
                            $(this).val(group_scope).trigger('change');
                        }
                    });
                    $('#group_ud').prop('disabled', false);
                    checkGroupUd(result['group_scope'],2);
                    var dept_id = result['dept_id'];
                    $('#dept_ud option').each(function () {
                        if ($(this).val() == dept_id) {
                            $(this).prop("selected", true);
                            $(this).val(dept_id).trigger('change');
                        }
                    });
                    $('#dept_ud').prop('disabled', false);

                    var province_id = result['ma_tinh'];
                    $('#tinhtp_user_update option').each(function () {
                        if ($(this).val() == province_id) {
                            $(this).prop("selected", true);
                            $(this).val(province_id).trigger('change');
                        }
                    });
                    $('#tinhtp_user_update').prop('disabled', false);
                    var ma_cskcb = result['ma_cskcb'];
                    $('.cskcb_update option').each(function () {
                        if ($(this).val() == ma_cskcb) {
                            $(this).prop("selected", true);
                            $(this).val(ma_cskcb).trigger('change');
                        }
                    });
                    $('.cskcb_update').prop('disabled', false);
                    if (result['gender'] == 1) {
                        $('#gender1').attr('checked', true);
                    } else $('#gender2').attr('checked', true);
                    if (result['status'] == 1) {
                        $('#status1').attr('checked', true).trigger('change');
                    } else $('#status2').attr('checked', true).trigger('change');
                    $("#myModal_update").find("input[type=radio]").attr('disabled', false);
                    $('#id_user').val(result['id']);
                    $('#check_username').val(result['username']);
                    $('.modal-footer').find('.btn-ud').show();
                    $(".loader").hide();
                    $('.modal-title').html("Chỉnh sửa thông tin người dùng : " + result['username']);
                    $("#myModal_update").modal("show");
                }
            });
        }

        function resetData() {
            $('#fullname').val('');
            $('#username').val('');
            $('#email').val('');
            $('#password_new').val('');
            $('#dept_ud').val('');
            $('#group_ud').val('');
            $('#tinhtp_user_update').val('');
            $('#checkCSKCBGroup_ud').val('');
        }

        // $(document).ready(function () {
        //     var table = $("#table-user").DataTable({
        //         "pageLength": 10,
        //         "ordering": false
        //     });
        //     table.columns().every(function () {
        //         var that = this;
        //         $('input', this.header()).on('keyup change changeDate', function () {
        //             if (that.search() !== this.value) {
        //                 that.search(this.value).draw();
        //             }
        //         });
        //         $('select', this.header()).on('change', function () {
        //             if (that.search() !== this.value) {
        //                 that.search(this.value ? '^' + this.value + '$' : '', true, false).draw();
        //             }
        //         });
        //     });
        //     $("#table-user_length").parent().parent().remove();
        // });
    </script>
    <style>
        .block-update_user a {
            cursor: pointer;
            font-size: 18px;
        }

        #myModal, #myModal_update label {
            color: black;
        }
    </style>
@endsection
