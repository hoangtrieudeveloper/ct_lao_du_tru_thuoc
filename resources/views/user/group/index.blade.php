@extends('layouts.app')

@section('title')
    Nhóm thành viên
@endsection
@section('content')
    {!! Form::open(array('route' => 'delete-user-group', 'class' => 'form', 'id' => 'frmrowremove')) !!}
    <input type="hidden" name="row_code" id="row_code">
    {!! Form::close() !!}
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        @if (Session::has('message'))
                            <div class="alert alert-info"
                                 style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                        @endif
                    </div>
                    <div class="page-title-box">
                        <h4 class="page-title mb-2 pull-left">Quản lý nhóm thành viên</h4>
                        <div class="page-title-right">
                            <a class="btn btn-info pull-right" href="{{route('group-user-create')}}">Thêm mới
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30 mt-3">
                        <div class="card-body">
                            <table id="datatable" class="table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th>Tên nhóm</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($groups as $k => $v)
                                    <tr>
                                        <td class="text-center">{{$k+1}}</td>
                                        <td>{{$v->group_name}}</td>
                                        <td class="block-update_user text-center">
                                            <a type="button"
                                                    class="btn btn-success waves-effect waves-light btn-sm"
                                                    href="{{route('group-user-update', ['id' => $v->id])}}"><i
                                                        class="fa fa-edit admin-action"></i></a>
                                            <button type="button" class="btn btn-danger waves-effect waves-light btn-sm"
                                                    onclick="removeRow({{$v->id}})"><i
                                                        class="fa fa-trash-o admin-action"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div><!-- container -->
    </div> <!-- Page content Wrapper -->
@endsection
@section('script')
    <script language="javascript">
        function removeRow(row_code) {
            if (confirm("Bạn có chắc chắn muốn xóa?")) {
                document.getElementById("row_code").value = row_code;
                frmrowremove.submit();
            }
        }
    </script>
@endsection
