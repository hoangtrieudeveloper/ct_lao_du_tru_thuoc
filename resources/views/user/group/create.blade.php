@extends('layouts.app')

@section('title')
    Tạo nhóm thành viên
@endsection
@section('content')
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Tạo nhóm thành viên</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            {!! Form::open(array('route' => 'group-user-create')) !!}
                            <div class="form-group col-md-6">
                                <label>Tên</label>
                                <input class="form-control" name="name" placeholder="Tên nhóm" required>
                                <input type="submit" value="Hoàn tất" style="margin-top: 20px" class="btn btn-info">
                            </div>
                            <div class="choose-scope col-md-6">
                                <label><b>Quyền</b></label>
                                @foreach($scopes as $root)
                                    <div class="scope-root">
                                        <label data-toggle="collapse" data-target="#child-{{$root['id']}}"
                                               aria-expanded="false"
                                               aria-controls="collapseOne">
                                            <input type="checkbox" name="scope[]" value="{{$root['all-route']}}"
                                                   data-toggle="toggle"
                                                   onchange="changeRoot(this)" data-id="{{$root['id']}}">
                                            {{$root['name']}}
                                            @if($root['children'])<span>&nbsp;<i class="mdi mdi-sort"></i></span> @endif
                                        </label>
                                        <div id="child-{{$root['id']}}"
                                             aria-expanded="false"
                                             class="collapse child">
                                            @foreach($root['children'] as $con)
                                                <div class="scope-child">
                                                    <label data-toggle="collapse" data-target="#child-{{$con['id']}}"
                                                           aria-expanded="false"
                                                           aria-controls="collapseOne">
                                                        <input type="checkbox" name="scope[]"
                                                               value="{{$con['all-route']}}"
                                                               data-toggle="toggle" class="input-child-{{$root['id']}}">
                                                        {{$con['name']}}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-sm-12 button-items">
                                        <a href="{{route('group-user-index')}}" class="btn btn-secondary waves-effect pull-left text-white">Quay lại</a>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
    <script>
        function changeRoot(e) {
            if (!$(e).is(":checked")) {
                $(".input-child-" + $(e).data('id')).prop('checked', false);
            }
        }
    </script>
    <style>
        .child {
            margin-left: 20px;
        }
    </style>
@endsection
