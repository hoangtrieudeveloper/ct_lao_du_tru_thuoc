@extends('layouts.app_login')

@section('content')
    <div class="card">
        <div class="card-body">

            <h3 class="text-center mt-0 m-b-15">
                <a href="#" class="logo logo-admin"><img src="/assets/images/logo_ctlao.png" height="60" alt="logo">&nbsp;&nbsp;Đăng
                    nhập</a>
            </h3>
            <div class="text-center">
                @if (Session::has('message'))
                    <div class="alert alert-info"
                         style="font-size: 20px;font-weight: bold">{!!  Session::get('message') !!}</div>
                @endif
            </div>

            <div class="p-3">
                <form class="form-horizontal m-t-20" method="POST" action="{{ route('security-login') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Tên đăng nhập"
                               id="username" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Mật khẩu" required>
                    </div>
                    <div class="form-group text-center row m-t-20">
                        <div class="col-12">
                            <button class="btn btn-danger btn-block waves-effect waves-light" type="submit" name="submit" value="0">Đăng nhập
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection
