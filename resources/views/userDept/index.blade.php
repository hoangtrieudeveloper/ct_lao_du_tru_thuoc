@extends('layouts.app')
@section('title')
    Quản lý phòng ban
@endsection
@section('content')
    {!! Form::open(array('route' => 'User-dept-delete', 'class' => 'form', 'id' => 'frmrowremove')) !!}
    <input type="hidden" name="row_code" id="row_code">
    {!! Form::close() !!}
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title mb-2 pull-left">Quản lý phòng ban</h4>
                        <div class="page-title-right">
                            <button type="button" class="btn btn-info pull-right btn-animation" data-animation="zoomIn"
                                    data-toggle="modal" data-target="#myModal">Thêm mới
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30 mt-3">
                        <div class="card-body">
                            <table id="datatable" class="table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th class="text-center">Tên phòng ban</th>
                                    <th class="text-center">Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $k => $v)
                                    <tr>
                                        <td class="text-center">{{$k+1}}</td>
                                        <td>{{$v['dept_name']}}</td>
                                        <td class="block-update_user text-center">
                                            <button type="button" class="btn btn-info waves-effect waves-light btn-sm"
                                                    onclick="view_detail({{$v['id']}})"><i class="mdi mdi-eye"></i>
                                            </button>
                                            <button type="button"
                                                    class="btn btn-success waves-effect waves-light btn-sm"
                                                    onclick="view_data({{$v['id']}})"><i
                                                        class="fa fa-edit admin-action"></i></button>
                                            <button type="button" class="btn btn-danger waves-effect waves-light btn-sm"
                                                    onclick="removeRow({{$v['id']}})"><i
                                                        class="fa fa-trash-o admin-action"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div><!-- container -->


    </div> <!-- Page content Wrapper -->

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 class="modal-title mt-0 text-white" id="myModalLabel">Thêm mới phòng ban</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(array('route' => 'User-dept-create','method' => 'POST')) !!}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tên phòng ban</label>
                        <input type="text" class="form-control" name="name" placeholder="Tên phòng ban" id="ad_name"
                               required>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info">Thêm mới</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
    <div id="myModal_update" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <h5 class="modal-title mt-0 text-white"></h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(array('route' => 'User-dept-update','method' => 'POST')) !!}
                <div class="modal-body">
                    <input type="hidden" id="id_userdept" name="id">
                    <div class="form-group">
                        <label>Tên phòng ban</label>
                        <input type="text" class="form-control" name="name" placeholder="Phòng ban" id="name"
                               required>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info btn-ud">Cập nhật</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection
@section('script')
    <script language="javascript">
        function removeRow(row_code) {
            if (confirm("Bạn có chắc chắn muốn xóa?")) {
                document.getElementById("row_code").value = row_code;
                frmrowremove.submit();
            }
        }
    </script>
    <script>

        {{--function updateUserDept() {--}}
        {{--    var id = $('#id_userdept').val();--}}
        {{--    var name = $('#name').val();--}}
        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: "{{route('User-dept-update')}}",--}}
        {{--        async: true,--}}
        {{--        data: {--}}
        {{--            _token: "{{ csrf_token() }}",--}}
        {{--            id: id,--}}
        {{--            name: name,--}}
        {{--        },--}}
        {{--        success: function (result) {--}}
        {{--            var c = jQuery.parseJSON(JSON.stringify(result));--}}
        {{--            if (c.status == 1) {--}}
        {{--                $("#myModal_update").modal("hide");--}}
        {{--                modalMessage(c.success, c.message)--}}
        {{--                setTimeout(function () {--}}
        {{--                    window.location.replace("{{route("User-dept-index")}}");--}}
        {{--                }, 1000);--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}
        {{--function addUserdept() {--}}
        {{--    var name = $('#ad_name').val();--}}
        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: "{{route('User-dept-create')}}",--}}
        {{--        async: true,--}}
        {{--        data: {--}}
        {{--            _token: "{{ csrf_token() }}",--}}
        {{--            name: name,--}}
        {{--        },--}}
        {{--        success: function (result) {--}}
        {{--            var c = jQuery.parseJSON(JSON.stringify(result));--}}
        {{--            if (c.status == 1) {--}}
        {{--                $('#myModal').modal().hide();--}}
        {{--                modalMessage(c.success, c.message)--}}
        {{--                setTimeout(function () {--}}
        {{--                    window.location.replace("{{route("User-dept-index")}}");--}}
        {{--                }, 1000);--}}
        {{--            }--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}

        function view_detail($id) {
            $(".loader").show();
            var check = 1;
            $.ajax({
                type: "GET",
                url: "{{route('User-dept-infor')}}",
                async: true,
                data: {id: $id},
                success: function (result) {
                    var c = jQuery.parseJSON(JSON.stringify(result));
                    $('#name').val(result[0]['dept_name']);
                    $('#name').prop('disabled', true);
                    $('#id_userdept').val(result[0]['id']);
                    $('.modal-footer').find('.btn-ud').hide();
                    $(".loader").hide();
                    $('.modal-title').html("Thông tin phòng ban");
                    $("#myModal_update").modal("show");
                }
            });
        }

        function view_data($id) {
            $(".loader").show();
            $.ajax({
                type: "GET",
                url: "{{route('User-dept-view')}}",
                async: true,
                data: {id: $id},
                success: function (result) {
                    var c = jQuery.parseJSON(JSON.stringify(result));
                    $('#name').val(result[0]['dept_name']);
                    $('#name').prop('disabled', false);
                    $('#id_userdept').val(result[0]['id']);
                    $('.modal-footer').find('.btn-ud').show()
                    $(".loader").hide();
                    $('.modal-title').html("Chỉnh sửa phòng ban");
                    $("#myModal_update").modal("show");
                }
            });
        }
    </script>
    <style>
        .block-update_user a {
            cursor: pointer;
            font-size: 18px;
        }

        .modal_bg {
            background: #EDEDED;
            color: black;
            border-radius: 7px 7px 0 0;
        }

        #myModal, #myModal_update label {
            color: black;
        }
    </style>
@endsection
