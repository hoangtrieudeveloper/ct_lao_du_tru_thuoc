<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class categorySite extends Authenticatable
{
    protected $table = 'category_site';
    use Notifiable;
    public static function deleteCate($row_code)
    {
        DB::table('category_site')->where('id', $row_code)->delete();
    }

    public static function createdSite($data)
    {
        if($data['ma_tinh']) {
            $ma_tinh = DB::table('provinces')->where('ma_tinh',$data['ma_tinh'])->first();
            $data['province_id'] = $ma_tinh->id;
            $data['dbhc_tinh'] = $data['ma_tinh'];
        }
        $data['created_at'] = date("Y/m/d H:i:s");
        return self::insertGetId($data);
    }
    public static function updatedSite($data)
    {
        $id = $data['id'];
        $data['updated_at'] = date("Y/m/d H:i:s");
        if($data['ma_tinh']) {
            $ma_tinh = DB::table('provinces')->where('ma_tinh',$data['ma_tinh'])->first();
            $data['province_id'] = $ma_tinh->id;
            $data['dbhc_tinh'] = $data['ma_tinh'];
        }
        unset($data['id']);
        return self::where('id', $id)->update($data);
    }
}
