<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Scopes extends Model
{
    protected $table = 'scopes';

    public static function getTree()
    {
        return self::getChildren('root');
    }

    public static function getChildren($route)
    {
        $children = array();
        $select = self::where('scope_parent', '=', $route)
            ->where('scope_enable', '=', 1)
            ->orderBy('id', 'asc')
            ->get();
        foreach ($select as $row) {
            $child = array();
            $child['id'] = $row->id;
            $child['name'] = $row->scope_name;
            $child['route'] = $row->scope_route;
            $child['all-route'] = $row->scope_route . ($row->scope_relation == null ? '' : '|' . $row->scope_relation);
            $child['children'] = self::getChildren($row->scope_route);
            $children[] = $child;
        }
        return $children;
    }

    public static function checkAccess($route, $getScope = false)
    {
        if ($getScope || !Session::has('scopes')) {
            $scopes = DB::table('user_group')->where('id', \session('uinfo')->group_scope)->first()->group_roles;
            Session::put('scopes', explode('|', $scopes));
        }
        $scopes = Session::get('scopes');
        return in_array($route, $scopes);
    }
}
