<?php


namespace App;


use http\Env\Request;
use Illuminate\Support\Facades\DB;

class GenView
{
    public static function viewSessionM03($sesId)
    {
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();

        $selectData = $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('ses_tinh_id', $sesId)
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt', 'site.name as ten_cskcb')
            ->get();
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_cskcb . $row->ma_thuoc, $data)) {
                $data[$row->ma_cskcb . $row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->ten_cskcb,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_cskcb . $row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_cskcb . $row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        usort($data, function ($item1, $item2) {
            return $item1['ma_cskcb'] > $item2['ma_cskcb'];
        });

        $html = '';
        $html .= '<a href="' . route('center-export-m03', ['id' => $sesId]) . '" class="btn btn-success btn-sm pull-right">Excel</a>';
        $html .= '<table class="table table-bordered table-striped datatable">';
        $html .= '<thead>';
        $html .= '<tr>
                        <th rowspan="3">Mã CSKCB</th>
                        <th rowspan="3">Tên CSKCB</th>
                        <th rowspan="3">Mã thuốc</th>
                        <th rowspan="3">Tên hoạt chất</th>
                        <th rowspan="3">Nồng độ, hàm lượng</th>
                        <th rowspan="3">Dạng bào chế</th>
                        <th rowspan="3">Đường dùng</th>
                        <th rowspan="3">Nhóm thuốc</th>
                        <th rowspan="3">Đơn vị tính</th>
                        <th rowspan="3"  style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo thống kê của
                            CTCLQG
                        </th>
                        <th colspan="8">Số lượng dự trù (24 tháng)</th>
                        <th rowspan="3">Tổng số lượng</th>
                    </tr>';
        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th colspan="4">' . $year . '</th>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th>Quý I</th>
                            <th>Quý II</th>
                            <th>Quý III</th>
                            <th>Quý IV</th>';
        }
        $html .= '</tr>';
        $html .= '</thead>';

        $html .= '<tbody>';
        foreach ($data as $row) {
            $html .= '<tr>
                            <td>' . $row['ma_cskcb'] . '</td>
                            <td>' . $row['ten_cskcb'] . '</td>
                            <td>' . $row['ma_thuoc'] . '</td>
                            <td>' . $row['ten_hoat_chat'] . '</td>
                            <td>' . $row['ham_luong'] . '</td>
                            <td>' . $row['dang_bao_che'] . '</td>
                            <td>' . $row['duong_dung'] . '</td>
                            <td>' . $row['nhom_thuoc'] . '</td>
                            <td>' . $row['dvt'] . '</td>
                            <td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row)) . '</td>';
            for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
                for ($q = 1; $q <= 4; $q++) {
                    $html .= '<td class="text-right" >' . Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row)) . '</td >';
                }
            }
            $html .= '<td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row)) . '</td>';
            $html .= '</tr>';
        }
        $html .= '<tr>';
        $html .= '<td colspan="19"><strong>Tổng ' . count($data) . ' khoản</strong></td>';

        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }

    public static function viewSessionM02($sesId)
    {
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('plan.ses_tinh_id', $sesId)
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $res['data'] = $selectData;
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);

        $html = '';
        $html .= '<a href="' . route('center-export-m02', ['id' => $sesId]) . '" class="btn btn-success btn-sm pull-right">Excel</a>';
        $html .= '<table class="table table-bordered table-striped">';
        $html .= '<thead>';
        $html .= '<tr>
                        <th rowspan="3">Mã thuốc</th>
                        <th rowspan="3">Tên hoạt chất</th>
                        <th rowspan="3">Nồng độ, hàm lượng</th>
                        <th rowspan="3">Dạng bào chế</th>
                        <th rowspan="3">Đường dùng</th>
                        <th rowspan="3">Nhóm thuốc</th>
                        <th rowspan="3">Đơn vị tính</th>
                        <th rowspan="3"  style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo thống kê của
                            CTCLQG
                        </th>
                        <th colspan="8">Số lượng dự trù (24 tháng)</th>
                        <th rowspan="3">Tổng số lượng</th>
                    </tr>';
        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th colspan="4">' . $year . '</th>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th>Quý I</th>
                            <th>Quý II</th>
                            <th>Quý III</th>
                            <th>Quý IV</th>';
        }
        $html .= '</tr>';
        $html .= '</thead>';

        $html .= '<tbody>';
        foreach ($data as $row) {
            $html .= '<tr>
                            <td>' . $row['ma_thuoc'] . '</td>
                            <td>' . $row['ten_hoat_chat'] . '</td>
                            <td>' . $row['ham_luong'] . '</td>
                            <td>' . $row['dang_bao_che'] . '</td>
                            <td>' . $row['duong_dung'] . '</td>
                            <td>' . $row['nhom_thuoc'] . '</td>
                            <td>' . $row['dvt'] . '</td>
                            <td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row)) . '</td>';
            for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
                for ($q = 1; $q <= 4; $q++) {
                    $html .= '<td class="text-right" >' . Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row)) . '</td >';
                }
            }
            $html .= '<td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row)) . '</td>';
            $html .= '</tr>';
        }
        $html .= '<tr>';
        $html .= '<td colspan="17"><strong>Tổng ' . count($data) . ' khoản</strong></td>';

        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }

    public static function viewSessionM04($sesId)
    {
        $macs = DB::table('plan_row')->where('ses_tinh_id', $sesId)->pluck('ma_cskcb')->toArray();
        $csyt = DB::table('category_site')->whereIn('ma_cskcb', $macs)->get();

        $html = '';
        $html .= '<table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th rowspan="2">TT</th>
                            <th rowspan="2">Tên Cơ sở y tế</th>
                            <th rowspan="2">Mã KBCB CSYT</th>
                            <th rowspan="2">Địa chỉ chi tiết của cơ sở y tế</th>
                            <th colspan="3">Thông tin liên hệ</th>
                        </tr>
                        <tr>
                            <th>Cán bộ đầu mối</th>
                            <th>Điện thoại</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>';
        foreach ($csyt as $stt => $row) {
            $html .= '<tr>
                                <td class="text-center">' . ($stt + 1) . '</td>
                                <td>' . $row->name . '</td>
                                <td class="text-center">' . $row->ma_cskcb . '</td>
                                <td>' . $row->address . '</td>
                                <td>' . $row->can_bo_dau_moi . '</td>
                                <td>' . $row->dienthoai . '</td>
                                <td>' . $row->email . '</td>
                            </tr>';
        }
        $html .= '</tbody></table>';
        return $html;
    }
}