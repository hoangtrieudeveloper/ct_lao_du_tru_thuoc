<?php

namespace App\Http\Controllers;

use App\categorySite;
use App\User;
use App\UserDEPT;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listProvince = Utils::getProvince();
        $listcskcb = DB::table('category_site')->get();
        $user = User::orderBy('id', 'desc')->get();
        $selectgroup = DB::table('user_group')->where('id','!=','1')->get();
        $dictGroup = array();
        foreach ($selectgroup as $row) {
            $dictGroup[$row->id] = $row->group_name;
        }
        $selectDEPT = DB::table('user_dept')->get();
        $dictDEPT = array();
        foreach ($selectDEPT as $row) {
            $dictDEPT[$row->id] = $row->dept_name;
        }
        $dictTinh = Utils::dictTinh();
        $dictHuyen = Utils::dictHuyen();
        return view('user.index', ['data' => $user, 'list_province' => $listProvince,'dictTinh' => $dictTinh,'dictHuyen' => $dictHuyen, 'listCskcb' => $listcskcb, "user_group" => $selectgroup, 'user_dept' => $selectDEPT,
            'dictGroup' => $dictGroup, 'dictDEPT' => $dictDEPT]);
    }

    public function deleteUser(Request $request)
    {
        $row_code = $request->row_code;
        User::deleteUser($row_code);
        $request->session()->flash('message', 'Xóa thành công');
        return redirect()->action(
            'UserController@index'
        );
    }

    public function updateUser(Request $request)
    {
        $data = $request->all();
        $check = false;
        if (array_key_exists("username", $data)) {
            if($data['username'] !== $data['check_username']){
                $item = DB::table('users')->where('username', $data['username'])->first();
                if ($item) $check = true; else $data['username'];
            }else $data['username'];
        } else $data['username'] = "";
        if ($check) $request->session()->flash('message', 'Tên đăng nhập đã tồn tại');
        else {
            if (array_key_exists("fullname", $data)) $data['fullname']; else $data['fullname'] = "";
            if (array_key_exists("password", $data)) $data['password']; else $data['password'];
            if (array_key_exists("email", $data)) $data['email']; else $data['email'] = "";
            if (array_key_exists("group_scope", $data)) $data['group_scope']; else $data['group_scope'] = "";
            if (array_key_exists("dept_id", $data)) $data['dept_id']; else $data['dept_id'] = "";
            if (array_key_exists("status", $data)) $data['status']; else $data['status'] = "";
            if (array_key_exists("gender", $data)) $data['gender']; else $data['gender'] = "";
            if (array_key_exists("ma_tinh", $data)) $data['ma_tinh']; else $data['ma_tinh'] = "";
            if (array_key_exists("ma_cskcb", $data)) $data['ma_cskcb']; else $data['ma_cskcb'] = "";
            unset($data['_token']);
            unset($data['check_username']);
            if ($data['password'] == '') unset($data['password']); else $data['password'] = bcrypt($data['password']);
            if (trim($data['dept_id']) == "") {
                $data['dept_id'] = null;
            } else if (!is_numeric($data['dept_id'])) {
                $user_dept = UserDEPT::where("dept_name", '=', $data['dept_id'])->first();
                $create = [
                    'name' => $data['dept_id'],
                ];
                if (empty($user_dept)) {
                    $dept_info = UserDEPT::createUserDept($create);
                    $data['dept_id'] = $dept_info;
                }
            } else if (is_numeric($data['dept_id'])) {
                $user_dept = UserDEPT::where("id", '=', $data['dept_id'])->first();
                $create = [
                    'name' => $data['dept_id'],
                ];
                if (empty($user_dept)) {
                    $dept_info = UserDEPT::createUserDept($create);
                    $data['dept_id'] = $dept_info;
                }
            }
            if ($data['ma_tinh']) {
                $ma_tinh = DB::table('provinces')->where('id', $data['ma_tinh'])->first();
                $data['ma_tinh'] = $ma_tinh->ma_tinh;
                $data['dbhc_tinh'] = $ma_tinh->ma_tinh;
            }
            $result = User::updateUser($data);
            if ($result) {
                $request->session()->flash('status', 'success');
                $request->session()->flash('message', 'Cập nhật Thành công!');
            } else {
                $request->session()->flash('status', 'error');
                $request->session()->flash('message', 'Cập nhật Thành công!');
            }
        }
        return redirect(route('user-index'));
    }

    public function createUser(Request $request)
    {
        $data = $request->all();
        $check = false;
        if (array_key_exists("username", $data)) {
            $item = DB::table('users')->where('username', $data['username'])->first();
            if ($item) $check = true; else $data['username'];
        } else $data['username'] = "";
        if ($check) $request->session()->flash('message', 'Tên đăng nhập đã tồn tại');
        else {
            if (array_key_exists("password", $data)) $data['password']; else $data['password'] = "";
            if (array_key_exists("email", $data)) $data['email']; else $data['email'] = "";
            if (array_key_exists("fullname", $data)) $data['fullname']; else $data['fullname'] = "";
            if (array_key_exists("dept", $data)) $data['dept']; else $data['dept'] = "";
            if (array_key_exists("group", $data)) $data['group']; else $data['group'] = "";
            if (array_key_exists("gender", $data)) $data['gender']; else $data['gender'] = "";
            if (array_key_exists("status", $data)) $data['status']; else $data['status'] = "";
            if (array_key_exists("ma_tinh", $data)) $data['ma_tinh']; else $data['ma_tinh'] = "";
            if (array_key_exists("ma_cskcb", $data)) $data['ma_cskcb']; else $data['ma_cskcb'] = "";
            unset($data['_token']);
            if (trim($data['dept']) == "") {
                $data['dept'] = null;
            } else if (!is_numeric($data['dept'])) {
                $user_dept = UserDEPT::where("dept_name", '=', $data['dept'])->first();
                $create = [
                    'name' => $data['dept'],
                ];
                if (empty($user_dept)) {
                    $dept_info = UserDEPT::createUserDept($create);
                    $data['dept'] = $dept_info;
                }
            } else if (is_numeric($data['dept'])) {
                $user_dept = UserDEPT::where("id", '=', $data['dept'])->first();
                $create = [
                    'name' => $data['dept'],
                ];
                if (empty($user_dept)) {
                    $dept_info = UserDEPT::createUserDept($create);
                    $data['dept'] = $dept_info;
                }
            }
            $result = User::creteUser($data);
            if ($result) {
                $request->session()->flash('status', 'success');
                $request->session()->flash('message', 'Thêm mới Thành công!');
            } else {
                $request->session()->flash('status', 'error');
                $request->session()->flash('message', 'Thêm mới Thành công!');
            }
        }
        return redirect(route('user-index'));
    }

    public function viewinfo(Request $request)
    {
        $data = $request->all();
        $result = User::where("id", $data['id'])->first();
        $result->cate_code = is_null($result->cate_code) ? '' : $result->cate_code;
        if($result->ma_tinh) {
            $ma_tinh = DB::table('provinces')->where('ma_tinh',$result->ma_tinh)->first();
            $result->ma_tinh = $ma_tinh->id;
        }
        return response()->json($result);
    }

    public function ViewProfile(Request $request)
    {
        $u = session('uinfo')->username;
        $user = User::where("username", $u)->first();
        return view('user.view', ['user' => $user]);
    }

    public function updateinfo(Request $request)
    {
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        $listProvince = Utils::getProvince();
        $listDictrict = Utils::getDistrict();
        $data = $request->all();
        $user = Session::get('uinfo');
        $dataObj = categorySite::where("ma_cskcb", $user->ma_cskcb)->first();
        if (isset($data['submit'])) {
            if (array_key_exists("name", $data)) $data['name']; else $data['name'] = "";
            if (array_key_exists("address", $data)) $data['address']; else $data['address'] = "";
            if (array_key_exists("ma_tinh", $data)) $data['ma_tinh']; else $data['ma_tinh'] = "";
            if (array_key_exists("district_id", $data)) $data['district_id']; else $data['district_id'] = "";
            if (array_key_exists("can_bo_dau_moi", $data)) $data['can_bo_dau_moi']; else $data['can_bo_dau_moi'] = "";
            if (array_key_exists("dienthoai", $data)) $data['dienthoai']; else $data['dienthoai'] = "";
            if (array_key_exists("email", $data)) $data['email']; else $data['email'] = "";
            if (preg_match($pattern, $data['email']) === 1) {
                unset($data['submit']);
                unset($data['_token']);
                $province = DB::table('provinces')->where('id', $data['ma_tinh'])->first();
                $data['ma_tinh'] = $province ? $province->ma_tinh : '';
                if($data['id']) $result = categorySite::updatedSite($data);
                else {
                    unset($data['id']);
                    $data['ma_cskcb'] = $user->ma_cskcb;
                    $result = categorySite::createdSite($data);
                }
                if ($result) {
                    $dataObj['name'] = $data['name'];
                    $dataObj['address'] = $data['address'];
                    $dataObj['ma_tinh'] = $data['ma_tinh'];
                    $dataObj['district_id'] = $data['district_id'];
                    $dataObj['can_bo_dau_moi'] = $data['can_bo_dau_moi'];
                    $dataObj['dienthoai'] = $data['dienthoai'];
                    $dataObj['email'] = $data['email'];
                    DB::table('users')
                        ->where('username', $user->username)
                        ->update(['update_cs_info' => 1,'fullname' => $data['name']]);
                    Session::put('uinfo', DB::table('users')->where('username', $user->username)->first());
                    $request->session()->flash('message', 'Cập nhật thông tin cơ sở thành công');
                    return redirect(route('index'));
                } else {
                    $request->session()->flash('message', 'Cập nhật thất bại');
                }
            } else {
                $request->session()->flash('message', 'Email không đúng định dạng!');
            }
        }
        return view('user.updateInfo', ['list_province' => $listProvince, 'listDictrict' => $listDictrict, 'object' => $dataObj]);
    }

    public function updateinfotinh(Request $request)
    {
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        $listProvince = Utils::getProvince();
        $listDictrict = Utils::getDistrict();
        $data = $request->all();
        $dataObj = Session::get('uinfo');
        if (isset($data['submit'])) {
            if (array_key_exists("fullname", $data)) $data['fullname']; else $data['fullname'] = "";
            if (array_key_exists("diachi", $data)) $data['diachi']; else $data['diachi'] = "";
            if (array_key_exists("can_bo_dau_moi", $data)) $data['can_bo_dau_moi']; else $data['can_bo_dau_moi'] = "";
            if (array_key_exists("ma_tinh", $data)) $data['ma_tinh']; else $data['ma_tinh'] = "";
            if (array_key_exists("dienthoai", $data)) $data['dienthoai']; else $data['dienthoai'] = "";
            if (array_key_exists("email", $data)) $data['email']; else $data['email'] = "";
            if (preg_match($pattern, $data['email']) === 1) {
                unset($data['submit']);
                unset($data['_token']);
                $result = User::updateUser($data);
                if ($result) {
                    DB::table('users')
                        ->where('username', $dataObj->username)
                        ->update(['update_cs_info' => 1]);
                    Session::put('uinfo', DB::table('users')->where('username', $dataObj->username)->first());
                    $request->session()->flash('message', 'Cập nhật thông tin cơ sở thành công');
                    return redirect(route('index'));
                } else {
                    $request->session()->flash('message', 'Cập nhật thất bại');
                }
            } else {
                $request->session()->flash('message', 'Email không đúng định dạng!');
            }
        }
        return view('user.updateInfo_tinh', ['list_province' => $listProvince, 'listDictrict' => $listDictrict, 'object' => $dataObj]);
    }


    public function changePassword(Request $request)
    {
        $data = $request->all();
        $userinfo = \session('uinfo');
        if (isset($data['submit'])) {
            if (array_key_exists("password", $data)) $data['password']; else $data['password'] = "";
            if (array_key_exists("password_new", $data)) $data['password_new']; else $data['password_new'] = "";
            if (array_key_exists("re_password_new", $data)) $data['re_password_new']; else $data['re_password_new'] = "";
            if (Hash::check($data['password'], $userinfo->password)) {
                if ($data['password_new'] === $data['re_password_new']) {
                    $user = DB::table('users')
                        ->where('username', $userinfo->username)
                        ->update(['password' => bcrypt($data['password_new']), 'change_pass' => 1]);
                    session(['uinfo' => DB::table('users')->where('username', $userinfo->username)->first()]);
                    if ($user) {
                        $request->session()->flash('status', 'success');
                        $request->session()->flash('message', 'Đổi mật khẩu thành công!');
                        return redirect(route('index'));
                    } else {
                        $request->session()->flash('status', 'danger');
                        $request->session()->flash('message', 'Đổi mật khẩu thất bại');
                    }
                } else {
                    $request->session()->flash('status', 'danger');
                    $request->session()->flash('message', 'Mật khẩu mới không trùng khớp!');
                }
            } else {
                $request->session()->flash('status', 'danger');
                $request->session()->flash('message', 'Mật khẩu cũ không đúng!');
            }
        }
        return view('user.changepass');

    }

}
