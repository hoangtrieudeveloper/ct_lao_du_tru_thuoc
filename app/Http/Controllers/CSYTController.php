<?php


namespace App\Http\Controllers;


use App\Plan;
use App\Utils;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use PHPExcel;
use PHPExcel_IOFactory;

class CSYTController extends Controller
{

    public function index(Request $request)
    {
        $res = array();

        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();

        $res['plan'] = $plan;

        $user = Session::get('uinfo');
        $res['cskcb'] = DB::table('category_site')->where('ma_cskcb', $user->ma_cskcb)->first();
        $tinh = null;
        if (isset($user->dbhc_tinh)) {
            $tinh = DB::table('provinces')->where('ma_tinh', $user->dbhc_tinh)->first();
        }
        if (is_null($tinh)) {
            $tinh = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();
        }
        $res['tinh'] = $tinh;

        $selectData = DB::table('plan_import as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('ma_cskcb', $user->ma_cskcb)
            ->where('plan_id', $planId)
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt')
            ->get();
//        return ['data' => $selectData];
        $data = array();
        $tong = ['tong_so' => 0];
        $arrMaThuoc = array();
        foreach ($selectData as $row) {
            if (!in_array($row->ma_thuoc, $arrMaThuoc)) $arrMaThuoc[] = $row->ma_thuoc;
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $defaultThuoc = ['30.316.N4.300', '30.317.N3.250', '30.318.N3.625', '30.313.N4.50', '30.313.N4.300', '30.312.N3.400', '30.320.N4.1000', '30.315.N4.500'];
        $res['tong'] = $tong;
        $hasGiaiTrinh = false;
        $hasThuocKhac = false;
        foreach ($data as $ma => $row) {
            $data[$ma]['giaitrinh'] = false;
            if (array_key_exists('sudung', $row) && array_key_exists('tong_so', $row)) {
                if ($row['tong_so'] > $row['sudung'] * 1.3 || $row['tong_so'] < $row['sudung'] * .7) {
                    $data[$ma]['giaitrinh'] = true;
                    $hasGiaiTrinh = true;
                }
            }else{
                $hasGiaiTrinh = true;
                $data[$ma]['giaitrinh'] = true;
            }
            if (!in_array($ma, $defaultThuoc))
                $hasThuocKhac = true;
        }
        $res['hasThuocKhac'] = $hasThuocKhac;


        $existPlan = DB::table('plan_session')->where('ma_cskcb', $user->ma_cskcb)
            ->where('session_level', 1)
            ->where('trang_thai', '>', 0)
            ->first();
        $res['existPlan'] = $existPlan;
        $res['data'] = $data;
        $res['hasGiaiTrinh'] = $hasGiaiTrinh;
        $res['soCV'] = Plan::getSoCV($user->ma_cskcb);
        $res['listMedicine'] = DB::table('category_medicine')
            ->whereNotIn('code', $arrMaThuoc)
            ->orderBy("name", 'asc')
            ->get();
        return view('csyt.plan.index', $res);

    }

    public function importPlan(Request $request)
    {
        $res = array();
        if ($request->isMethod('get')) {
            $res['method'] = 'GET';
        } else {
            $res['method'] = 'POST';
            if ($request->hasFile('file')) {
                $inputFileName = $request->file('file')->getRealPath();
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }
                $sheet = $objPHPExcel->getSheet(0);
                $startRow = 10;
                $highestRow = $sheet->getHighestRow();
                $dataExcel = array();
                $dataValid = array();
                $arrMaThuoc = [];
                for ($row = $startRow; $row <= $highestRow; $row++) {
                    $temp = array();
                    $stt = $sheet->getCell('A' . $row)->getValue();
                    if (!is_null($stt) && is_numeric($stt)) {
                        $mess = '<ul>';
                        $temp['status'] = 1;

                        $ma_thuoc = Utils::masterTrim($sheet->getCell('B' . $row)->getValue());
                        $temp['stt'] = $stt;
                        $temp['ma_thuoc'] = Utils::masterTrim($ma_thuoc);
                        $temp['ten_hoat_chat'] = Utils::masterTrim($sheet->getCell('C' . $row)->getValue());
                        $temp['ham_luong'] = Utils::masterTrim($sheet->getCell('D' . $row)->getValue());
                        $temp['dang_bao_che'] = Utils::masterTrim($sheet->getCell('E' . $row)->getValue());
                        $temp['duong_dung'] = Utils::masterTrim($sheet->getCell('F' . $row)->getValue());
                        $temp['nhom_thuoc'] = Utils::masterTrim($sheet->getCell('G' . $row)->getValue());
                        $temp['dvt'] = Utils::masterTrim($sheet->getCell('H' . $row)->getValue());
                        $temp['sudung'] = trim($sheet->getCell('I' . $row)->getCalculatedValue());

                        $temp['20221'] = trim($sheet->getCell('J' . $row)->getCalculatedValue());
                        $temp['20222'] = trim($sheet->getCell('K' . $row)->getCalculatedValue());
                        $temp['20223'] = trim($sheet->getCell('L' . $row)->getCalculatedValue());
                        $temp['20224'] = trim($sheet->getCell('M' . $row)->getCalculatedValue());
                        $temp['20231'] = trim($sheet->getCell('N' . $row)->getCalculatedValue());
                        $temp['20232'] = trim($sheet->getCell('O' . $row)->getCalculatedValue());
                        $temp['20233'] = trim($sheet->getCell('P' . $row)->getCalculatedValue());
                        $temp['20234'] = trim($sheet->getCell('Q' . $row)->getCalculatedValue());

                        $temp['tongso'] = trim($sheet->getCell('R' . $row)->getCalculatedValue());
                        $temp = Plan::validate($temp);

                        $dataExcel[] = $temp;
                    }
                }
                $res['dataExcel'] = $dataExcel;
            }
        }
        return view('csyt.plan.import', $res);
    }

    public function submitValidData(Request $request)
    {
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $yearFrom = $plan->year_from;
        $yearTo = $plan->year_to;

        $dataValid = Session::get('dataValid');

        foreach ($dataValid as $row) {
            for ($year = $yearFrom; $year <= $yearTo; $year++) {
                for ($q = 1; $q <= 4; $q++) {
                    $quy = $year . $q;
                    Plan::updatePlanImport($row, $quy);
                }
            }
            Plan::updatePlanImport($row, 'sudung');
        }
        $user = Session::get('uinfo');
        Plan::updateSoCV($user->ma_cskcb);
        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Thành công!');
        return redirect(route('csyt-plan-index'));
    }

    public function submitPlan(Request $request)
    {
        $planId = 1;
        $user = Session::get('uinfo');
        $soCV = 'CSYT' . Plan::getSoCV($user->ma_cskcb);
//        $checkExist = DB::table("plan_session")->where('cong_van', $soCV)->first();
//        if (!is_null($checkExist)) {
//            $request->session()->flash('status', 'danger');
//            $request->session()->flash('message', 'Kế hoạch số ' . $soCV . ' đã được gửi lên Tỉnh từ trước đó rồi!');
//            return redirect(route('csyt-plan-index'));
//        }
        $selectData = DB::table('plan_import as plan')
            ->where('ma_cskcb', $user->ma_cskcb)
            ->where('plan_id', $planId)
            ->get();
        $jsonData = json_decode(json_encode($selectData), true);
//        return $jsonData;
        $dataFile = [
            'file_bckh' => ['title' => 'Báo cáo nhu cầu kế hoạch thuốc'],
            'file_bb_hop' => ['title' => 'Biên bản họp hội đồng thuốc và điều trị'],
            'file_cv_cam_ket' => ['title' => 'Công văn cam kết sử dụng ít nhất 80% nhu cầu dự trù'],
            'file_cv_giai_trinh' => ['title' => 'Công văn giải trình trong trường hợp cần thiết giải trình về việc dự trù thuốc tăng
                            giảm quá 30% so với lượng thuốc đã sử dụng giai đoạn 2019-2020'],
            'file_bao_gia' => ['title' => 'Báo giá của nhà cung ứng nếu có dự trù thuốc thuộc nhóm TCKT khác']
        ];
        foreach ($dataFile as $key => $row) {
            if ($key == 'file_bao_gia') {
                for ($i = 1; $i <= 3; $i++) {
                    $keyB = $key . $i;
                    if ($request->hasFile($keyB)) {
                        $file = $request->file($keyB);
                        $filename = $file->getClientOriginalName();
                        $file = $file->getRealPath();
                        $directory = base_path() . '/cv/' . $soCV . '/' . $key;
                        if (!file_exists($directory)) {
                            File::makeDirectory($directory, 0777, true);
                        }
                        move_uploaded_file($file, $directory . '/' . $filename);
                        if (!array_key_exists('file', $dataFile[$key])) $dataFile[$key]['file'] = [];
                        $dataFile[$key]['file'][] = $filename;
                    }
                }
            } else {
                if ($request->hasFile($key)) {
                    $file = $request->file($key);
                    $filename = $file->getClientOriginalName();
                    $file = $file->getRealPath();
                    $directory = base_path() . '/cv/' . $soCV . '/' . $key;
                    if (!file_exists($directory)) {
                        File::makeDirectory($directory, 0777, true);
                    }
                    move_uploaded_file($file, $directory . '/' . $filename);
                    $dataFile[$key]['file'] = $filename;
                }
            }
        }
        DB::table('plan_session')->where('ma_cskcb', $user->ma_cskcb)
            ->where('plan_id', $planId)
            ->where('session_level', 1)
//            ->where('trang_thai', 0)
            ->update(['trang_thai' => -3]);
        $dataSession = [
            'session_level' => 1,
            'ma_tinh' => $user->ma_tinh,
            'ma_cskcb' => $user->ma_cskcb,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $user->id,
            'cong_van' => $soCV,
            'trang_thai' => 0,
            'plan_id' => $planId,
            'file_attach' => json_encode($dataFile)
        ];
        $sesId = DB::table('plan_session')->insertGetId($dataSession);
        $dataRow = array();
        foreach ($jsonData as $row) {
            unset($row['id']);
            unset($row['plan_id']);
            unset($row['created_at']);
            unset($row['created_by']);
            unset($row['updated_at']);
            $row['ses_cs_id'] = $sesId;
            $row['trang_thai'] = 0;
            $dataRow[] = $row;
        }


        DB::table('plan_row')->insert($dataRow);
        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Gửi kế hoạch thành công!');
        return redirect(route('csyt-plan-index'));
    }

    public function session(Request $request)
    {
        $user = Session::get('uinfo');
        $planId = 1;
        $selectSession = DB::table('plan_session')
            ->where('ma_cskcb', $user->ma_cskcb)
            ->where('plan_id', $planId)
            ->orderBy('created_at', 'desc')
            ->get();
        $res = array();
        $res['session'] = $selectSession;

        return view('csyt.plan.session', $res);
    }

    public function viewSessionData(Request $request)
    {

        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();

        $sesId = $request->id;
        $selectData = $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('ses_cs_id', $sesId)
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt')
            ->get();
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }

        $html = '';
        $html .= '<table class="table table-bordered table-striped">';
        $html .= '<thead>';
        $html .= '<tr>
                        <th rowspan="3">Mã thuốc</th>
                        <th rowspan="3">Tên hoạt chất</th>
                        <th rowspan="3">Nồng độ, hàm lượng</th>
                        <th rowspan="3">Dạng bào chế</th>
                        <th rowspan="3">Đường dùng</th>
                        <th rowspan="3">Nhóm thuốc</th>
                        <th rowspan="3">Đơn vị tính</th>
                        <th rowspan="3"  style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo thống kê của
                            CTCLQG
                        </th>
                        <th colspan="8">Số lượng dự trù (24 tháng)</th>
                        <th rowspan="3">Tổng số lượng</th>
                    </tr>';
        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th colspan="4">' . $year . '</th>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th>Quý I</th>
                            <th>Quý II</th>
                            <th>Quý III</th>
                            <th>Quý IV</th>';
        }
        $html .= '</tr>';
        $html .= '</thead>';

        $html .= '<tbody>';
        foreach ($data as $row) {
            $html .= '<tr>
                            <td>' . $row['ma_thuoc'] . '</td>
                            <td>' . $row['ten_hoat_chat'] . '</td>
                            <td>' . $row['ham_luong'] . '</td>
                            <td>' . $row['dang_bao_che'] . '</td>
                            <td>' . $row['duong_dung'] . '</td>
                            <td>' . $row['nhom_thuoc'] . '</td>
                            <td>' . $row['dvt'] . '</td>
                            <td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row)) . '</td>';
            for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
                for ($q = 1; $q <= 4; $q++) {
                    $html .= '<td class="text-right" >' . Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row)) . '</td >';
                }
            }
            $html .= '<td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row)) . '</td>';
            $html .= '</tr>';
        }
        $html .= '<tr>';
        $html .= '<td colspan="17"><strong>Tổng ' . count($data) . ' khoản</strong></td>';

        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }

    public function createPlan(Request $request)
    {
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $data = $request->all();
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            for ($q = 1; $q <= 4; $q++) {
                if (array_key_exists($year . ($q), $data)) $data[$year . ($q)]; else $data[$year . ($q)] = "";
            }
        }
        if (array_key_exists("ma_thuoc", $data)) $data['ma_thuoc']; else $data['ma_thuoc'] = "";
        if (array_key_exists("sudung", $data)) $data['sudung']; else $data['sudung'] = "";
        $ma_thuoc = $data['ma_thuoc'];
        $user = Session::get('uinfo');
        unset($data['_token']);
        unset($data['ma_thuoc']);

        foreach ($data as $k => $v) {
            $object = [
                'plan_id' => 1,
                'ma_thuoc' => $ma_thuoc,
                'quy' => $k,
                'so_luong' => $v,
                'created_at' => date("Y-m-d H:i:s"),
                'ma_tinh' => $user->ma_tinh,
                'ma_cskcb' => $user->ma_cskcb,
                'created_by' => $user->id,
            ];
            DB::table('plan_import')->insertGetId($object);
        }
        $user = Session::get('uinfo');
        Plan::updateSoCV($user->ma_cskcb);
        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Thêm mới Thành công!');
        return redirect(route('csyt-plan-index'));
    }

    public function updatePlan(Request $request)
    {
        $data = $request->all();
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $data = $request->all();
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            for ($q = 1; $q <= 4; $q++) {
                if (array_key_exists($year . ($q), $data)) $data[$year . ($q)]; else $data[$year . ($q)] = "";
            }
        }
        if (array_key_exists("ma_thuoc", $data)) $data['ma_thuoc']; else $data['ma_thuoc'] = "";
        if (array_key_exists("ma_cskcb", $data)) $data['ma_cskcb']; else $data['ma_cskcb'] = "";
        if (array_key_exists("sudung", $data)) $data['sudung']; else $data['sudung'] = "";
        $ma_thuoc = $data['ma_thuoc'];
        $ma_cskcb = $data['ma_cskcb'];
        unset($data['_token']);
        unset($data['ma_cskcb']);
        unset($data['ma_thuoc']);
        $selectData = DB::table('plan_import')
            ->where('ma_cskcb', $ma_cskcb)
            ->where('ma_thuoc', $ma_thuoc)
            ->get();
        foreach ($selectData as $v) {
            if (array_key_exists($v->quy, $data)) {
                DB::table('plan_import')
                    ->where('ma_cskcb', $ma_cskcb)
                    ->where('ma_thuoc', $ma_thuoc)
                    ->where('quy', $v->quy)
                    ->update(['so_luong' => $data[$v->quy]]);
            }
        }
        $user = Session::get('uinfo');
        Plan::updateSoCV($user->ma_cskcb);
        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Cập nhật Thành công!');
        return redirect(route('csyt-plan-index'));
    }

    public function viewinfo(Request $request)
    {
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();

        $data = $request->all();
        $selectData = DB::table('plan_import as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('plan.ma_cskcb', $data['ma_cskcb'])
            ->where('plan.ma_thuoc', $data['ma_thuoc'])
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt')
            ->get();
        $array = [];
        $quySudung = [];
        foreach ($selectData as $v) {
            for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
                for ($q = 1; $q <= 4; $q++) {
                    if ($year . $q === $v->quy) {
                        $array[$year][] = $v;
                    }
                    if ($v->quy === 'sudung') {
                        $quySudung = $v;
                    }
                }
            }
        }
        $result['0'] = $selectData[0];
        $result['1'] = $array;
        $result['2'] = $plan;
        $result['3'] = $quySudung;
        return response()->json($result);
    }

    public function viewinfoDmthuoc(Request $request)
    {
        $data = $request->all();
        $result = DB::table('category_medicine')
            ->where('code', $data['code'])
            ->first();
        return response()->json($result);
    }

    public function deletePlan(Request $request)
    {
        $ma_thuoc_delete = $request->ma_thuoc_delete;
        $ma_cskcb_delete = $request->ma_cskcb_delete;
        $selectData = DB::table('plan_import')
            ->where('ma_cskcb', $ma_cskcb_delete)
            ->where('ma_thuoc', $ma_thuoc_delete)
            ->delete();
        $user = Session::get('uinfo');
        Plan::updateSoCV($user->ma_cskcb);
        $request->session()->flash('message', 'Xóa thành công');
        return redirect()->action(
            'CSYTController@index'
        );
    }

    public function testTime()
    {
        $time = date('Y-m-d H:i:s');
        DB::table('plan_session')->where('id', 2)->update(['created_at' => $time]);
        return $time;
    }

    public function checkRow(Request $request)
    {
        $data = $request->all();
        $idx = $data['idx'];
        $data = Plan::validate($data);
        $html = Plan::rowInport($data, $idx);
        return ['view' => $html];
    }

    public function submitImport(Request $request)
    {

        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $yearFrom = $plan->year_from;
        $yearTo = $plan->year_to;

        $data = $request->all();
        if (array_key_exists('status', $data)) {
            foreach ($data['status'] as $idx => $status) {
                if ($status != 'invalid') {
                    $row = [
                        'ma_thuoc' => $data['ma_thuoc'][$idx],
                        'sudung' => $data['sudung'][$idx]
                    ];
                    for ($year = $yearFrom; $year <= $yearTo; $year++) {
                        for ($q = 1; $q <= 4; $q++) {
                            $quy = $year . $q;
                            $row[$quy] = $data[$quy][$idx];
                            Plan::updatePlanImport($row, $quy);
                        }
                    }
                    Plan::updatePlanImport($row, 'sudung');
                }
            }
        }


        $user = Session::get('uinfo');
        Plan::updateSoCV($user->ma_cskcb);
        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Thành công!');
        return redirect(route('csyt-plan-index'));

    }

    public function changeMa(Request $request){
        $maCu = $request->ma_cu;
        $maMoi = $request->ma_moi;
        DB::table('category_site')->where('ma_cskcb', $maCu)->update(['ma_cskcb' => $maMoi]);
        DB::table('plan_session')->where('ma_cskcb', $maCu)->update(['ma_cskcb' => $maMoi]);
        DB::table('plan_import')->where('ma_cskcb', $maCu)->update(['ma_cskcb' => $maMoi]);
        DB::table('plan_row')->where('ma_cskcb', $maCu)->update(['ma_cskcb' => $maMoi]);
        DB::table('users')->where('ma_cskcb', $maCu)
            ->where('group_scope', 3)
            ->update(['ma_cskcb' => $maMoi, 'username'=> $maMoi]);
        return "Thành công";
    }
}
