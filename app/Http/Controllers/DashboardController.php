<?php

namespace App\Http\Controllers;

use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('uinfo')->group_scope == 2) {
            return redirect(route('prov-csyt-session'));
        }
        if (Session::get('uinfo')->group_scope == 3) {
            return redirect(route('csyt-plan-index'));
        }
        if (Session::get('uinfo')->group_scope == 4) {
            return redirect(route('center-prov-session-tw'));
        }
        return view('dashboard');
    }

    public function validateSite()
    {
        $fileName = base_path() . "/template/hf_list.xlsx";
        $inputFileType = \PHPExcel_IOFactory::identify($fileName);
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($fileName);
        $sheet = $objPHPExcel->getSheet(0);
        $startRow = 2;
        $highestRow = $sheet->getHighestRow();
        $dictSite = array();
        for ($row = $startRow; $row <= $highestRow; $row++) {
            $ma = trim($sheet->getCell('A' . $row)->getValue());
            $ten = trim($sheet->getCell('B' . $row)->getValue());
            $dictSite[$ma] = $ten;
        }
        $selectSite = DB::table('category_site')
            ->where('ma_cskcb', 'not like', '%L%')
            ->select('ma_cskcb', 'name')->get();

        $result = array();
        $text= "";
        foreach ($selectSite as $row) {
            if (!array_key_exists($row->ma_cskcb, $dictSite)) {
                $result[] = [
                    'mess' => "Không tồn tại",
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->name,
                ];
                $text .= $row->ma_cskcb . ' - ' . $row->name . PHP_EOL;
            }
        }
        return $text;
//        return $result;
    }
}
