<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DMController extends Controller
{
    public static function search(Request $request, $table, $fKey)
    {
        $label = explode(',', $request->label);
        $search = explode(',', $request->search);
        $key = $request->has('key') ? $request->key : '';

        $stmt = DB::table($table);
        $stmt = $stmt->where(function ($query) use ($search, $key) {
            foreach ($search as $idx => $col) {
                if ($idx === 0) {
                    $query->where($col, 'LIKE', '%' . $key . '%');
                } else {
                    $query->orWhere($col, 'LIKE', '%' . $key . '%');
                }
            }
        });
        if ($table == 'category_site' && $request->has('ma_tinh') && $request->ma_tinh != '') {
            $stmt->where('ma_tinh', $request->ma_tinh);
        }
        $select = $stmt->limit(50)->offset(0)->get();
        $select = json_decode(json_encode($select), true);
        $data = array();
        foreach ($select as $row) {
            $arrText = array();
            foreach ($label as $lb) {
                if (array_key_exists($lb, $row)) {
                    $arrText[] = $row[$lb];
                }
            }
            $data[] = [
                'id' => $row[$fKey],
                'text' => implode('-', $arrText)
            ];
        }
        return $data;
    }
}