<?php

namespace App\Http\Controllers;

use App\Unit;
use App\User;
use App\UserDEPT;
use Illuminate\Http\Request;

class UserDeptController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $user_dept = UserDEPT::all();
        return view('userDept.index', ['data' => $user_dept]);
    }

    public function delete(Request $request)
    {
        $row_code = $request->row_code;
        UserDEPT::deleteUserDept($row_code);
        $request->session()->flash('message', 'Xóa thành công');
        return redirect()->action(
            'UserDeptController@index'
        );
    }

    public function update(Request $request)
    {
        $data = $request->all();
        if (array_key_exists("name", $data)) $data['name']; else $data['name'] = "";
        unset($data['_token']);
        $result = UserDEPT::updateUserDept($data);
        if($result){
            $request->session()->flash('status', 'success');
            $request->session()->flash('message', 'Cập nhât Thành công!');
        }else{
            $request->session()->flash('status', 'error');
            $request->session()->flash('message', 'Cập nhật Thành công!');
        }
        return redirect(route('User-dept-index'));
    }

    public function create(Request $request)
    {
        $data = $request->all();
        if (array_key_exists("name", $data)) $data['name']; else $data['name'] = "";
        unset($data['_token']);
        $result = UserDEPT::createUserDept($data);
        if($result){
            $request->session()->flash('status', 'success');
            $request->session()->flash('message', 'Thêm mới Thành công!');
        }else{
            $request->session()->flash('status', 'error');
            $request->session()->flash('message', 'Thêm mới Thành công!');
        }
        return redirect(route('User-dept-index'));
    }

    public function viewinfo(Request $request)
    {
        $data = $request->all();
        $result = UserDEPT::where("id", $data['id'])->get();
        return response()->json($result);
    }


}
