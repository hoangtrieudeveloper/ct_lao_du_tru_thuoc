<?php


namespace App\Http\Controllers;


use App\categorymedicine;
use App\categorySite;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryMedicineController extends Controller
{
    public function index()
    {
        $data = categorymedicine::orderBy('id', 'desc')->get();
        return view('categorymedicine.index', ['data' => $data]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $category_obj = DB::table('category_medicine')->where("id",$id)->first();
        if (isset($data['submit'])) {
            if (array_key_exists("code", $data)) $data['code']; else $data['code'] = "";
            if (array_key_exists("name", $data)) $data['name']; else $data['name'] = "";
            if (array_key_exists("cell_type", $data)) $data['cell_type']; else $data['cell_type'] = "";
            if (array_key_exists("concentration", $data)) $data['concentration']; else $data['concentration'] = "";
            if (array_key_exists("type", $data)) $data['type']; else $data['type'] = "";
            if (array_key_exists("group_tckt", $data)) $data['group_tckt']; else $data['group_tckt'] = "";
            if (array_key_exists("unit", $data)) $data['unit']; else $data['unit'] = "";
            if (array_key_exists("ctcl_planning", $data)) $data['ctcl_planning']; else $data['ctcl_planning'] = "";
            unset($data['submit']);
            unset($data['_token']);
            $result = categorymedicine::updatedMedicine($data);
            if ($result) {
                $request->session()->flash('message', 'Cập nhật thành công');
                return redirect()->action(
                    'CategoryMedicineController@index'
                );
            } else {
                $request->session()->flash('message', 'Cập nhật thất bại');
            }
        }
        return view('categorymedicine.update', ['category_obj' => $category_obj]);
    }

    public function create(Request $request)
    {
        $data = $request->all();
        if (isset($data['submit'])) {
            if (array_key_exists("code", $data)) $data['code']; else $data['code'] = "";
            if (array_key_exists("name", $data)) $data['name']; else $data['name'] = "";
            if (array_key_exists("cell_type", $data)) $data['cell_type']; else $data['cell_type'] = "";
            if (array_key_exists("concentration", $data)) $data['concentration']; else $data['concentration'] = "";
            if (array_key_exists("type", $data)) $data['type']; else $data['type'] = "";
            if (array_key_exists("group_tckt", $data)) $data['group_tckt']; else $data['group_tckt'] = "";
            if (array_key_exists("unit", $data)) $data['unit']; else $data['unit'] = "";
            if (array_key_exists("ctcl_planning", $data)) $data['ctcl_planning']; else $data['ctcl_planning'] = "";
            unset($data['submit']);
            unset($data['_token']);
            $result = categorymedicine::createdMedicine($data);
            if ($result) {
                $request->session()->flash('message', 'Thêm mới thành công');
            } else {
                $request->session()->flash('message', 'Thêm mới thất bại');
            }
        }
        return view('categorymedicine.create', []);
    }

    public function delete(Request $request)
    {
        $row_code = $request->row_code;
        categorymedicine::deleteCate($row_code);
        $request->session()->flash('message', 'Xóa thành công');
        return redirect()->action(
            'CategoryMedicineController@index'
        );
    }

    public function viewprofile(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $object = DB::table('category_medicine')->where("id",$id)->first();
        return view('categorymedicine.viewinfo', ['data' => $object]);
    }
}

