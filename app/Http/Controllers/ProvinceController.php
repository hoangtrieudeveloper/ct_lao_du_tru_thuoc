<?php


namespace App\Http\Controllers;


use App\GenView;
use App\Plan;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class ProvinceController extends Controller
{
    public function csytSession(Request $request)
    {
        $user = Session::get('uinfo');


        $planId = 1;
        $selectSession = DB::table('plan_session as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->where('plan.ma_tinh', $user->ma_tinh)
            ->where('plan.plan_id', $planId)
            ->where('plan.session_level', 1)
            ->whereNotIn('plan.trang_thai', [-3])
            ->orderBy('plan.created_at', 'desc')
            ->select('plan.*', 'site.name as ten_cskcb')
            ->get();
        $res = array();
        $res['session'] = $selectSession;

        $lstCoSo = DB::table('category_site')->where('ma_tinh', $user->ma_tinh)->orderBy('ma_cskcb', 'asc')->get();
        $selectCoSoSend = DB::table('plan_session')
            ->where('ma_tinh', $user->ma_tinh)
            ->where('plan_id', $planId)
            ->where('session_level', 1)
            ->where('trang_thai', '>=', 0)
            ->select(DB::raw("DISTINCT(ma_cskcb)"))->pluck('ma_cskcb')->toArray();
        $res['lstCoSo'] = $lstCoSo;
        $res['csGuiKH'] = $selectCoSoSend;
//        return $res;
        return view('prov.plan.session', $res);
    }



    public function viewSessionData(Request $request)
    {

        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $sesId = $request->id;
        $session = DB::table('plan_session as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->where('plan.id', $sesId)
            ->select('plan.*', 'site.name as ten_cskcb')->first();

        $selectData = $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('ses_cs_id', $sesId)
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt')
            ->get();
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }

        $html = '';
        $html .= '<div class="pull-right">';
        if (in_array($session->trang_thai, [-1, 0, 2])) {
            $html .= '<a class="btn btn-success btn-sm" href="javascript:confirmSession(1)">Xác nhận</a>
<a class="btn btn-danger btn-sm" style="margin-left: 10px" href="javascript:confirmSession(0)">Từ chối</a>';
        }
        $html .= '</div>';
        $html .= '<div>Số kế hoạch: <strong>' . $session->cong_van . '</strong></div>';
        $html .= '<div>Mã CSKCB: <strong>' . $session->ma_cskcb . '</strong></div>';
        $html .= '<div>Tên CSKCB: <strong>' . $session->ten_cskcb . '</strong></div>';
        $html .= '<input id="session_id_xu_ly" hidden value="' . $session->id . '"/>';
        if (in_array($session->trang_thai, [-1, 0, 2])) {
            $html .= '<div class="form-group">
<label>Ghi chú cho Cơ sở y tế</label>
<textarea name="ghi_chu" id="input-ghi-chu" class="form-control"></textarea>
</div>';
        }
        $html .= '<table class="table table-bordered table-striped">';
        $html .= '<thead>';
        $html .= '<tr>
                        <th rowspan="3">Mã thuốc</th>
                        <th rowspan="3">Tên hoạt chất</th>
                        <th rowspan="3">Nồng độ, hàm lượng</th>
                        <th rowspan="3">Dạng bào chế</th>
                        <th rowspan="3">Đường dùng</th>
                        <th rowspan="3">Nhóm thuốc</th>
                        <th rowspan="3">Đơn vị tính</th>
                        <th rowspan="3"  style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo thống kê của
                            CTCLQG
                        </th>
                        <th colspan="8">Số lượng dự trù (24 tháng)</th>
                        <th rowspan="3">Tổng số lượng</th>
                    </tr>';
        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th colspan="4">' . $year . '</th>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th>Quý I</th>
                            <th>Quý II</th>
                            <th>Quý III</th>
                            <th>Quý IV</th>';
        }
        $html .= '</tr>';
        $html .= '</thead>';

        $html .= '<tbody>';
        foreach ($data as $row) {
            $html .= '<tr>
                            <td>' . $row['ma_thuoc'] . '</td>
                            <td>' . $row['ten_hoat_chat'] . '</td>
                            <td>' . $row['ham_luong'] . '</td>
                            <td>' . $row['dang_bao_che'] . '</td>
                            <td>' . $row['duong_dung'] . '</td>
                            <td>' . $row['nhom_thuoc'] . '</td>
                            <td>' . $row['dvt'] . '</td>
                            <td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row)) . '</td>';
            for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
                for ($q = 1; $q <= 4; $q++) {
                    $html .= '<td class="text-right" >' . Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row)) . '</td >';
                }
            }
            $html .= '<td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row)) . '</td>';
            $html .= '</tr>';
        }
        $html .= '<tr>';
        $html .= '<td colspan="17"><strong>Tổng ' . count($data) . ' khoản</strong></td>';

        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }

    public function confirmSession(Request $request)
    {
        $id = $request->id;
        $confirm = $request->confirm;
        $ghi_chu = $request->ghi_chu;
        $trang_thai = $confirm == "1" ? 2 : -1;
        DB::table('plan_session')->where('id', $id)->update([
            'trang_thai' => $trang_thai,
            'ghi_chu' => $ghi_chu
        ]);
        $user = Session::get('uinfo');
        Plan::updateSoCV($user->ma_tinh);
        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Xử lý thành công!');
        return ['status' => 'sussess'];
    }

    public function plan(Request $request)
    {
        $res = array();
        $filter = $request->all();
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $res['plan'] = $plan;
        $user = Session::get('uinfo');

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $query = [];
        if (array_key_exists('ma_cskcb', $filter)) {
            $site = DB::table('category_site')->where('ma_cskcb', $filter['ma_cskcb'])->first();
            if (is_null($site)) {
                unset($filter['ma_cskcb']);
            } else {
                $query[] = ['ses.ma_cskcb', '=', $filter['ma_cskcb']];
                $res['site'] = $site;
            }
        }

        if (array_key_exists('ma_thuoc', $filter)) {
            $thuoc = DB::table('category_medicine')->where('code', $filter['ma_thuoc'])->first();
            if (is_null($thuoc)) {
                unset($filter['ma_thuoc']);
            } else {
                $query[] = ['plan.ma_thuoc', '=', $filter['ma_thuoc']];
                $res['thuoc'] = $thuoc;
            }
        }
        $res['filter'] = $filter;

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_cs_id')
            ->where('ses.ma_tinh', $user->ma_tinh)
            ->where('ses.plan_id', $planId)
            ->where('ses.session_level', 1)
            ->where('ses.trang_thai', 2)
            ->where($query)
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                'site.name as ten_cskcb')
            ->get();
        $res['data'] = $selectData;
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_cskcb . $row->ma_thuoc, $data)) {
                $data[$row->ma_cskcb . $row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->ten_cskcb,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_cskcb . $row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_cskcb . $row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        usort($data, function ($item1, $item2) {
            return $item1['ma_cskcb'] > $item2['ma_cskcb'];
        });
        $hasGiaiTrinh = false;
        foreach ($data as $ma => $row) {
            if ($row['tong_so'] > $row['sudung'] * 1.3 || $row['tong_so'] < $row['sudung'] * .7) {
                $data[$ma]['giaitrinh'] = true;
                $hasGiaiTrinh = true;
            } else {
                $data[$ma]['giaitrinh'] = false;
            }
        }
        $res['hasGiaiTrinh'] = $hasGiaiTrinh;
        $res['tong'] = $tong;
        $res['data'] = $data;
        $res['soCV'] = Plan::getSoCV($user->ma_tinh);
        $res['tHead'] = Utils::genTheadM03($plan);
//        return $res;
        return view('prov.plan.plan', $res);
    }

    public function planExport(Request $request)
    {
        $res = array();
        $filter = $request->all();
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $res['plan'] = $plan;
        $user = Session::get('uinfo');

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $query = [];
        if (array_key_exists('ma_cskcb', $filter)) {
            $site = DB::table('category_site')->where('ma_cskcb', $filter['ma_cskcb'])->first();
            if (is_null($site)) {
                unset($filter['ma_cskcb']);
            } else {
                $query[] = ['ses.ma_cskcb', '=', $filter['ma_cskcb']];
                $res['site'] = $site;
            }
        }

        if (array_key_exists('ma_thuoc', $filter)) {
            $thuoc = DB::table('category_medicine')->where('code', $filter['ma_thuoc'])->first();
            if (is_null($thuoc)) {
                unset($filter['ma_thuoc']);
            } else {
                $query[] = ['plan.ma_thuoc', '=', $filter['ma_thuoc']];
                $res['thuoc'] = $thuoc;
            }
        }
        $res['filter'] = $filter;

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_cs_id')
            ->where('ses.ma_tinh', $user->ma_tinh)
            ->where('ses.plan_id', $planId)
            ->where('ses.session_level', 1)
            ->where('ses.trang_thai', 2)
            ->where($query)
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                'site.name as ten_cskcb')
            ->get();
        $res['data'] = $selectData;
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_cskcb . $row->ma_thuoc, $data)) {
                $data[$row->ma_cskcb . $row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->ten_cskcb,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_cskcb . $row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_cskcb . $row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        usort($data, function ($item1, $item2) {
            return $item1['ma_cskcb'] > $item2['ma_cskcb'];
        });
        $res['tong'] = $tong;
        $res['data'] = $data;

        $fileName = base_path() . "/template/template_03.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 9;
        $sheet->setCellValue('A1', Session::get('uinfo')->fullname);
        foreach ($data as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
            $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
            $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
            $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
            $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
            $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
            $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
            $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
            $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
            $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
            $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
            $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
            $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
            $sheet->setCellValue('S' . ($startRow + $idx), $row['ten_cskcb']);
        }

        $idxSum = $startRow + count($data);
        $sheet->setCellValue('A' . $idxSum, "Tổng " . count($data) . " khoản");

        $sheet->mergeCells("A" . $idxSum . ":S" . $idxSum);
        $sheet->getStyle("A" . $startRow . ":S" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle("A" . $idxSum . ":S" . $idxSum)->applyFromArray($styleArray);

        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Báo cáo kế hoạch dự trù mẫu 03_' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }


    public function planByMedicine(Request $request)
    {
        $res = array();
        $filter = $request->all();
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $res['plan'] = $plan;
        $user = Session::get('uinfo');

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $query = [];

        if (array_key_exists('ma_thuoc', $filter)) {
            $thuoc = DB::table('category_medicine')->where('code', $filter['ma_thuoc'])->first();
            if (is_null($thuoc)) {
                unset($filter['ma_thuoc']);
            } else {
                $query[] = ['plan.ma_thuoc', '=', $filter['ma_thuoc']];
                $res['thuoc'] = $thuoc;
            }
        }
        $res['filter'] = $filter;

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_cs_id')
            ->where('ses.ma_tinh', $user->ma_tinh)
            ->where('ses.plan_id', $planId)
            ->where('ses.session_level', 1)
            ->where('ses.trang_thai', 2)
            ->where($query)
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $res['data'] = $selectData;
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        usort($data, function ($item1, $item2) {
            return $item1['ma_thuoc'] > $item2['ma_thuoc'];
        });
        $hasGiaiTrinh = false;
        foreach ($data as $ma => $row) {
            if ($row['tong_so'] > $row['sudung'] * 1.3 || $row['tong_so'] < $row['sudung'] * .7) {
                $data[$ma]['giaitrinh'] = true;
                $hasGiaiTrinh = true;
            } else {
                $data[$ma]['giaitrinh'] = false;
            }
        }
        $res['tong'] = $tong;
        $res['data'] = $data;
        $res['hasGiaiTrinh'] = $hasGiaiTrinh;
        $res['soCV'] = Plan::getSoCV($user->ma_tinh);
//        return $res;
        return view('prov.mau_02.plan', $res);
    }

    public function planByMedicineExport(Request $request)
    {
        $res = array();
        $filter = $request->all();
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $res['plan'] = $plan;
        $user = Session::get('uinfo');

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $query = [];

        if (array_key_exists('ma_thuoc', $filter)) {
            $thuoc = DB::table('category_medicine')->where('code', $filter['ma_thuoc'])->first();
            if (is_null($thuoc)) {
                unset($filter['ma_thuoc']);
            } else {
                $query[] = ['plan.ma_thuoc', '=', $filter['ma_thuoc']];
                $res['thuoc'] = $thuoc;
            }
        }
        $res['filter'] = $filter;

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_cs_id')
            ->where('ses.ma_tinh', $user->ma_tinh)
            ->where('ses.plan_id', $planId)
            ->where('ses.session_level', 1)
            ->where('ses.trang_thai', 2)
            ->where($query)
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $res['data'] = $selectData;
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        usort($data, function ($item1, $item2) {
            return $item1['ma_thuoc'] > $item2['ma_thuoc'];
        });
        $res['tong'] = $tong;
        $res['data'] = $data;

        $fileName = base_path() . "/template/template_01.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 9;
        $sheet->setCellValue('A1', Session::get('uinfo')->fullname);
        $sheet->setCellValue('P1', "Mẫu 02");
        foreach ($data as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
            $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
            $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
            $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
            $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
            $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
            $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
            $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
            $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
            $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
            $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
            $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
            $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
        }

        $idxSum = $startRow + count($data);
        $sheet->setCellValue('A' . $idxSum, "Tổng " . count($data) . " khoản");

        $sheet->mergeCells("A" . $idxSum . ":R" . $idxSum);
        $sheet->getStyle("A" . $startRow . ":R" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle("A" . $idxSum . ":R" . $idxSum)->applyFromArray($styleArray);

        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Báo cáo kế hoạch dự trù mẫu 02_' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }

    public function submitPlan(Request $request)
    {
        $user = Session::get('uinfo');
        $existPlan = DB::table('plan_session')->where('ma_tinh', $user->ma_tinh)
            ->where('session_level', 2)
            ->where('trang_thai', '=', 1)
            ->first();
        if(!is_null($existPlan)){
            $request->session()->flash('status', 'danger');
            $request->session()->flash('message', 'Kế hoạch số ' . $existPlan->cong_van . ' đã được trung ương xác nhận, không thể gửi thêm kế hoạch');
            return redirect(route('prov-plan-by-medicine'));
        }
        $planId = 1;

        $soCV = 'TTP' . Plan::getSoCV($user->ma_tinh);
        $status = 3;

        $dataFile = [
            'mau_02' => ['title' => 'Báo cáo tổng hợp kế hoạch theo thuốc (Mẫu 02)'],
            'mau_03' => ['title' => 'Báo cáo tổng hợp kế hoạch theo cơ sở y tế (Mẫu 03)'],
            'mau_04' => ['title' => 'Thông tin hành chính các cơ sở y tế (Mẫu 04)'],
            'file_cv_giai_trinh' => ['title' => 'Công văn giải trình trong trường hợp cần thiết giải trình về việc dự trù thuốc tăng
                            giảm quá 30% so với lượng thuốc đã sử dụng giai đoạn 2019-2020'],
            'bb_syt' => ['title' => 'Biên bản thẩm định của sở y tế']
        ];
        foreach ($dataFile as $key => $row) {

            if ($request->hasFile($key)) {
                $file = $request->file($key);
                $filename = $file->getClientOriginalName();
                $file = $file->getRealPath();
                $directory = base_path() . '/cv/' . $soCV . '/' . $key;
                if (!file_exists($directory)) {
                    File::makeDirectory($directory, 0777, true);
                }
                move_uploaded_file($file, $directory . '/' . $filename);
                $dataFile[$key]['file'] = $filename;
            }
        }

        $dataSession = [
            'session_level' => 2,
            'ma_tinh' => $user->ma_tinh,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $user->id,
            'cong_van' => $soCV,
            'trang_thai' => $status,
            'plan_id' => $planId,
            'file_attach' => json_encode($dataFile)
        ];
        DB::table('plan_session as ses')
            ->where('ses.ma_tinh', $user->ma_tinh)
            ->where('ses.plan_id', $planId)
            ->where('ses.session_level', 2)
//            ->where('ses.trang_thai', 3)
            ->update(['trang_thai' => -3]);

        $sesId = DB::table('plan_session')->insertGetId($dataSession);

        DB::table('plan_row as plan')
            ->whereIn('ses_cs_id', (
            DB::table('plan_session as ses')
                ->where('ses.ma_tinh', $user->ma_tinh)
                ->where('ses.plan_id', $planId)
                ->where('ses.session_level', 1)
                ->where('ses.trang_thai', 2)
                ->pluck('id')
            ))->update(['ses_tinh_id' => $sesId]);
//        DB::table('plan_session as ses')
//            ->where('ses.ma_tinh', $user->ma_tinh)
//            ->where('ses.plan_id', $planId)
//            ->where('ses.session_level', 1)
//            ->where('ses.trang_thai', 2)->update(['trang_thai' => $status]);

        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Gửi kế hoạch thành công!');
        return redirect(route('prov-session-sent'));
    }

    public function sessionSent(Request $request)
    {
        $user = Session::get('uinfo');
        $planId = 1;
        $selectSession = DB::table('plan_session')
            ->where('ma_tinh', $user->ma_tinh)
            ->where('plan_id', $planId)
            ->where('session_level', 2)
            ->orderBy('created_at', 'desc')
            ->get();
        $res = array();
        $res['session'] = $selectSession;

        return view('prov.plan.session_sent', $res);
    }

    public function sessionSentRow(Request $request)
    {
        return GenView::viewSessionM03($request->id);
    }

    public function infoSite(Request $request)
    {

        $user = Session::get('uinfo');
        $res = array();

        $res['tinh'] = DB::table('provinces')
            ->where('ma_tinh', $user->ma_tinh)->first();

        $query = [];
        $query[] = ['ma_tinh', '=', $user->ma_tinh];
//        $query[] = ['can_bo_dau_moi', 'not', null];

        $macs = DB::table('plan_session')
            ->where('ma_tinh', $user->ma_tinh)
            ->where('session_level', 1)
            ->where('trang_thai',2)
            ->pluck('ma_cskcb')->toArray();


        $lstCoSo = DB::table('category_site')
            ->where($query)
            ->whereIn('ma_cskcb', $macs)
            ->orderBy('ma_cskcb', 'asc')->get();
        $res['data'] = $lstCoSo;
        $res['tHead'] = '<thead>
                        <tr>
                            <th rowspan="2">TT</th>
                            <th rowspan="2">Tên Cơ sở y tế</th>
                            <th rowspan="2">Mã KBCB CSYT</th>
                            <th rowspan="2">Địa chỉ chi tiết của cơ sở y tế</th>
                            <th colspan="3">Thông tin liên hệ</th>
                        </tr>
                        <tr>
                            <th>Cán bộ đầu mối</th>
                            <th>Điện thoại</th>
                            <th>Email</th>
                        </tr>
                        </thead>';
        return view('prov.mau_04.index', $res);
    }

    public function infoSiteExport(Request $request)
    {
        $user = Session::get('uinfo');
        $res = array();

        $res['tinh'] = DB::table('provinces')->where('ma_tinh', $user->ma_tinh)->first();

        $query = [];
        $query[] = ['ma_tinh', '=', $user->ma_tinh];

        $data = DB::table('category_site')
            ->where($query)
            ->whereNotNull('can_bo_dau_moi')
            ->orderBy('ma_cskcb', 'asc')->get();
        $data = json_decode(json_encode($data), true);
        $fileName = base_path() . "/template/template_04.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 8;
        $sheet->setCellValue('A1', "Sở Y tế/ Đơn vị đầu mối chống Lao " . $res['tinh']->name);
        foreach ($data as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), $row['name']);
            $sheet->setCellValue('C' . ($startRow + $idx), $row['ma_cskcb']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['address']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['can_bo_dau_moi']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['dienthoai']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['email']);

        }

        $idxSum = $startRow + count($data);

        $sheet->getStyle("A" . $startRow . ":G" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );


        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Thông tin hành chính cơ sở y tế mẫu 02_' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }
}
