<?php


namespace App\Http\Controllers;


use App\categorySite;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategorySiteController extends Controller
{

    public function index()
    {
        $data = categorySite::orderBy('id', 'desc')->get();
        $selectProvince = Utils::getProvince();
        $dictProvince = array();
        foreach ($selectProvince as $row) {
            $dictProvince[$row->ma_tinh] = ['name' => $row->name, 'number_code' => $row->number_code, 'ma_tinh' => $row->ma_tinh];
        }
        $selectDistrict = DB::table('districts')->get();
        $dictDistricts = array();
        foreach ($selectDistrict as $row) {
            $dictDistricts[$row->id] = $row->name;
        }

        return view('categorysite.index', ['data' => $data, 'dictProvince' => $dictProvince, 'dictDistrict' => $dictDistricts]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $listProvince = Utils::getProvince();
        $listDictrict = Utils::getDistrict();
        $id = $data['id'];
        $category_obj = DB::table('category_site')->where("id", $id)->first();
        if (isset($data['submit'])) {
            $check = false;
            if (array_key_exists("ma_cskcb", $data)) {
                if($data['ma_cskcb'] !== $category_obj->ma_cskcb){
                    $item = DB::table('category_site')->where('ma_cskcb', $data['ma_cskcb'])->first();
                    if ($item) $check = true; else $data['ma_cskcb'];
                }else $data['ma_cskcb'];
            } else $data['ma_cskcb'] = "";
            if ($check) $request->session()->flash('message', 'Mã CSKCB đã tồn tại');
            else {
                if (array_key_exists("name", $data)) $data['name']; else $data['name'] = "";
                if (array_key_exists("address", $data)) $data['address']; else $data['address'] = "";
                if (array_key_exists("ma_tinh", $data)) $data['ma_tinh']; else $data['ma_tinh'] = "";
//            if (array_key_exists("province_id", $data)) $data['province_id']; else $data['province_id'] = "";
//            if (array_key_exists("district_id", $data)) $data['district_id']; else $data['district_id'] = "";
                unset($data['submit']);
                unset($data['_token']);
                $result = categorySite::updatedSite($data);
                if ($result) {
                    $request->session()->flash('message', 'Cập nhật thành công');
                    return redirect()->action(
                        'CategorySiteController@index'
                    );
                } else {
                    $request->session()->flash('message', 'Cập nhật thất bại');
                }
            }
        }
        return view('categorysite.update', ['category_obj' => $category_obj, 'list_province' => $listProvince, 'listDictrict' => $listDictrict]);
    }

    public function create(Request $request)
    {
        $listProvince = Utils::getProvince();
        $listDictrict = Utils::getDistrict();
        $data = $request->all();
        if (isset($data['submit'])) {
            $check = false;
            if (array_key_exists("ma_cskcb", $data)) {
                $item = DB::table('category_site')->where('ma_cskcb', $data['ma_cskcb'])->first();
                if ($item) $check = true; else $data['ma_cskcb'];
            } else $data['ma_cskcb'] = "";
            if ($check) $request->session()->flash('message', 'Mã CSKCB đã tồn tại');
            else {
                if (array_key_exists("name", $data)) $data['name']; else $data['name'] = "";
                if (array_key_exists("address", $data)) $data['address']; else $data['address'] = "";
                if (array_key_exists("ma_tinh", $data)) $data['ma_tinh']; else $data['ma_tinh'] = "";
//            if (array_key_exists("province_id", $data)) $data['province_id']; else $data['province_id'] = "";
//            if (array_key_exists("district_id", $data)) $data['district_id']; else $data['district_id'] = "";
                unset($data['submit']);
                unset($data['_token']);
                $result = categorySite::createdSite($data);
                if ($result) {
                    $request->session()->flash('message', 'Thêm mới thành công');
                } else {
                    $request->session()->flash('message', 'Thêm mới thất bại');
                }
            }
        }
        return view('categorysite.create', ['list_province' => $listProvince, 'listDictrict' => $listDictrict]);
    }

    public function delete(Request $request)
    {
        $row_code = $request->row_code;
        categorySite::deleteCate($row_code);
        $request->session()->flash('message', 'Xóa thành công');
        return redirect()->action(
            'CategorySiteController@index'
        );
    }

    public function viewprofile(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $object = DB::table('category_site')->where("id", $id)->first();

        $selectProvince = Utils::getProvince();
        $dictProvince = array();
        foreach ($selectProvince as $row) {
            $dictProvince[$row->ma_tinh] = ['name' => $row->name, 'number_code' => $row->number_code, 'ma_tinh' => $row->ma_tinh];
        }
        $selectDistrict = DB::table('districts')->get();
        $dictDistricts = array();
        foreach ($selectDistrict as $row) {
            $dictDistricts[$row->id] = $row->name;
        }
        return view('categorysite.viewinfo', ['data' => $object, 'dictProvince' => $dictProvince, 'dictDistrict' => $dictDistricts]);
    }
}

