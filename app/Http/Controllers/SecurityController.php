<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SecurityController extends Controller
{
    public function login(Request $request)
    {
        if ($request->session()->exists('uinfo')) {
            return redirect()->route('index');
        }
        $data = $request->all();
        if (isset($data['submit'])) {
            if ($data['username']) {
                $result = DB::table('users')
                    ->where('username', $data['username'])
                    ->first();
                if (!empty($result->id)) {
                    if (Hash::check($data['password'],$result->password)) {
                        session(['uinfo' => $result]);
                        return redirect()->action(
                            'DashboardController@index'
                        );
                    }else{
                        $request->session()->flash('message', 'Tài khoản hoặc mật khẩu hiện tại không đúng');
                    }
                } else {
                    $request->session()->flash('message', 'Tài khoản hoặc mật khẩu hiện tại không đúng');
                }
            }
        }
        return view('security.login');
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->action(
            'SecurityController@login'
        );
    }

}
