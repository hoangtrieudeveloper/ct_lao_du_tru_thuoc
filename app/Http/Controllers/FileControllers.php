<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FileControllers extends Controller
{
    public function downloadSessionCV(Request $request)
    {
        $sesId = $request->id;
        $session = DB::table('plan_session')->where('id', $sesId)->first();
        if (is_null($session)) return response()->json(['message' => 'Not Found.'], 404);
        $fileAttach = json_decode($session->file_attach, true);
        $key = $request->key;
        if ($key == 'file_bao_gia') {
            $fid = $request->fid;
            $file = base_path() . '/cv/' . $session->cong_van . '/' . $key . '/' . $fileAttach[$key]['file'][$fid];
        } else {
            $file = base_path() . '/cv/' . $session->cong_van . '/' . $key . '/' . $fileAttach[$key]['file'];
        }
        if (file_exists($file)) {
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
            if($request->has("download")) {
                $size = filesize($file);
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($file) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . $size);
                $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
                if ($size > $chunksize) {
                    $handle = fopen($file, 'rb');
                    $buffer = '';
                    while (!feof($handle)) {
                        $buffer = fread($handle, $chunksize);
                        echo $buffer;
                        ob_flush();
                        flush();
                    }
                    fclose($handle);
                } else {
                    readfile($file);
                }
            }else{
                if($ext == 'pdf') {
                    header("Content-type: application/pdf");
                    header("Content-Disposition: inline; filename=" . basename($file));
                    @readfile($file);
                }else{
                    header("Content-type: image/".$ext);
                    header("Content-Disposition: inline; filename=" . basename($file));
                    @readfile($file);
                }
            }
        } else {
            return response()->json(['message' => 'Not Found.'], 404);
        }
    }
}