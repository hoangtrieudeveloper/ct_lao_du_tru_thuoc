<?php

namespace App\Http\Controllers;

use App\Scopes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupUserController extends Controller
{
    public function index()
    {
        $res = array();
        $res['groups'] = DB::table('user_group')->get();
        return view('user.group.index', $res);
    }

    //
    public function create(Request $request)
    {

        if ($request->isMethod('get')) {
            $res = array();
            $res['scopes'] = Scopes::getTree();
            return view('user.group.create', $res);
        } else {
            $data = $request->all();
            DB::table('user_group')->insert([
                'group_name' => $data['name'],
                'group_roles' => implode('|', $data['scope'])
            ]);
            return redirect(route('group-user-index'));
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('get')) {
            $res = array();
            $res['scopes'] = Scopes::getTree();
            $res['group'] = DB::table('user_group')->where('id', '=', $request->id)->first();
            $res['curent'] = explode('|', $res['group']->group_roles);
            return view('user.group.update', $res);
        } else {
            $data = $request->all();
            DB::table('user_group')
                ->where('id', '=', $request->id)
                ->update([
                    'group_name' => $data['name'],
                    'group_roles' => implode('|', $data['scope'])
                ]);
            return redirect(route('group-user-update', ['id' => $request->id]));
        }
    }
    public function deleteUserGroup(Request $request)
    {
        $row_code = $request->row_code;
        DB::table('user_group')->where('id', $row_code)->delete();
        $request->session()->flash('message', 'Xóa thành công');
        return redirect()->action(
            'GroupUserController@index'
        );
    }
}
