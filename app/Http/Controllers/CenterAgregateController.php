<?php


namespace App\Http\Controllers;


use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CenterAgregateController extends Controller
{
    public function mau05(Request $request)
    {
        $res = array();
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $res['plan'] = $plan;

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_tinh_id')
            ->where('ses.session_level', 2)
            ->where('ses.trang_thai', 1)
            ->orderBy('plan.ma_thuoc', 'asc')
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_filter($data, function ($v, $k) {
            return $v['tong_so'] > 0;
        }, ARRAY_FILTER_USE_BOTH);
        $data = array_values($data);

        $res['data'] = $data;
        return view('center.tong_hop.mau05', $res);
    }

    public function mau05Export(Request $request)
    {
        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_tinh_id')
            ->where('ses.session_level', 2)
            ->where('ses.trang_thai', 1)
            ->orderBy('plan.ma_thuoc', 'asc')
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $res['data'] = $selectData;
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_filter($data, function ($v, $k) {
            return $v['tong_so'] > 0;
        }, ARRAY_FILTER_USE_BOTH);
        $dt = array_values($data);

        $fileName = base_path() . "/template/tw_05.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 10;
        foreach ($dt as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
            $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
            $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
            $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
            $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
            $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
            $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
            $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
            $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
            $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
            $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
            $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
            $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
        }

        $idxSum = $startRow + count($dt);
        $sheet->setCellValue('A' . $idxSum, "Tổng " . count($dt) . " khoản");

        $sheet->mergeCells("A" . $idxSum . ":R" . $idxSum);
        $sheet->getStyle("A" . $startRow . ":R" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle("A" . $idxSum . ":R" . $idxSum)->applyFromArray($styleArray);

        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="(Mẫu 05)Tổng hợp kế hoạch đã xác nhận theo thuốc-' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }

    public function mau06(Request $request)
    {
        $res = array();
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $res['plan'] = $plan;

        $filter = $request->all();
        $query = [];
        if ($request->has('ma_tinh') && $request->ma_tinh != '') {
            $tinh = DB::table('provinces')->where('ma_tinh', $request->ma_tinh)->first();
            if (is_null($tinh)) {
                unset($filter['ma_tinh']);
            } else {
                $res['tinh'] = $tinh;
                $query[] = ['ses.ma_tinh', '=', $request->ma_tinh];
            }
        }

        if ($request->has('ma_thuoc') && $request->ma_thuoc != '') {
            $thuoc = DB::table('category_medicine')->where('code', $request->ma_thuoc)->first();
            if (is_null($thuoc)) {
                unset($filter['ma_thuoc']);
            } else {
                $query[] = ['plan.ma_thuoc', '=', $request->ma_thuoc];
                $res['thuoc'] = $thuoc;
            }
        }
        $res['filter'] = $filter;

        $dictTinh = Utils::dictTinh();
        $res['dictTinh'] = $dictTinh;
        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_tinh_id')
            ->where('ses.session_level', 2)
            ->where('ses.trang_thai', 1)
            ->where($query)
            ->orderBy('ses.ma_tinh', 'asc')
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type', 'ses.ma_tinh',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che', 'ses.ma_tinh',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc . $row->ma_tinh, $data)) {
                $data[$row->ma_thuoc . $row->ma_tinh] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ma_tinh' => $row->ma_tinh,
                    'ten_tinh' => $dictTinh[$row->ma_tinh],
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }

            $data[$row->ma_thuoc . $row->ma_tinh][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc . $row->ma_tinh]['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_filter($data, function ($v, $k) {
            return $v['tong_so'] > 0;
        }, ARRAY_FILTER_USE_BOTH);
        $data = array_values($data);

        $res['data'] = $data;

        return view('center.tong_hop.mau06', $res);
    }

    public function mau06Export(Request $request)
    {
        $query = [];
        if ($request->has('ma_tinh') && $request->ma_tinh != '') {
            $query[] = ['ses.ma_tinh', '=', $request->ma_tinh];
        }

        if ($request->has('ma_thuoc') && $request->ma_thuoc != '') {
            $query[] = ['plan.ma_thuoc', '=', $request->ma_thuoc];
        }

        $dictTinh = Utils::dictTinh();

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_tinh_id')
            ->where('ses.session_level', 2)
            ->where('ses.trang_thai', 1)
            ->where($query)
            ->orderBy('ses.ma_tinh', 'asc')
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type', 'ses.ma_tinh',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che', 'ses.ma_tinh',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $res['data'] = $selectData;
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc . $row->ma_tinh, $data)) {
                $data[$row->ma_thuoc . $row->ma_tinh] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ma_tinh' => $row->ma_tinh,
                    'ten_tinh' => $dictTinh[$row->ma_tinh],
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }

            $data[$row->ma_thuoc . $row->ma_tinh][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc . $row->ma_tinh]['tong_so'] += intval($row->so_luong);
            }
        }
        $dt = array_values($data);

        $fileName = base_path() . "/template/tw_06.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 9;
        foreach ($dt as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
            $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
            $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
            $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
            $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
            $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
            $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
            $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
            $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
            $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
            $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
            $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
            $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
            $sheet->setCellValue('S' . ($startRow + $idx), $row['ten_tinh']);
        }

        $idxSum = $startRow + count($dt);
        $sheet->setCellValue('A' . $idxSum, "Tổng " . count($dt) . " khoản");

        $sheet->mergeCells("A" . $idxSum . ":R" . $idxSum);
        $sheet->getStyle("A" . $startRow . ":R" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle("A" . $idxSum . ":R" . $idxSum)->applyFromArray($styleArray);

        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="(Mẫu 06)Tổng hợp kế hoạch đã xác nhận theo Tỉnh/TP-' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }

    public function mau07(Request $request){
        $res = array();
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $res['plan'] = $plan;

        $filter = $request->all();
        $query = [];
        if ($request->has('ma_tinh') && $request->ma_tinh != '') {
            $tinh = DB::table('provinces')->where('ma_tinh', $request->ma_tinh)->first();
            if (is_null($tinh)) {
                unset($filter['ma_tinh']);
            } else {
                $res['tinh'] = $tinh;
                $query[] = ['ses.ma_tinh', '=', $request->ma_tinh];
            }
        }

        if ($request->has('ma_cskcb') && $request->ma_cskcb != '') {
            $site = DB::table('category_site')->where('ma_cskcb', $request->ma_cskcb)->first();
            if (is_null($site)) {
                unset($filter['ma_cskcb']);
            } else {
                $res['site'] = $site;
                $query[] = ['plan.ma_cskcb', '=', $request->ma_cskcb];
            }
        }

        if ($request->has('ma_thuoc') && $request->ma_thuoc != '') {
            $thuoc = DB::table('category_medicine')->where('code', $request->ma_thuoc)->first();
            if (is_null($thuoc)) {
                unset($filter['ma_thuoc']);
            } else {
                $query[] = ['plan.ma_thuoc', '=', $request->ma_thuoc];
                $res['thuoc'] = $thuoc;
            }
        }
        $res['filter'] = $filter;

        $dictTinh = Utils::dictTinh();
        $res['dictTinh'] = $dictTinh;

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_tinh_id')
            ->where('ses.session_level', 2)
            ->where('ses.trang_thai', 1)
            ->where($query)
            ->orderBy('ses.ma_tinh', 'asc')
            ->orderBy('plan.ma_cskcb', 'asc')
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                'site.name as ten_cskcb')
            ->get();
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_cskcb . $row->ma_thuoc, $data)) {
                $data[$row->ma_cskcb . $row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->ten_cskcb,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            $data[$row->ma_cskcb . $row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_cskcb . $row->ma_thuoc]['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_filter($data, function ($v, $k) {
            return $v['tong_so'] > 0;
        }, ARRAY_FILTER_USE_BOTH);
        $data = array_values($data);

        $res['data'] = $data;

        return view('center.tong_hop.mau07', $res);
    }

    public function mau07Export(Request $request)
    {
        $query = [];
        if ($request->has('ma_tinh') && $request->ma_tinh != '') {
                $query[] = ['ses.ma_tinh', '=', $request->ma_tinh];
        }

        if ($request->has('ma_cskcb') && $request->ma_cskcb != '') {
                $query[] = ['plan.ma_cskcb', '=', $request->ma_cskcb];
        }

        if ($request->has('ma_thuoc') && $request->ma_thuoc != '') {
                $query[] = ['plan.ma_thuoc', '=', $request->ma_thuoc];
        }
        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_tinh_id')
            ->where('ses.session_level', 2)
            ->where('ses.trang_thai', 1)
            ->where($query)
            ->orderBy('ses.ma_tinh', 'asc')
            ->orderBy('plan.ma_cskcb', 'asc')
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                'site.name as ten_cskcb')
            ->get();
        $res['data'] = $selectData;
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_cskcb . $row->ma_thuoc, $data)) {
                $data[$row->ma_cskcb . $row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->ten_cskcb,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            $data[$row->ma_cskcb . $row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_cskcb . $row->ma_thuoc]['tong_so'] += intval($row->so_luong);
            }
        }
        $dt = array_values($data);

        $fileName = base_path() . "/template/tw_07.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 9;
        foreach ($dt as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
            $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
            $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
            $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
            $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
            $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
            $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
            $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
            $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
            $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
            $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
            $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
            $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
            $sheet->setCellValue('S' . ($startRow + $idx), $row['ten_cskcb']);
        }

        $idxSum = $startRow + count($dt);
        $sheet->setCellValue('A' . $idxSum, "Tổng " . count($dt) . " khoản");

        $sheet->mergeCells("A" . $idxSum . ":R" . $idxSum);
        $sheet->getStyle("A" . $startRow . ":R" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle("A" . $idxSum . ":R" . $idxSum)->applyFromArray($styleArray);

        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="(Mẫu 07)Tổng hợp kế hoạch đã xác nhận theo Cơ sở y tế-' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }

    public function mau08(Request $request){
        $res = array();
        $filter = $request->all();
        $query = [];
        if ($request->has('ma_tinh') && $request->ma_tinh != '') {
            $tinh = DB::table('provinces')->where('ma_tinh', $request->ma_tinh)->first();
            if (is_null($tinh)) {
                unset($filter['ma_tinh']);
            } else {
                $res['tinh'] = $tinh;
                $query[] = ['ma_tinh', '=', $request->ma_tinh];
            }
        }

        if ($request->has('ma_cskcb') && $request->ma_cskcb != '') {
            $site = DB::table('category_site')->where('ma_cskcb', $request->ma_cskcb)->first();
            if (is_null($site)) {
                unset($filter['ma_cskcb']);
            } else {
                $res['site'] = $site;
                $query[] = ['ma_cskcb', '=', $request->ma_cskcb];
            }
        }

        $res['filter'] = $filter;

        $dictTinh = Utils::dictTinh();
        $res['dictTinh'] = $dictTinh;
        $macs = DB::table('plan_session')
            ->where('session_level', 1)
            ->where('trang_thai',2)
            ->pluck('ma_cskcb')->toArray();

        $data = DB::table('category_site')
            ->where($query)
            ->whereNotNull('can_bo_dau_moi')
            ->whereNotNull('ma_cskcb')
            ->whereIn('ma_cskcb', $macs)
            ->orderBy('ma_cskcb', 'asc')->get();
        $res['data'] = $data;
        return view('center.tong_hop.mau08', $res);
    }

    public function mau08export(Request $request)
    {
        $query = [];
        if ($request->has('ma_tinh') && $request->ma_tinh != '') {
                $query[] = ['ma_tinh', '=', $request->ma_tinh];
        }

        if ($request->has('ma_cskcb') && $request->ma_cskcb != '') {
                $query[] = ['ma_cskcb', '=', $request->ma_cskcb];
        }
        $macs = DB::table('plan_session')
            ->where('session_level', 1)
            ->where('trang_thai',2)
            ->pluck('ma_cskcb')->toArray();
        $data = DB::table('category_site')
            ->where($query)
            ->whereNotNull('can_bo_dau_moi')
            ->whereNotNull('ma_cskcb')
            ->whereIn('ma_cskcb', $macs)
            ->orderBy('ma_cskcb', 'asc')->get();
        $dt = json_decode(json_encode($data), true);
        $dictTinh = Utils::dictTinh();

        $fileName = base_path() . "/template/template_08.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 9;
        foreach ($dt as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), array_key_exists($row['ma_tinh'], $dictTinh)?$dictTinh[$row['ma_tinh']]:'');
            $sheet->setCellValue('C' . ($startRow + $idx), $row['name']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['ma_cskcb']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['address']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['can_bo_dau_moi']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['dienthoai']);
            $sheet->setCellValue('H' . ($startRow + $idx), $row['email']);
        }

        $idxSum = $startRow + count($data);

        $sheet->getStyle("A" . $startRow . ":G" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );


        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="(Mẫu 08)Thông tin hành chính Cơ sở y tế-' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }

    public function exportCsyt()
    {
        $TINH = ['01', '12', '42', '87', '56'];
        $selectTinh = DB::table('provinces')->whereIn('ma_tinh', $TINH)->get();
        $dictTinh = array();
        foreach ($selectTinh as $row) {
            $dictTinh[$row->ma_tinh] = $row->name;
        }
        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_cs_id')
            ->whereIn('ses.ma_tinh', $TINH)
            ->where('ses.session_level', 1)
            ->where('ses.trang_thai', 2)
            ->orderBy('ses.ma_tinh', 'asc')
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                'site.name as ten_cskcb')
            ->get();
        $res['data'] = $selectData;
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_cskcb . $row->ma_thuoc, $data)) {
                $data[$row->ma_cskcb . $row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->ten_cskcb,
                    'ma_tinh' => $row->ma_tinh,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            $data[$row->ma_cskcb . $row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_cskcb . $row->ma_thuoc]['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        $temp = array();
        foreach ($data as $row) {
            if (!array_key_exists($row['ma_cskcb'], $temp)) $temp[$row['ma_cskcb']] = [];
            $temp[$row['ma_cskcb']][] = $row;
        }
        $fileName = base_path() . "/template/template_01.xlsx";
        foreach ($temp as $dt) {
            $excelobj = \PHPExcel_IOFactory::load($fileName);
            $excelobj->setActiveSheetIndex(0);
            $excelobj->getActiveSheet()->toArray(null, true, true, true);
            $sheet = $excelobj->getActiveSheet();
            $startRow = 9;
            $ten_tinh = $dictTinh[$dt[0]['ma_tinh']];
            $sheet->setCellValue('A1', 'Mã CSYT: ' . $dt[0]['ma_cskcb']);
            $sheet->setCellValue('A2', 'Tên CSYT: ' . $dt[0]['ten_cskcb']);
            $sheet->setCellValue('A3', 'Tỉnh/TP: ' . $ten_tinh);
            $sheet->setCellValue('P1', "Mẫu 01");
            foreach ($dt as $idx => $row) {
                $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
                $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
                $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
                $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
                $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
                $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
                $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
                $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
                $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
                $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
                $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
                $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
                $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
                $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
                $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
                $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
                $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
                $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
            }

            $idxSum = $startRow + count($dt);
            $sheet->setCellValue('A' . $idxSum, "Tổng " . count($dt) . " khoản");

            $sheet->mergeCells("A" . $idxSum . ":R" . $idxSum);
            $sheet->getStyle("A" . $startRow . ":R" . $idxSum)->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                )
            );
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            );
            $sheet->getStyle("A" . $idxSum . ":R" . $idxSum)->applyFromArray($styleArray);

            $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
            $objWriter->save(base_path() . "/public/temp/" . $ten_tinh . '_' . $dt[0]['ma_cskcb'] . '_' . $dt[0]['ten_cskcb'] . '.xlsx');
        }
        return $temp;
    }

    public function exportTinh()
    {
        $TINH = ['01', '12', '42', '87', '56'];
        $selectTinh = DB::table('provinces')->whereIn('ma_tinh', $TINH)->get();
        $dictTinh = array();
        foreach ($selectTinh as $row) {
            $dictTinh[$row->ma_tinh] = $row->name;
        }
        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_cs_id')
            ->whereIn('ses.ma_tinh', $TINH)
            ->where('ses.session_level', 1)
            ->where('ses.trang_thai', 2)
            ->orderBy('ses.ma_tinh', 'asc')
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type', 'ses.ma_tinh',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che', 'ses.ma_tinh',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $res['data'] = $selectData;
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc . $row->ma_tinh, $data)) {
                $data[$row->ma_thuoc . $row->ma_tinh] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ma_tinh' => $row->ma_tinh,
                    'ten_tinh' => $dictTinh[$row->ma_tinh],
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }

            $data[$row->ma_thuoc . $row->ma_tinh][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc . $row->ma_tinh]['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        $temp = array();
        foreach ($data as $row) {
            if (!array_key_exists($row['ma_tinh'], $temp)) $temp[$row['ma_tinh']] = [];
            $temp[$row['ma_tinh']][] = $row;
        }
        $fileName = base_path() . "/template/template_01.xlsx";
        foreach ($temp as $dt) {
            $excelobj = \PHPExcel_IOFactory::load($fileName);
            $excelobj->setActiveSheetIndex(0);
            $excelobj->getActiveSheet()->toArray(null, true, true, true);
            $sheet = $excelobj->getActiveSheet();
            $startRow = 9;
            $ten_tinh = $dictTinh[$dt[0]['ma_tinh']];
            $sheet->setCellValue('A1', 'Tỉnh/TP: ' . $ten_tinh);
            $sheet->setCellValue('P1', "Mẫu 02");
            foreach ($dt as $idx => $row) {
                $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
                $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
                $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
                $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
                $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
                $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
                $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
                $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
                $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
                $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
                $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
                $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
                $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
                $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
                $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
                $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
                $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
                $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
            }

            $idxSum = $startRow + count($dt);
            $sheet->setCellValue('A' . $idxSum, "Tổng " . count($dt) . " khoản");

            $sheet->mergeCells("A" . $idxSum . ":R" . $idxSum);
            $sheet->getStyle("A" . $startRow . ":R" . $idxSum)->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                )
            );
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            );
            $sheet->getStyle("A" . $idxSum . ":R" . $idxSum)->applyFromArray($styleArray);

            $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
            $objWriter->save(base_path() . "/public/temp/" . $ten_tinh . '.xlsx');
        }
        return $temp;
    }

    public function exportMau03(Request $request)
    {
        $TINH = ['01', '12', '42', '87', '56'];
        $selectTinh = DB::table('provinces')->whereIn('ma_tinh', $TINH)->get();
        $dictTinh = array();
        foreach ($selectTinh as $row) {
            $dictTinh[$row->ma_tinh] = $row->name;
        }
        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->leftJoin('plan_session as ses', 'ses.id', '=', 'plan.ses_cs_id')
            ->whereIn('ses.ma_tinh', $TINH)
            ->where('ses.session_level', 1)
            ->where('ses.trang_thai', 2)
            ->orderBy('ses.ma_cskcb', 'asc')
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                'site.name as ten_cskcb')
            ->get();
        $res['data'] = $selectData;
        $data = array();
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_cskcb . $row->ma_thuoc, $data)) {
                $data[$row->ma_cskcb . $row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->ten_cskcb,
                    'ma_tinh' => $row->ma_tinh,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            $data[$row->ma_cskcb . $row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_cskcb . $row->ma_thuoc]['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        $temp = array();
        foreach ($data as $row) {
            if (!array_key_exists($row['ma_tinh'], $temp)) $temp[$row['ma_tinh']] = [];
            $temp[$row['ma_tinh']][] = $row;
        }

        $fileName = base_path() . "/template/template_03.xlsx";

        foreach ($temp as $dt) {
            $excelobj = \PHPExcel_IOFactory::load($fileName);
            $excelobj->setActiveSheetIndex(0);
            $excelobj->getActiveSheet()->toArray(null, true, true, true);
            $sheet = $excelobj->getActiveSheet();
            $startRow = 9;
            $ten_tinh = $dictTinh[$dt[0]['ma_tinh']];
            $sheet->setCellValue('A1', 'Tỉnh/TP: ' . $ten_tinh);
            $sheet->setCellValue('P1', "Mẫu 03");
            foreach ($dt as $idx => $row) {
                $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
                $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
                $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
                $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
                $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
                $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
                $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
                $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
                $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
                $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
                $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
                $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
                $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
                $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
                $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
                $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
                $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
                $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
                $sheet->setCellValue('S' . ($startRow + $idx), $row['ten_cskcb']);
            }

            $idxSum = $startRow + count($dt);
            $sheet->setCellValue('A' . $idxSum, "Tổng " . count($dt) . " khoản");

            $sheet->mergeCells("A" . $idxSum . ":S" . $idxSum);
            $sheet->getStyle("A" . $startRow . ":S" . $idxSum)->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                )
            );
            $styleArray = array(
                'font' => array(
                    'bold' => true,
                ),
                'alignment' => array(
                    'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            );
            $sheet->getStyle("A" . $idxSum . ":S" . $idxSum)->applyFromArray($styleArray);

            $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
            $objWriter->save(base_path() . "/public/temp/" . $ten_tinh . '_Mẫu 03.xlsx');
        }
        return $temp;
    }

    public function exportMau04()
    {
        $TINH = ['01', '12', '42', '87', '56'];
        $selectTinh = DB::table('provinces')->whereIn('ma_tinh', $TINH)->get();
        $dictTinh = array();
        foreach ($selectTinh as $row) {
            $dictTinh[$row->ma_tinh] = $row->name;
        }

        $data = DB::table('category_site')->whereIn('ma_tinh', $TINH)->orderBy('ma_cskcb', 'asc')->get();
        $data = json_decode(json_encode($data), true);
        $temp = array();
        foreach ($data as $row) {
            if (!array_key_exists($row['ma_tinh'], $temp)) $temp[$row['ma_tinh']] = [];
            $temp[$row['ma_tinh']][] = $row;
        }

        $fileName = base_path() . "/template/template_04.xlsx";

        foreach ($temp as $dt) {
            $excelobj = \PHPExcel_IOFactory::load($fileName);
            $excelobj->setActiveSheetIndex(0);
            $excelobj->getActiveSheet()->toArray(null, true, true, true);
            $sheet = $excelobj->getActiveSheet();
            $startRow = 9;
            $ten_tinh = $dictTinh[$dt[0]['ma_tinh']];
            $sheet->setCellValue('A1', 'Tỉnh/TP: ' . $ten_tinh);
            $sheet->setCellValue('P1', "Mẫu 04");
            foreach ($dt as $idx => $row) {
                $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
                $sheet->setCellValue('B' . ($startRow + $idx), $row['name']);
                $sheet->setCellValue('C' . ($startRow + $idx), $row['ma_cskcb']);
                $sheet->setCellValue('D' . ($startRow + $idx), $row['address']);
                $sheet->setCellValue('E' . ($startRow + $idx), $row['can_bo_dau_moi']);
                $sheet->setCellValue('F' . ($startRow + $idx), $row['dienthoai']);
                $sheet->setCellValue('G' . ($startRow + $idx), $row['email']);
            }

            $idxSum = $startRow + count($data);

            $sheet->getStyle("A" . $startRow . ":G" . $idxSum)->applyFromArray(
                array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                )
            );


            $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
            $objWriter->save(base_path() . "/public/temp/" . $ten_tinh . '_Mẫu 04.xlsx');
        }
        return $temp;
    }


}