<?php


namespace App\Http\Controllers;


use App\GenView;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CenterFollowController extends Controller
{


    public function syncPro(Request $request)
    {
        $ma_tinh = $request->ma_tinh;

        $macs = DB::table('plan_session')->where('ma_tinh', $ma_tinh)
            ->where('trang_thai', 2)->pluck('ma_cskcb')->toArray();
        $dataImport = DB::table('plan_import')->where('ma_tinh', $ma_tinh)
            ->whereIn('ma_cskcb', $macs)
            ->get();
        $dataImport = json_decode(json_encode($dataImport), true);
        foreach ($dataImport as $idx => $row) {
            unset($dataImport[$idx]['id']);
        }
        DB::connection('pro')->table('plan_import')->insert($dataImport);

        $dataSesion = DB::table('plan_session')->where('ma_tinh', $ma_tinh)
            ->where('trang_thai', 2)
            ->where('session_level', 1)
            ->get();
        $dataSesion = json_decode(json_encode($dataSesion), true);

        $dataRow = DB::table('plan_row')->where('ma_tinh', $ma_tinh)->get();
        $dataRow = json_decode(json_encode($dataRow), true);

        $dictRow = array();
        foreach ($dataRow as $row) {
            if (!array_key_exists($row['ses_cs_id'], $dictRow)) $dictRow[$row['ses_cs_id']] = [];
            unset($row['id']);
            $dictRow[$row['ses_cs_id']][] = $row;
        }

        $insertRow = [];
        foreach ($dataSesion as $row) {
            $oldId = $row['id'];
            unset($row['id']);
            $newId = DB::connection('pro')->table('plan_session')->insertGetId($row);
            if (array_key_exists($oldId, $dictRow)) {
                foreach ($dictRow[$oldId] as $r) {
                    $r['ses_cs_id'] = $newId;
                    $insertRow[] = $r;
                }
            }
        }
        DB::connection('pro')->table('plan_row')->insert($insertRow);
        return $insertRow;
    }

    public function syncProCSYT(Request $request)
    {
        $ma_cskcb = $request->ma_cskcb;


        $dataImport = DB::table('plan_import')->where('ma_cskcb', $ma_cskcb)
            ->get();
        $dataImport = json_decode(json_encode($dataImport), true);
        foreach ($dataImport as $idx => $row) {
            unset($dataImport[$idx]['id']);
        }
        DB::connection('pro')->table('plan_import')->insert($dataImport);

        $dataSesion = DB::table('plan_session')->where('ma_cskcb', $ma_cskcb)
            ->whereIn('trang_thai', [0, 2])
            ->where('session_level', 1)
            ->get();
        $dataSesion = json_decode(json_encode($dataSesion), true);

        $dataRow = DB::table('plan_row')->where('ma_cskcb', $ma_cskcb)->get();
        $dataRow = json_decode(json_encode($dataRow), true);

        $dictRow = array();
        foreach ($dataRow as $row) {
            if (!array_key_exists($row['ses_cs_id'], $dictRow)) $dictRow[$row['ses_cs_id']] = [];
            unset($row['id']);
            $dictRow[$row['ses_cs_id']][] = $row;
        }

        $insertRow = [];
        foreach ($dataSesion as $row) {
            $oldId = $row['id'];
            unset($row['id']);
            $newId = DB::connection('pro')->table('plan_session')->insertGetId($row);
            if (array_key_exists($oldId, $dictRow)) {
                foreach ($dictRow[$oldId] as $r) {
                    $r['ses_cs_id'] = $newId;
                    $insertRow[] = $r;
                }
            }
        }
        DB::connection('pro')->table('plan_row')->insert($insertRow);
        return $insertRow;
    }

    public function csytSessionTw(Request $request)
    {
        $res = array();
        $filter = $request->all();
        $query = [];
        $query_cs = [];
        $res['tinh'] = DB::table('provinces')->get();
        $dictTinh = array();
        foreach ($res['tinh'] as $row) {
            $dictTinh[$row->ma_tinh] = $row->name;
        }
        $res['dictTinh'] = $dictTinh;
        $res['ma_tinh'] = '';
        if (array_key_exists('ma_tinh', $filter)) {
            $query[] = ['plan.ma_tinh', $filter['ma_tinh']];
            $query_cs[] = ['ma_tinh', $filter['ma_tinh']];
            $res['ma_tinh'] = $filter['ma_tinh'];
        }
        $planId = 1;
        $selectSession = DB::table('plan_session as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->where($query)
            ->where('plan.plan_id', $planId)
            ->where('plan.session_level', 1)
            ->whereNotIn('plan.trang_thai', [-3, -2, -1])
            ->orderBy('plan.created_at', 'desc')
            ->select('plan.*', 'site.name as ten_cskcb')
            ->get();
        $res['session'] = $selectSession;

        $lstCoSo = DB::table('category_site')->where($query_cs)->orderBy('ma_cskcb', 'asc')->get();
        $selectCoSoSend = DB::table('plan_session')
            ->where($query_cs)
            ->where('plan_id', $planId)
            ->where('trang_thai', '>=', 0)
            ->select(DB::raw("DISTINCT(ma_cskcb)"))->pluck('ma_cskcb')->toArray();
        $res['lstCoSo'] = $lstCoSo;
        $res['csGuiKH'] = $selectCoSoSend;

        return view('center.csyt.session_tw', $res);
    }

    public function rowSessionCSYT(Request $request)
    {
        $planId = 1;
        $plan = DB::table('plan')->where('id', $planId)->first();
        $sesId = $request->id;
        $session = DB::table('plan_session as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->where('plan.id', $sesId)
            ->select('plan.*', 'site.name as ten_cskcb', 'site.*')->first();

        $selectData = $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('ses_cs_id', $sesId)
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt')
            ->get();
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $dictTinh = Utils::dictTinh();
        $dictHuyen = Utils::dictHuyen();
        $tinhTP = array_key_exists($session->ma_tinh, $dictTinh) ? $dictTinh[$session->ma_tinh] : '';
        $quanHuyen = array_key_exists($session->district_id, $dictHuyen) ? $dictHuyen[$session->district_id] : '';
        $html = '';
        $html .= '<div class="tab-content">';
        $html .= '<div id="home" class="tab-pane fade in active show">';
        $html .= '<div>Số kế hoạch: <strong>' . $session->cong_van . '</strong></div>';
        $html .= '<div>Mã CSKCB: <strong>' . $session->ma_cskcb . '</strong></div>';
        $html .= '<div>Tên CSKCB: <strong>' . $session->ten_cskcb . '</strong></div>';
        $html .= '<table class="table table-bordered table-striped">';
        $html .= '<thead>';
        $html .= '<tr>
                        <th rowspan="3">Mã thuốc</th>
                        <th rowspan="3">Tên hoạt chất</th>
                        <th rowspan="3">Nồng độ, hàm lượng</th>
                        <th rowspan="3">Dạng bào chế</th>
                        <th rowspan="3">Đường dùng</th>
                        <th rowspan="3">Nhóm thuốc</th>
                        <th rowspan="3">Đơn vị tính</th>
                        <th rowspan="3"  style="max-width: 150px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo thống kê của
                            CTCLQG
                        </th>
                        <th colspan="8">Số lượng dự trù (24 tháng)</th>
                        <th rowspan="3">Tổng số lượng</th>
                    </tr>';
        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th colspan="4">' . $year . '</th>';
        }
        $html .= '</tr>';

        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th>Quý I</th>
                            <th>Quý II</th>
                            <th>Quý III</th>
                            <th>Quý IV</th>';
        }
        $html .= '</tr>';
        $html .= '</thead>';

        $html .= '<tbody>';
        foreach ($data as $row) {
            $html .= '<tr>
                            <td>' . $row['ma_thuoc'] . '</td>
                            <td>' . $row['ten_hoat_chat'] . '</td>
                            <td>' . $row['ham_luong'] . '</td>
                            <td>' . $row['dang_bao_che'] . '</td>
                            <td>' . $row['duong_dung'] . '</td>
                            <td>' . $row['nhom_thuoc'] . '</td>
                            <td>' . $row['dvt'] . '</td>
                            <td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('sudung', $row)) . '</td>';
            for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
                for ($q = 1; $q <= 4; $q++) {
                    $html .= '<td class="text-right" >' . Utils::formatNumber(\App\Utils::keyFromArr($year . $q, $row)) . '</td >';
                }
            }
            $html .= '<td class="text-right">' . Utils::formatNumber(\App\Utils::keyFromArr('tong_so', $row)) . '</td>';
            $html .= '</tr>';
        }
        $html .= '<tr>';
        $html .= '<td colspan="17"><strong>Tổng ' . count($data) . ' khoản</strong></td>';

        $html .= '</tr>';

        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= '<div id="menu1" class="tab-pane fade">';
        $html .= '<div class="card m-b-30">';
        $html .= '<div class="card-body row view-info">';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="form-group">';
        $html .= '<span>Mã CSKCB : </span> <label for="control-label" style="color: #0b0b0b;font-weight: 600">' . $session->ma_cskcb . '</label>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="form-group">';
        $html .= '<span>Tên : </span><label for="control-label" style="color: #0b0b0b;font-weight: 600">' . $session->name . '</label>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="form-group">';
        $html .= '<span>Địa chỉ : </span><label for="control-label" style="color: #0b0b0b;font-weight: 600">' . $session->address . '</label>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="form-group">';
        $html .= '<span>Cán bộ đầu mối  : </span><label for="control-label" style="color: #0b0b0b;font-weight: 600">' . $session->can_bo_dau_moi . '</label>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="form-group">';
        $html .= '<span>Số điện thoại  : </span><label for="control-label" style="color: #0b0b0b;font-weight: 600">' . $session->dienthoai . '</label>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="form-group">';
        $html .= '<span>Email  : </span><label for="control-label" style="color: #0b0b0b;font-weight: 600">' . $session->email . '</label>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="form-group">';
        $html .= '<span>Tỉnh/TP : </span><label for="control-label" style="color: #0b0b0b;font-weight: 600">' . $tinhTP . '</label>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="form-group">';
        $html .= '<span>Quận/Huyện : </span><label for="control-label" style="color: #0b0b0b;font-weight: 600">' . $quanHuyen . '</label>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function sessionTW(Request $request)
    {
        $planId = 1;
        $selectSession = DB::table('plan_session')
            ->where('plan_id', $planId)
            ->where('session_level', 2)
            ->whereIn('trang_thai', [1, 3, -2])
            ->orderBy('trang_thai', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();
        $res = array();
        $res['session'] = $selectSession;
        $res['dictTinh'] = Utils::dictTinh();
        $res['dictUser'] = Utils::dictUserFullName();

        $selectMaTinh = DB::table('plan_session')
            ->where('plan_id', $planId)
            ->where('session_level', 2)
            ->whereIn('trang_thai', [1, 3])
            ->pluck('ma_tinh')->toArray();
        $userTinh = DB::table('users')->where('group_scope', 2)
            ->select('ma_tinh', 'dienthoai', 'email', 'can_bo_dau_moi')
            ->get();
        $res['maTinhSent'] = $selectMaTinh;
        $res['userTinh'] = $userTinh;

        return view('center.prov.session_sent', $res);
    }

    public function rowSessionTW(Request $request)
    {
        $sesId = $request->id;
        $res = array();
        $res['m03'] = GenView::viewSessionM03($sesId);
        $res['m02'] = GenView::viewSessionM02($sesId);
        $res['m04'] = GenView::viewSessionM04($sesId);
        return $res;
    }

    public function xuLyKH(Request $request)
    {
        $sesId = $request->id;
        $note = $request->note;
        $status = $request->trang_thai == "1" ? 1 : -2;
        $user = Session::get('uinfo');
        DB::table('plan_session')->where('id', $sesId)->update([
            'trang_thai' => $status,
            'ghi_chu' => $note,
            'user_xacnhan_kehoach' => $user->id,
            'updated_at_xacnhan_kehoach' => date("Y-m-d H:i:s")
        ]);
        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'Xử lý thành công!');
        return redirect(route('center-prov-session-tw'));
    }

    public function fixBug()
    {
        $selectSession = DB::table('plan_session')->where('session_level', 2)->get();
        $data = array();
        foreach ($selectSession as $row) {
            $fileAttach = json_decode($row->file_attach, true);
            $data[] = $fileAttach;
            if (array_key_exists('mau_04', $fileAttach)) {
                $fileAttach['mau_04']['title'] = 'Thông tin hành chính các cơ sở y tế (Mẫu 04)';
                DB::table('plan_session')->where('id', $row->id)->update(['file_attach' => json_encode($fileAttach)]);
            }
        }
        return $data;
    }

    public function exportSession02(Request $request)
    {
        $sesId = $request->id;
        $session = DB::table('plan_session')->where('id', $sesId)->first();
        $tinh = DB::table('provinces')->where('ma_tinh', $session->ma_tinh)->first();

        $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('plan.ses_tinh_id', $sesId)
            ->groupBy('plan.ma_thuoc', 'plan.quy', 'med.name', 'med.cell_type',
                'med.concentration', 'med.type', 'med.group_tckt', 'med.unit')
            ->select('plan.ma_thuoc', 'plan.quy', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt',
                DB::raw('SUM(plan.so_luong) as so_luong'))
            ->get();
        $res['data'] = $selectData;
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_thuoc, $data)) {
                $data[$row->ma_thuoc] = [
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);

        $fileName = base_path() . "/template/template_01.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 9;
        $sheet->setCellValue('A1', $tinh->name);
        $sheet->setCellValue('P1', "Mẫu 02");
        foreach ($data as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
            $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
            $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
            $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
            $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
            $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
            $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
            $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
            $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
            $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
            $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
            $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
            $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
        }

        $idxSum = $startRow + count($data);
        $sheet->setCellValue('A' . $idxSum, "Tổng " . count($data) . " khoản");

        $sheet->mergeCells("A" . $idxSum . ":R" . $idxSum);
        $sheet->getStyle("A" . $startRow . ":R" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle("A" . $idxSum . ":R" . $idxSum)->applyFromArray($styleArray);

        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $tinh->name . ' - Báo cáo kế hoạch dự trù mẫu 02_' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }

    public function exportSession03(Request $request)
    {
        $sesId = $request->id;
        $session = DB::table('plan_session')->where('id', $sesId)->first();
        $tinh = DB::table('provinces')->where('ma_tinh', $session->ma_tinh)->first();
        $selectData = $selectData = DB::table('plan_row as plan')
            ->leftJoin('category_site as site', 'site.ma_cskcb', '=', 'plan.ma_cskcb')
            ->leftJoin('category_medicine as med', 'med.code', '=', 'plan.ma_thuoc')
            ->where('ses_tinh_id', $sesId)
            ->select('plan.*', 'med.name as ten_hoat_chat', 'med.cell_type as dang_bao_che',
                'med.concentration as ham_luong', 'med.type as duong_dung', 'med.group_tckt as nhom_thuoc', 'med.unit as dvt', 'site.name as ten_cskcb')
            ->get();
        $data = array();
        $tong = ['tong_so' => 0];
        foreach ($selectData as $row) {
            if (!array_key_exists($row->ma_cskcb . $row->ma_thuoc, $data)) {
                $data[$row->ma_cskcb . $row->ma_thuoc] = [
                    'ma_cskcb' => $row->ma_cskcb,
                    'ten_cskcb' => $row->ten_cskcb,
                    'quy' => $row->quy,
                    'ma_thuoc' => $row->ma_thuoc,
                    'ten_hoat_chat' => $row->ten_hoat_chat,
                    'dang_bao_che' => $row->dang_bao_che,
                    'ham_luong' => $row->ham_luong,
                    'duong_dung' => $row->duong_dung,
                    'nhom_thuoc' => $row->nhom_thuoc,
                    'dvt' => $row->dvt,
                    'tong_so' => 0
                ];
            }
            if (!array_key_exists($row->quy, $tong)) $tong[$row->quy] = 0;
            $tong[$row->quy] += intval($row->so_luong);

            $data[$row->ma_cskcb . $row->ma_thuoc][$row->quy] = $row->so_luong;
            if ($row->quy != 'sudung') {
                $data[$row->ma_cskcb . $row->ma_thuoc]['tong_so'] += intval($row->so_luong);
                $tong['tong_so'] += intval($row->so_luong);
            }
        }
        $data = array_values($data);
        usort($data, function ($item1, $item2) {
            return $item1['ma_cskcb'] > $item2['ma_cskcb'];
        });

        $fileName = base_path() . "/template/template_03.xlsx";
        $excelobj = \PHPExcel_IOFactory::load($fileName);
        $excelobj->setActiveSheetIndex(0);
        $excelobj->getActiveSheet()->toArray(null, true, true, true);
        $sheet = $excelobj->getActiveSheet();
        $startRow = 9;
        $sheet->setCellValue('A1', $tinh->name);
        foreach ($data as $idx => $row) {
            $sheet->setCellValue('A' . ($startRow + $idx), ($idx + 1));
            $sheet->setCellValue('B' . ($startRow + $idx), $row['ma_thuoc']);
            $sheet->setCellValue('C' . ($startRow + $idx), $row['ten_hoat_chat']);
            $sheet->setCellValue('D' . ($startRow + $idx), $row['ham_luong']);
            $sheet->setCellValue('E' . ($startRow + $idx), $row['dang_bao_che']);
            $sheet->setCellValue('F' . ($startRow + $idx), $row['duong_dung']);
            $sheet->setCellValue('G' . ($startRow + $idx), $row['nhom_thuoc']);
            $sheet->setCellValue('H' . ($startRow + $idx), $row['dvt']);
            $sheet->setCellValue('I' . ($startRow + $idx), $row['sudung']);
            $sheet->setCellValue('J' . ($startRow + $idx), $row['20221']);
            $sheet->setCellValue('K' . ($startRow + $idx), $row['20222']);
            $sheet->setCellValue('L' . ($startRow + $idx), $row['20223']);
            $sheet->setCellValue('M' . ($startRow + $idx), $row['20224']);
            $sheet->setCellValue('N' . ($startRow + $idx), $row['20231']);
            $sheet->setCellValue('O' . ($startRow + $idx), $row['20232']);
            $sheet->setCellValue('P' . ($startRow + $idx), $row['20233']);
            $sheet->setCellValue('Q' . ($startRow + $idx), $row['20234']);
            $sheet->setCellValue('R' . ($startRow + $idx), $row['tong_so']);
            $sheet->setCellValue('S' . ($startRow + $idx), $row['ten_cskcb']);
        }

        $idxSum = $startRow + count($data);
        $sheet->setCellValue('A' . $idxSum, "Tổng " . count($data) . " khoản");

        $sheet->mergeCells("A" . $idxSum . ":S" . $idxSum);
        $sheet->getStyle("A" . $startRow . ":S" . $idxSum)->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'vertival' => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle("A" . $idxSum . ":S" . $idxSum)->applyFromArray($styleArray);

        $objWriter = \PHPExcel_IOFactory::createWriter($excelobj, "Excel2007");
        header('Content-Description: File Transfer');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $tinh->name . ' - Báo cáo kế hoạch dự trù mẫu 03_' . date('Ymd') . '.xlsx"');
        $objWriter->save('php://output');
    }

}