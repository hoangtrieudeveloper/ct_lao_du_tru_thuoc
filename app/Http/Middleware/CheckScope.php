<?php

namespace App\Http\Middleware;

use App\Scopes;
use Closure;
use Illuminate\Http\Response;

class CheckScope
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        return $next($request);
        $except = [
            "index",
            "update-view-info-tinh"
        ];
        $route = $request->route()->getName();
        $check = Scopes::checkAccess($route, true);
        if (in_array($route, $except)) {
            return $next($request);
        }
        if ($check){
            return $next($request);
        }
        return new Response(view('errscope'));
    }
}
