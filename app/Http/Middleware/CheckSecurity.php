<?php

namespace App\Http\Middleware;

use App\Scopes;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CheckSecurity
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if (!$request->session()->exists('uinfo')) {
            return redirect()->route('security-login');
        }
        if (in_array($request->route()->getName(), ['security-logout', 'change-password', 'update-view-info'])) {
            return $next($request);
        }
        $user = $request->session()->get('uinfo');
        if (isset($user->change_pass) && $user->change_pass != 1) {
            return redirect()->route('change-password');
        }
        if (($user->group_scope == 3 || $user->group_scope == 2) && isset($user->update_cs_info) && $user->update_cs_info != 1) {
             if($user->group_scope == 3) return redirect()->route('update-view-info');
             else redirect()->route('update-view-info-tinh');
        }
        return $next($request);
    }
}
