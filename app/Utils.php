<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Utils extends Model
{
    public static function post_file_key($key, $id = null)
    {
        if (!isset($_FILES["$key"])) return null;
        $target_dir = base_path('files_faqs');
        $folder = "/" . $id . "/";
        $listallow = [
            "jpg",
            "jpeg",
            "png",
            "gif",
            "mp3",
            "mp4",
            "doc",
            "docx",
            "xlsx",
            'pdf'
        ];
        $fileParts = strtolower(pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION));
        $folder_name = '/general/';
        if (in_array($fileParts, [
            'jpg',
            'jpeg',
            'gif',
            'png'
        ])) {
            $folder_name = 'picture/';
        }
        if (in_array($fileParts, [
            'mp3',
            'mp4',
            'avi',
            'mkv'
        ])) {
            $folder_name = 'video/';
        }
        if (in_array($fileParts, [
            'doc',
            'docx',
            'xlsx',
            'pdf'
        ])) {
            $folder_name = 'file/';
        }
        if (in_array($fileParts, ['srt'])) {
            $folder_name = 'sub/';
        }
//        if (!file_exists($target_dir . $folder . $folder_name)) mkdir($target_dir . $folder . $folder_name, 0777, true);
        $target_file = $folder . $folder_name . Utils::removeTitle($_FILES["$key"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        if (file_exists($target_file)) return null;
        if ($_FILES["$key"]["size"] <= 0) return null;
        if (!in_array($imageFileType, $listallow)) return null;
        move_uploaded_file('\tmp\php1VTXFg', $target_dir . $target_file);
//        $url_file = $target_dir . $target_file;
//        chmod($url_file, 0777);
        return $target_file;
    }

    public static function removeTitleCategories($string, $keyReplace = '-')
    {
        $string = Utils::RemoveSign($string);
        //neu muon de co dau
        $string = trim(preg_replace("/[^A-Za-z0-9.]/i", " ", $string)); // khong dau
        $string = str_replace(" ", "", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace($keyReplace, "-", $string);
        return $string;
    }


    public static function removeTitle($string, $keyReplace = '-')
    {
        $string = Utils::RemoveSign($string);
        //neu muon de co dau
        $string = trim(preg_replace("/[^A-Za-z0-9.]/i", " ", $string)); // khong dau
        $string = str_replace(" ", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace("--", "-", $string);
        $string = str_replace($keyReplace, "-", $string);
        $string = strtolower($string);
        return $string;
    }

    public static function RemoveSign($str)
    {
        $coDau = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ", "ê", "ù", "à");

        $khongDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D", "e", "u", "a");
        return str_replace($coDau, $khongDau, $str);
    }


    public static function vn_to_str($str)
    {

        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);

        }

        return strtolower($str);

    }

    public static function formatDigit($digit)
    {
        return $digit >= 10 ? $digit : '0' . $digit;
    }

    public static function getProvince()
    {
        return DB::table('provinces')->get();
    }

    public static function getDistrict()
    {
        return DB::table('districts')->get();
    }

    public static function formatNumber($priceFloat)
    {
        if (is_null($priceFloat) || $priceFloat == '') return "";
        $text = "" . $priceFloat;
        $plit = explode(',', $text);
        $symbol = count($plit) == 1 ? '' : ',' . $text[1];
        $symbol_thousand = '.';
        $decimal_place = 0;
        $price = number_format($priceFloat, $decimal_place, ',', $symbol_thousand);
        return $price . $symbol;
    }

    public static function formatNumberNone($priceFloat)
    {
        return $priceFloat;
    }

    public static function keyFromArr($key, $arr)
    {
        return array_key_exists($key, $arr) ? $arr[$key] : '';
    }

    public static function masterTrim($str)
    {
        $str = trim($str);
        $str = str_replace(PHP_EOL, ' ', $str);
        return $str;
    }

    public static function equalNoSpace($str1, $str2)
    {
        $str1 = str_replace([PHP_EOL, ' '], '', trim($str1));
        $str2 = str_replace([PHP_EOL, ' '], '', trim($str2));
        return strtolower($str1) == strtolower($str2);
    }

    public static function genTheadM03($plan)
    {
        $html = '<thead>
                        <tr>
                            <th rowspan="3">STT</th>

                            <th rowspan="3">Mã thuốc</th>
                            <th rowspan="3">Tên hoạt chất</th>
                            <th rowspan="3">Nồng độ, hàm lượng</th>
                            <th rowspan="3">Dạng bào chế</th>
                            <th rowspan="3">Đường dùng</th>
                            <th rowspan="3">Nhóm thuốc</th>
                            <th rowspan="3">Đơn vị tính</th>
                            <th rowspan="3" style="max-width: 100px">Số lượng sử dụng từ 01/01/2019 đến 31/12/2020 theo
                                thống kê của
                                CTCLQG
                            </th>
                            <th colspan="' . (($plan->year_to + 1 - $plan->year_from) * 4) . '">Số lượng dự trù (24 tháng)
                            </th>
                            <th rowspan="3">Tổng số lượng</th>
                            <th rowspan="3" style="width: 150px;">Cơ sở y tế</th>
                        </tr>
                        <tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th colspan="4">' . $year . '</th>';
        }

        $html .= '</tr>';
        $html .= '<tr>';
        for ($year = $plan->year_from; $year <= $plan->year_to; $year++) {
            $html .= '<th>Quý I</th>
                                <th>Quý II</th>
                                <th>Quý III</th>
                                <th>Quý IV</th>';
        }

        $html .= '</tr>
                        </thead>';
        return $html;
    }

    public static function dictTinh(){
        $selectTinh = DB::table('provinces')->get();
        $dictTinh = array();
        foreach ($selectTinh as $row){
            $dictTinh[$row->ma_tinh]=$row->name;
        }
        return $dictTinh;
    }
    public static function dictHuyen(){
        $selectHuyen = DB::table('districts')->get();
        $dictHuyen = array();
        foreach ($selectHuyen as $row){
            $dictHuyen[$row->id]=$row->name;
        }
        return $dictHuyen;
    }

    public static function dictUserFullName(){
        $selectUser = DB::table('users')->get();
        $dictUser = array();
        foreach ($selectUser as $row){
            $dictUser[$row->id]=$row->fullname;
        }
        return $dictUser;
    }

}
