<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static function deleteUser($row_code)
    {
        DB::table('users')->where('id', $row_code)->delete();
    }

    public static function updateUser($data)
    {
        $id = $data['id'];
        $data['updated_at'] = date("Y-m-d H:i:s");
        unset($data['id']);
        return self::where('id', $id)->update($data);
    }


    public static function creteUser($data)
    {
        if($data['ma_tinh']) {
            $ma_tinh = DB::table('provinces')->where('id',$data['ma_tinh'])->first();
            $data['ma_tinh'] = $ma_tinh->ma_tinh;
            $data['dbhc_tinh'] = $ma_tinh->ma_tinh;
        }
        if($data['group'] == 2) $data['ma_cskcb'] = '';
        $create = [
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'email' => $data['email'],
            'fullname' => $data['fullname'],
            'dept_id' => $data['dept'],
            'created_at' => date("Y-m-d H:i:s"),
//            'remember_token' => $data['_token'],
            'group_scope' => $data['group'],
            'ma_tinh' => $data['ma_tinh'],
            'ma_cskcb' => $data['ma_cskcb'],
            'gender' => $data['gender'],
            'status' => $data['status'],
            'dbhc_tinh' => $data['dbhc_tinh']
        ];
        return self::insertGetId($create);
    }

    public  static function dict(){
        $select = self::all();
        $data = array();
        foreach ($select as $row){
            $data[$row->id] = $row->fullname;
        }
        return $data;
    }
}
