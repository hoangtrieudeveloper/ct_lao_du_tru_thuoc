<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class categorymedicine extends Authenticatable
{
    protected $table = 'category_medicine';
    use Notifiable;
    public static function deleteCate($row_code)
    {
        DB::table('category_medicine')->where('id', $row_code)->delete();
    }

    public static function createdMedicine($data)
    {
        $data['created_at'] = date("Y/m/d H:i:s");
        return self::insertGetId($data);
    }
    public static function updatedMedicine($data)
    {
        $id = $data['id'];
        $data['updated_at'] = date("Y/m/d H:i:s");
        unset($data['id']);
        return self::where('id', $id)->update($data);
    }
}
