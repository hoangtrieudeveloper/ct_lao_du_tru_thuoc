<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class UserDEPT extends Authenticatable
{
    protected $table = 'user_dept';
    use Notifiable;

    public static function deleteUserDept($row_code)
    {
        DB::table('user_dept')->where('id', $row_code)->delete();
    }

    public static function updateUserDept($data)
    {
        $update = [
          "dept_name" => $data['name']
        ];
        return DB::table('user_dept')->where('id', $data['id'])->update($update);
    }


    public static function createUserDept($data)
    {
        $create = [
            'dept_name' => $data['name'],
        ];
        return self::insertGetId($create);
    }

}
