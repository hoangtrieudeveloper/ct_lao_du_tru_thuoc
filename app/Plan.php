<?php


namespace App;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Plan
{
    public static function updatePlanImport($row, $quy, $planId = 1)
    {
        if (array_key_exists($quy, $row)) {
            $user = Session::get('uinfo');
            if (array_key_exists($quy, $row)) {
                $query = [
                    'plan_id' => $planId,
                    'ma_cskcb' => $user->ma_cskcb,
                    'ma_tinh' => $user->ma_tinh,
                    'quy' => $quy,
                    'ma_thuoc' => $row['ma_thuoc']
                ];
                $data = [
                    'so_luong' => $row[$quy],
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $user->id
                ];
                DB::table('plan_import')->updateOrInsert($query, $data);
            }
        }
    }

    public static function getSoCV($code)
    {
        $select = DB::table('cv_seq')->where('code', $code)->first();
        if (is_null($select)) {
            $seq = 1;
        } else {
            $seq = $select->_sequence;
        }
        $str = $seq;
        if ($seq < 10) $str = '0' . $str;
        if ($seq < 100) $str = '0' . $str;
        if ($seq < 100) $str = '0' . $str;
        return $code . $str;
    }

    public static function updateSoCV($code)
    {
        $select = DB::table('cv_seq')->where('code', $code)->first();
        if (is_null($select)) {
            $seq = 2;
        } else {
            $seq = $select->_sequence + 1;
        }
        DB::table('cv_seq')->updateOrInsert(
            ['code' => $code],
            ['_sequence' => $seq]
        );
    }

    public static function validate($temp)
    {
        $output = array();
        $invalid = array();
        $warning = array();
        $checkMaThuoc = DB::table('category_medicine')->where('code', $temp['ma_thuoc'])->first();
        if (is_null($checkMaThuoc)) {
            $invalid['ma_thuoc'] = ['Mã thuốc không tồn tại trong danh mục'];
        } else {
            if (!Utils::equalNoSpace($checkMaThuoc->name, $temp['ten_hoat_chat'])) $invalid['ten_hoat_chat'] = ['Tên hoạt chất không khớp với mã thuốc'];
            if (!Utils::equalNoSpace($checkMaThuoc->group_tckt, $temp['nhom_thuoc'])) $invalid['nhom_thuoc'] = ['Nhóm thuốc không khớp với mã thuốc'];
            if (!Utils::equalNoSpace($checkMaThuoc->concentration, $temp['ham_luong'])) $invalid['ham_luong'] = ['Nồng độ, hàm lượng không khớp với mã thuốc'];
            if (!Utils::equalNoSpace($checkMaThuoc->cell_type, $temp['dang_bao_che'])) $invalid['dang_bao_che'] = ['Dạng bào chế không khớp với mã thuốc'];
            if (!Utils::equalNoSpace($checkMaThuoc->type, $temp['duong_dung'])) $invalid['duong_dung'] = ['Đường dùng không khớp với mã thuốc'];
            if (!Utils::equalNoSpace($checkMaThuoc->unit, $temp['dvt'])) $invalid['dvt'] = ['Đơn vị tính không khớp với mã thuốc'];
            if(array_key_exists('ten_hoat_chat', $invalid) && array_key_exists('nhom_thuoc', $invalid)
                && array_key_exists('ham_luong', $invalid)&& array_key_exists('dang_bao_che', $invalid)
                && array_key_exists('duong_dung', $invalid)&& array_key_exists('dvt', $invalid)){
                $invalid['ma_thuoc'] = ['Mã thuốc không khớp với thông tin thuốc'];
            }
        }
        if (!is_numeric($temp['sudung'])) {
            $invalid['sudung'] = ['Cột sử dụng không đúng định dạng số'];
        } else {
            $temp['sudung'] = intval($temp['sudung']);
            if(intval($temp['sudung']) < 0) $invalid['sudung'] = ['Cột sử dụng không được nhỏ hơn 0'];
        }
        if (!is_numeric($temp['tongso'])) {
            $invalid['tongso'] = ['Cột tổng số không đúng định dạng số'];
        } else {
            $temp['tongso'] = intval($temp['tongso']);
            if(intval($temp['tongso']) < 0) $invalid['tongso'] = ['Cột tổng số không được nhỏ hơn 0'];
        }
        $tongTuCong = 0;
        for ($y = 2022; $y <= 2023; $y++) {
            for ($q = 1; $q <= 4; $q++) {
                $quy = $y . $q;
                if (!is_numeric($temp[$quy])) {
                    $invalid[$quy] = ['Cột quý ' . $q . ' năm ' . $y . 'không đúng định dạng số'];
                } else {
                    $temp[$quy] = intval($temp[$quy]);
                    if(intval($temp[$quy]) < 0) $invalid[$quy] = ['Cột quý ' . $q . ' năm ' . $y . ' không được nhỏ hơn 0'];
                    $tongTuCong += $temp[$quy];
                }
            }
        }
        if($tongTuCong == 0 && $temp['sudung'] == 0){
            $invalid['tongso'] = ["Chưa nhập số liệu"];
            $invalid['sudung'] = ["Chưa nhập số liệu"];
//            unset($invalid['sudung']);
//            for ($y = 2022; $y <= 2023; $y++) {
//                for ($q = 1; $q <= 4; $q++) {
//                    $quy = $y . $q;
//                    unset($invalid[$quy]);
//                }
//            }
        } else if ($tongTuCong != $temp['tongso']) {
            $mess = 'Tổng số không bằng tổng các quý cộng lại';
            if (array('tongso', $invalid)) {
                $invalid['tongso'][] = $mess;
            } else {
                $invalid['tongso'] = [$mess];
            }
        }
        if (!array_key_exists('sudung', $invalid) && !array_key_exists('tongso', $invalid)) {
            if ($temp['tongso'] > $temp['sudung'] * 1.3) {
                $warning[] = 'Tổng kế hoạch dự trù cao hơn 130% so với số sử dụng 2019-2020. Lưu ý gửi kèm công văn giải trình khi gửi kế hoạch lên Tỉnh';
            } else if ($temp['tongso'] < $temp['sudung'] * .7) {
                $warning[] = 'Tổng kế hoạch dự trù thấp hơn 70% so với số sử dụng 2019-2020. Lưu ý gửi kèm công văn giải trình khi gửi kế hoạch lên Tỉnh';
            }

        }

        $temp['invalid'] = $invalid;
        $temp['warning'] = $warning;
        return $temp;
    }

    public static function rowInport($row, $idx)
    {
        $html = '';
        $html .= '<td>';
        if (count($row['invalid']) > 0) {
            $html .= '<a class="dot-error text-danger text-lg-center" data-toggle="tooltip" data-html="true" title="';
            foreach ($row['invalid'] as $key => $arr) {
                foreach ($arr as $e) {
                    $html .= '<p>' . $e . '</p>';
                }
            }
            $html .= '"><i class="fa fa-circle"></i></a>';
            $html .= '<input type="hidden" name="status[]" value="invalid"/>';
        } elseif (count($row['warning']) > 0) {
            $html .= '<a class="text-warning text-lg-center" data-toggle="tooltip" data-html="true" title="';
            foreach ($row['warning'] as $e) {
                $html .= '<p>' . $e . '</p>';
            }
            $html .= '"><i class="fa fa-circle"></i></a>';
            $html .= '<input type="hidden" name="status[]" value="warning"/>';
        } else {
            $html .= '<a class="text-success text-lg-center"><i class="fa fa-circle"></i></a>';
            $html .= '<input type="hidden" name="status[]" value="success"/>';
        }
        $html .= '</td>';

        $html .= '<td class="text-center td-stt">' . ($row['stt']) . '</td>
<td class="' . (array_key_exists('ma_thuoc', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="ma_thuoc[]" value="' . ($row['ma_thuoc']) . '" readonly></td>
<td class="' . (array_key_exists('ten_hoat_chat', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="ten_hoat_chat[]" value="' . ($row['ten_hoat_chat']) . '" readonly></td>
<td class="' . (array_key_exists('ham_luong', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="ham_luong[]" value="' . ($row['ham_luong']) . '" readonly></td>
<td class="' . (array_key_exists('dang_bao_che', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="dang_bao_che[]" value="' . ($row['dang_bao_che']) . '" readonly></td>
<td class="' . (array_key_exists('duong_dung', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="duong_dung[]" value="' . ($row['duong_dung']) . '" readonly></td>
<td class="' . (array_key_exists('nhom_thuoc', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="nhom_thuoc[]" value="' . ($row['nhom_thuoc']) . '" readonly></td>
<td class="' . (array_key_exists('dvt', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="dvt[]" value="' . ($row['dvt']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('sudung', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="sudung[]" value="' . ($row['sudung']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('20221', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="20221[]" value="' . ($row['20221']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('20222', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="20222[]" value="' . ($row['20222']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('20223', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="20223[]" value="' . ($row['20223']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('20224', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="20224[]" value="' . ($row['20224']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('20231', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="20231[]" value="' . ($row['20231']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('20232', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="20232[]" value="' . ($row['20232']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('20233', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="20233[]" value="' . ($row['20233']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('20234', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="20234[]" value="' . ($row['20234']) . '" readonly></td>
<td class="text-right ' . (array_key_exists('tongso', $row['invalid']) ? 'text-danger' : '') . '">
    <input name="tongso[]" value="' . ($row['tongso']) . '" readonly></td>
<td class="text-center">
    <a class="text-primary table-action" href="javascript:enableEdit(' . ($idx) . ')"><i
            class="fa fa-edit"></i></a>
    <a class="text-danger table-action" href="javascript:removeRow(' . ($idx) . ')"><i
            class="fa fa-remove"></i></a>        
</td>';
        return $html;
    }
}