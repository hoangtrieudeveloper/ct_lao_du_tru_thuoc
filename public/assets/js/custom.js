function modalMessage(type, message) {
    $('.modal-title-message').html(message);
    $('.modal-title-message').addClass("text-" + type);
    $("#modal_message").modal("show");
    setTimeout(function () {
        $("#modal_message").modal("hide");
    }, 2000);
}

function checkQuanHuyen() {
    var tinhtp = $('#tinhtp').val();
    $('.custom-select.district option').css('display', 'none');
    $('.quanhuyen_' + tinhtp).css('display', 'block');
}
setTimeout(function (){
    checkQuanHuyen();
},1000)
