$(document).ready(function () {
    initSelect2Data();
    initDatatable();
    $("input[type='file']").change(function () {
        console.log(this.files[0].size);
        if (this.files[0].size >= 10485760 * 5) {
            modalAlert("Tệp tin đẩy lên không được quá 10MB");
            this.value = "";
        }
    })

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        console.log($.fn.dataTable.tables({visible: true, api: true}))
        $.fn.dataTable.tables({visible: true, api: true}).columns.adjust().draw();
        ;
    });

    $('select[name="ma_tinh"]').on('change', function () {
        initSelectCSKCB($(this).val())
    });
});

function initSelect2Data() {
    $('.select-filter-data').each(function () {
        var placeholder = "--- Chọn ---";
        if ($(this).data('pick') != undefined) {
            placeholder = "--- Chọn " + $(this).data('pick') + " ---";
        }
        var url = '/dm/search/' + $(this).data('table') + "/" + $(this).data('key') + "?label=" + $(this).data('label') + "&search=" + $(this).data('search');
        if ($(this).data('table') == 'category_site' && $(this).data('tinh') != '') {
            url += "&ma_tinh=" + $(this).data('tinh');
        }

        $(this).select2({
            allowClear: true,
            placeholder: {
                id: "",
                text: placeholder
            },
            ajax: {
                url: url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        key: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    });

}

function initSelectCSKCB(ma_tinh) {
    $('select[data-table="category_site"]').val(null).trigger("change");
    $('select[data-table="category_site"]').select2("destroy");
    var url = '/dm/search/category_site/' + $('select[data-table="category_site"]').data('key') + "?label="
        + $('select[data-table="category_site"]').data('label') + "&search="
        + $('select[data-table="category_site"]').data('search');
    url += "&ma_tinh=" + ma_tinh;
    console.log(url);
    var placeholder = "--- Chọn CSKCB ---";
    $('select[data-table="category_site"]').select2({
        allowClear: true,
        placeholder: {
            id: "",
            text: placeholder
        },
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    key: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
}

function initDatatable() {
    var table = $(".datatable").DataTable({
        "pageLength": 15,
        "language": {
            "url": "/datatable/Vietnamese.json"
        },
        "ordering": false
    });
    table.columns().every(function () {
        var that = this;
        $('input', this.header()).on('keyup change changeDate', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
        $('select', this.header()).on('change', function () {
            if (that.search() !== this.value) {
                that.search(this.value ? '^' + this.value + '$' : '', true, false).draw();
            }
        });
    });
    $("#table-user_length").parent().parent().remove();
}