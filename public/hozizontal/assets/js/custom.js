function modalMessage(type, message) {
    $('.modal-title-message').html(message);
    $('.modal-title-message').addClass("text-" + type);
    $("#modal_message").modal("show");
    setTimeout(function () {
        $("#modal_message").modal("hide");
    }, 2000);
}

function checkQuanHuyen(check) {
    var tinhtp = $('#tinhtp').val();
    $('.custom-select.district option').css('display', 'none');
    $('.quanhuyen_' + tinhtp).css('display', 'block');
    if(check == 1){
        $('.custom-select.district').val('');
    }
}
setTimeout(function (){
    checkQuanHuyen();
},1000)

function checkCskcb(check) {
    var tinhtp = $('#tinhtp_user').val();
    $('.custom-select.cskcb option').css('display', 'none');
    $('.cskcb_' + tinhtp).css('display', 'block');
    if(check == 1){
        $('.custom-select.cskcb').val('');
    }
}
setTimeout(function (){
    checkCskcb();
},1000)

function checkCskcb_update(check) {
    var tinhtp = $('#tinhtp_user_update').val();
    $('.custom-select.cskcb_update option').css('display', 'none');
    $('.update_cskcb_' + tinhtp).css('display', 'block');
    if(check == 1){
        $('.custom-select.cskcb_update').val('');
    }
}
setTimeout(function (){
    checkCskcb_update();
},1000)

function checkGroup(){
    var group = $('#group').val();
    if(group == 2) {
        $("#checkCSKCBGroup").prop("required", false);
        $('#checkCSKCBGroup').removeAttr('required');
        $('#ma_cskcb').css('display', 'none');
    } else {
        $("#checkCSKCBGroup").prop("required", true);
        $('#ma_cskcb').css('display', 'block');
    }
}
setTimeout(function (){
    checkGroup();
},2000)

function checkGroupUd(data){
    var group = '';
    if(data !==  ''){
        group = data;
    }else {
        group = $('#group_ud').val();
    }
    if(group == 2) {
        $('#checkCSKCBGroup_ud').removeAttr('required');
        $('#ma_cskcb_ud').css('display', 'none');
    } else {
        $("#checkCSKCBGroup_ud").prop("required", true);
        $('#ma_cskcb_ud').css('display', 'block');
    }
}


