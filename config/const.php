<?php
return [
    'session_status_label' => [
        -3 => 'Kế hoạch đã bị thay thế',
        0 => 'Đã gửi lên Tỉnh',
        -1 => 'Tỉnh từ chối',
        -2 => 'TW từ chối',
        1 => 'Đã được xác nhận',
        2 => 'Tỉnh đã xác nhận',
        3 => 'Tỉnh đã gửi TW'
    ],
    'session_status_color' => [
        -3 => 'text-secondary',
        0 => 'text-warning',
        -1 => 'text-danger',
        -2 => 'text-danger',
        1 => 'text-success',
        2 => 'text-primary',
        3 => 'text-primary'
    ]
];
