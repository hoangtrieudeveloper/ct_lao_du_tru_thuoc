<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/validateSite', 'DashboardController@validateSite');

Route::group(['prefix' => 'security'], function () {
    Route::any('/login', 'SecurityController@login')->name('security-login');

});
//Auth::routes();
//Route::group(['middleware' => 'auth'], function () {
Route::group(['middleware' => 'check_security'], function () {

    Route::group(['prefix' => 'security'], function () {
        Route::any('/logout', 'SecurityController@logout')->name('security-logout');
    });
    Route::group(['middleware' => 'check_scope'], function () {
        Route::group(['middleware' => 'check_scope'], function () {

            Route::any('/home', 'HomeController@index')->name('home');
            Route::get('/', 'DashboardController@index')->name('index');

            Route::group(['prefix' => 'user'], function () {
                Route::any('/', 'UserController@index')->name('user-index');
                Route::any('/adduser', 'UserController@createUser')->name('create-user');
                Route::any('/updateuser', 'UserController@updateUser')->name('update-user');
                Route::post('deleteuser', 'UserController@deleteUser')->name('delete-user');
                Route::get('/viewinfo', 'UserController@viewinfo')->name('view-info');
                Route::any('/updateviewinfo', 'UserController@updateinfo')->name('update-view-info');
                Route::any('/updateviewinfotinh', 'UserController@updateinfotinh')->name('update-view-info-tinh');
                Route::any('/changepassword', 'UserController@changePassword')->name('change-password');
            });
            Route::group(['prefix' => 'group-user'], function () {
                Route::get('/', 'GroupUserController@index')->name('group-user-index');
                Route::any('/create', 'GroupUserController@create')->name('group-user-create');
                Route::any('/update', 'GroupUserController@update')->name('group-user-update');
                Route::post('deletegroup', 'GroupUserController@deleteUserGroup')->name('delete-user-group');
            });
            Route::group(['prefix' => 'user-dept'], function () {
                Route::get('/', 'UserDeptController@index')->name('User-dept-index');
                Route::any('/create', 'UserDeptController@create')->name('User-dept-create');
                Route::get('/view', 'UserDeptController@viewinfo')->name('User-dept-view');
                Route::get('/viewinfo', 'UserDeptController@viewinfo')->name('User-dept-infor');
                Route::any('/update', 'UserDeptController@update')->name('User-dept-update');
                Route::any('/delete', 'UserDeptController@delete')->name('User-dept-delete');
            });

            Route::group(['prefix' => 'categorysite'], function () {
                Route::any('/', 'CategorySiteController@index')->name('categorysite-index');
                Route::any('/addcategorysite', 'CategorySiteController@create')->name('create-categorysite');
                Route::any('/updatecategorysite', 'CategorySiteController@update')->name('update-categorysite');
                Route::post('deletecategorysite', 'CategorySiteController@delete')->name('delete-categorysite');
                Route::get('/viewinfocategorySite', 'CategorySiteController@viewprofile')->name('view-info-category-site');
            });

            Route::group(['prefix' => 'categorymedicine'], function () {
                Route::any('/', 'CategoryMedicineController@index')->name('categorymedicine-index');
                Route::any('/addcategorymedicine', 'CategoryMedicineController@create')->name('create-categorymedicine');
                Route::any('/updatecategorymedicine', 'CategoryMedicineController@update')->name('update-categorymedicine');
                Route::post('deletecategorymedicine', 'CategoryMedicineController@delete')->name('delete-categorymedicine');
                Route::get('/viewinfocategorymedicine', 'CategoryMedicineController@viewprofile')->name('view-info-category-medicine');
            });

        });
        //Tiến CH
        Route::group(['prefix' => 'csyt'], function () {
            Route::any('/', 'CSYTController@index')->name('csyt-plan-index');
            Route::any('/importPlan', 'CSYTController@importPlan')->name('csyt-plan-import');
            Route::any('/submitValidData', 'CSYTController@submitValidData')->name('csyt-plan-submit-valid');
            Route::any('/submitPlan', 'CSYTController@submitPlan')->name('csyt-plan-submit-plan');
            Route::any('/session', 'CSYTController@session')->name('csyt-plan-session');
            Route::any('/viewSessionData', 'CSYTController@viewSessionData')->name('csyt-plan-session-row');
            Route::get('/viewinfo', 'CSYTController@viewinfo')->name('view-info-csyt');
            Route::any('/updateplan', 'CSYTController@updatePlan')->name('update-plan');
            Route::any('/createplan', 'CSYTController@createPlan')->name('create-plan');
            Route::post('/deleteplan', 'CSYTController@deletePlan')->name('delete-plan');
            Route::get('/viewinfodmthuoc', 'CSYTController@viewinfoDmthuoc')->name('view-info-dm-thuoc');
            Route::post('/checkRow', 'CSYTController@checkRow')->name('csyt-check-row');
            Route::post('/submitImport', 'CSYTController@submitImport')->name('csyt-submit-import');
            Route::get('/changeMa', 'CSYTController@changeMa');
        });

        Route::group(['prefix' => 'prov'], function () {

            Route::any('/csytSession', 'ProvinceController@csytSession')->name('prov-csyt-session');
            Route::any('/viewSessionData', 'ProvinceController@viewSessionData')->name('prov-plan-session-row');
            Route::any('/confirmSession', 'ProvinceController@confirmSession')->name('prov-plan-session-confirm');

            Route::any('/submitPlan', 'ProvinceController@submitPlan')->name('prov-submit-plan');
            Route::any('/sessionSent', 'ProvinceController@sessionSent')->name('prov-session-sent');
            Route::any('/sessionSentRow', 'ProvinceController@sessionSentRow')->name('prov-session-sent-row');

            Route::any('/plan', 'ProvinceController@plan')->name('prov-plan');
            Route::any('/planExport', 'ProvinceController@planExport')->name('prov-plan-export');

            Route::any('/planByMedicine', 'ProvinceController@planByMedicine')->name('prov-plan-by-medicine');
            Route::any('/planByMedicineExport', 'ProvinceController@planByMedicineExport')->name('prov-plan-by-medicine-export');

            Route::any('/infoSite', 'ProvinceController@infoSite')->name('prov-info-site');
            Route::any('/infoSiteExport', 'ProvinceController@infoSiteExport')->name('prov-info-site-export');

        });

        Route::group(['prefix' => 'center'], function () {
            Route::group(['prefix' => 'follow'], function () {
                Route::any('/csytSessiontw', 'CenterFollowController@csytSessionTw')->name('center-csyt-session-tw');
                Route::any('/rowSessionCSYT', 'CenterFollowController@rowSessionCSYT')->name('center-row-session-csyt');

                Route::any('/sessionTW', 'CenterFollowController@sessionTW')->name('center-prov-session-tw');
                Route::any('/rowSessionTW', 'CenterFollowController@rowSessionTW')->name('center-prov-session-tw-row');

                Route::any('/xuLyKH', 'CenterFollowController@xuLyKH')->name('center-xuly-kh');

                Route::any('/syncPro', 'CenterFollowController@syncPro')->name('center-syncPro');
                Route::any('/syncProCSYT', 'CenterFollowController@syncProCSYT');

                Route::any('/exportSession02', 'CenterFollowController@exportSession02')->name('center-export-m02');
                Route::any('/exportSession03', 'CenterFollowController@exportSession03')->name('center-export-m03');

            });
            Route::group(['prefix' => 'tonghop'], function () {
                Route::any('/mau08', 'CenterAgregateController@mau08')->name('center-mau-08');
                Route::any('/mau08export', 'CenterAgregateController@mau08export')->name('center-mau-08-export');

                Route::any('/mau07', 'CenterAgregateController@mau07')->name('center-mau-07');
                Route::any('/mau07Export', 'CenterAgregateController@mau07Export')->name('center-mau-07-export');

                Route::any('/mau06', 'CenterAgregateController@mau06')->name('center-mau-06');
                Route::any('/mau06Export', 'CenterAgregateController@mau06Export')->name('center-mau-06-export');

                Route::any('/mau05', 'CenterAgregateController@mau05')->name('center-mau-05');
                Route::any('/mau05Export', 'CenterAgregateController@mau05Export')->name('center-mau-05-export');
                Route::any('/exportCsyt', 'CenterAgregateController@exportCsyt')->name('center-exportCsyt');
                Route::any('/exportTinh', 'CenterAgregateController@exportTinh')->name('center-exportTinh');
                Route::any('/exportMau03', 'CenterAgregateController@exportMau03')->name('center-exportMau03');
                Route::any('/exportMau04', 'CenterAgregateController@exportMau04')->name('center-exportMau04');
            });
        });

        Route::group(['prefix' => 'file'], function () {
            Route::any('/downloadSessionCV', 'FileControllers@downloadSessionCV')->name('file-session-cv');

        });
        Route::group(['prefix' => 'dm'], function () {
            Route::any('/search/{table}/{fKey}', 'DMController@search')->name('dm-search');
        });
    });
    Route::any('/fixBug', 'CenterFollowController@fixBug');
});
